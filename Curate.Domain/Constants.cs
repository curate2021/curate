﻿namespace Curate.Domain
{
    internal static class Constants
    {
        public const string MaxPriceAmountStr = "999999999";

        public const string MinPriceAmountStr = "0";
    }
}