﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Diagnostics.CodeAnalysis;
using Curate.Domain.Base;
using Curate.Domain.Payment.State;
using Force.Ddd;

namespace Curate.Domain.Payment
{
    public class Payment : IHasCreateDate, IHasId<int>, IPaymentState
    {
        public Payment([NotNull] Order.Order order, int paymentMethodId)
        {
            Order = order;
            PaymentMethodId = paymentMethodId;

            CreateDate = DateTime.Now;
        }

        public Payment([NotNull] Order.Order order, [NotNull] PaymentMethod paymentMethod)
        {
            Order = order;
            PaymentMethod = paymentMethod;

            CreateDate = DateTime.Now;
        }

        protected Payment()
        {
        }

        [Key]
        [ForeignKey(nameof(Order))]
        public int Id { get; protected set; }

        object IHasId.Id { get; }

        public virtual Order.Order Order { get; protected set; }

        public decimal? Amount { get; protected set; }

        public PaymentStatus Status { get; set; }

        [ForeignKey(nameof(PaymentMethod))]
        public int PaymentMethodId { get; protected set; }

        public virtual PaymentMethod PaymentMethod { get; protected set; }

        public DateTime CreateDate { get; protected set; }

        private IPaymentStateful _paymentStateful;

        [NotMapped]
        public IPaymentStateful State
        {
            get => _paymentStateful ??= PaymentStateful.New(this, Status);
            set => _paymentStateful = value;
        }

        public void ApplyPayment(decimal amount, int paymentMethodId)
        {
            if (amount <= 0)
                throw new ArgumentException(nameof(amount));

            Amount = amount;
            PaymentMethodId = paymentMethodId;
        }

        public void Pay(decimal amount, int paymentMethodId) => State.Pay(amount, paymentMethodId);


        public void Fail()
        {
            State.Fail();
        }
    }
}