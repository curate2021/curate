﻿namespace Curate.Domain.Payment
{
    public enum PaymentMethodType : byte
    {
        CreditOrDebitCard,
        PayPal,
        Btc,
        Xcur
    }
}