﻿using System;
using Curate.Domain.Base;

namespace Curate.Domain.Payment.State
{
    public abstract class PaymentStateful : IPaymentStateful
    {
        protected PaymentStateful(Payment payment, PaymentStatus status)
        {
            Payment = payment;
            PaymentStatus = status;
            OnEnter();
        }

        protected Payment Payment { get; }

        private PaymentStatus PaymentStatus { get; }

        public virtual void Pay(decimal amount, int paymentMethodId) => throw new InvalidOperationException();

        public virtual void Fail() => throw new InvalidOperationException();

        public void OnEnter()
        {
            Payment.Status = PaymentStatus;
        }

        public void Become(IStateful<Payment> next)
        {
            Payment.State = (IPaymentStateful) next;
            next.OnEnter();
        }

        public static IPaymentStateful New(Payment payment, PaymentStatus status)
            => status switch
            {
                PaymentStatus.NotPaid => new NotPaid(payment, status),
                PaymentStatus.Paid => new Paid(payment, status),
                PaymentStatus.Failed => new Failed(payment, status),
                _ => throw new ArgumentOutOfRangeException(nameof(status), status, null)
            };
    }
}