﻿namespace Curate.Domain.Payment.State
{
    public class NotPaid : PaymentStateful
    {
        public override void Pay(decimal amount, int paymentMethodId)
        {
            Payment.ApplyPayment(amount, paymentMethodId);
            Become(New(Payment, PaymentStatus.Paid));
        }

        public NotPaid(Payment payment, PaymentStatus status) : base(payment, status)
        {
        }
    }
}