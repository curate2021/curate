﻿namespace Curate.Domain.Payment.State
{
    public class Failed : PaymentStateful
    {
        public Failed(Payment payment, PaymentStatus status) : base(payment, status)
        {
        }
    }
}