﻿namespace Curate.Domain.Payment.State
{
    public class Paid : PaymentStateful
    {
        public Paid(Payment payment, PaymentStatus status) : base(payment, status)
        {
        }
    }
}