﻿using Curate.Domain.Base;

namespace Curate.Domain.Payment.State
{
    public interface IPaymentStateful : IPaymentState, IStateful<Payment>
    {
    }
}