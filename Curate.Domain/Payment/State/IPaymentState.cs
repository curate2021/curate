﻿namespace Curate.Domain.Payment.State
{
    public interface IPaymentState
    {
        void Pay(decimal amount, int paymentMethodId);
        void Fail();
    }
}