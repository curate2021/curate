﻿namespace Curate.Domain.Payment
{
    public enum PaymentStatus : byte
    {
        NotPaid,
        Paid,
        Failed
    }
}