﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Curate.Domain.Entities;
using Force.Ddd;

namespace Curate.Domain.Payment
{
    public class PaymentMethod : IHasId<int>
    {
        public PaymentMethod(PaymentMethodType type, string name, File icon = null, decimal? fee = null)
        {
            Type = type;
            Name = name;
            Icon = icon;
            Fee = fee;
        }

        protected PaymentMethod()
        {
        }

        object IHasId.Id => Id;

        [Key]
        [Required]
        public int Id { get; private set; }

        public string Name { get; protected set; }

        public PaymentMethodType Type { get; protected set; }

        public string IconId { get; protected set; }
        public virtual File Icon { get; protected set; }

        [Column(TypeName = "numeric(3,3)")]
        public decimal? Fee { get; protected set; }

        public static Spec<PaymentMethod> IsPayPal =>
            new Spec<PaymentMethod>(pm => pm.Type == PaymentMethodType.PayPal);
    }
}