﻿using Microsoft.AspNetCore.Identity;

namespace Curate.Domain.Entities
{
    public class Role : IdentityRole<int>
    {
        public Role(string roleName) : base(roleName)
        {
        }
        
        protected Role() {}
    }
}