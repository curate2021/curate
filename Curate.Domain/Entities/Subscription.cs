﻿using System.ComponentModel.DataAnnotations.Schema;

namespace Curate.Domain.Entities
{
    public class Subscription
    {
        public Subscription(int toId, int fromId)
        {
            ToId = toId;
            FromId = fromId;
        }

        protected Subscription()
        {
        }

        [ForeignKey(nameof(To))]
        public int ToId { get; protected set; }

        public virtual Profile To { get; protected set; }

        [ForeignKey(nameof(From))]
        public int FromId { get; protected set; }

        public virtual Profile From { get; protected set; }
    }
}