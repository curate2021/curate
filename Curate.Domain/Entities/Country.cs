﻿using Curate.Domain.Base;
using Force.Ddd;

namespace Curate.Domain.Entities
{
    public class Country : HasNameBase
    {
        protected Country()
        {
        }

        public Country(string name)
        {
            Name = name;
        }
    }
}