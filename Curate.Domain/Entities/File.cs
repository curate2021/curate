﻿using System;
using System.ComponentModel.DataAnnotations.Schema;
using System.Diagnostics.CodeAnalysis;
using Curate.Domain.Base;
using Force.Ddd;

namespace Curate.Domain.Entities
{
    public class File : HasIdBase<string>, IHasCreateDate
    {
        public File([NotNull] string id, FileType type)
        {
            Id = id ?? throw new ArgumentNullException(nameof(id));
            Type = type;
            CreateDate = DateTime.UtcNow;
        }

        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public override string Id { get; set; }

        public FileType Type { get; set; }

        public DateTime CreateDate { get; private set; }

        public static Spec<File> IsType(FileType type) => new Spec<File>(f => f.Type == type);
    }

    public enum FileType
    {
        Image,
        Icon,
        KycDocument
    }
}