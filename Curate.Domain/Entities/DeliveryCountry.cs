﻿using System;
using System.ComponentModel.DataAnnotations;
using Curate.Domain.Base;
using Force.Ddd;

namespace Curate.Domain.Entities
{
    public class DeliveryCountry : HasIdBase
    {
        protected DeliveryCountry()
        {
        }

        protected DeliveryCountry(decimal cost)
        {
            if (cost < 0)
            {
                throw new ArgumentException(nameof(cost));
            }

            Cost = cost;
        }

        public DeliveryCountry(int countryId, int listingId, decimal cost) : this(cost)
        {
            CountryId = countryId;
            ListingId = listingId;
        }

        public DeliveryCountry(Country country, Listing.Listing listing, decimal cost) : this(cost)
        {
            Country = country;
            Listing = listing;
        }

        public DeliveryCountry(int countryId, Listing.Listing listing, decimal cost) : this(cost)
        {
            CountryId = countryId;
            Listing = listing;
        }
        
        public int CountryId { get; protected set; }

        public virtual Country Country { get; protected set; }

        public int ListingId { get; protected set; }

        public virtual Listing.Listing Listing { get; protected set; }

        [Range(typeof(decimal), Constants.MinPriceAmountStr, Constants.MaxPriceAmountStr)]
        public decimal Cost { get; protected set; }
    }
}