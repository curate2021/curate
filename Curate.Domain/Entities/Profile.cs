﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using System.Net;
using Curate.Exceptions;

namespace Curate.Domain.Entities
{
    public class Profile
    {
        protected Profile()
        {
        }

        public Profile(string name, string surname, int countryId, User user)
        {
            Name = !string.IsNullOrEmpty(name) ? name : throw new ArgumentNullException(nameof(name));
            Surname = !string.IsNullOrEmpty(surname) ? surname : throw new ArgumentNullException(nameof(surname));
            User = user;
            CountryId = countryId;
        }

        [Key]
        [ForeignKey(nameof(User))]
        public int Id { get; protected set; }

        public virtual User User { get; protected set; }

        [ForeignKey(nameof(Country))]
        public int CountryId { get; protected set; }

        public virtual Country Country { get; protected set; }

        [Required]
        [MaxLength(byte.MaxValue)]
        public string Name { get; protected set; }

        [Required]
        [MaxLength(byte.MaxValue)]
        public string Surname { get; protected set; }

        [NotMapped]
        public string FullName => Name + " " + Surname;

        public string PhotoId { get; protected set; }

        public virtual File Photo { get; protected set; }
        
        [ForeignKey(nameof(Kyc))]
        public int? KycId { get; protected set; }
        public virtual Kyc.Kyc Kyc { get; protected set; }

        public virtual ICollection<Interest> Interests { get; protected set; }

        public virtual ICollection<Subscription> Subscriptions { get; private set; }

        public virtual ICollection<Subscription> Subscribers { get; private set; }

        public virtual ICollection<Listing.Listing> Listings { get; private set; }

        public void AddPhoto([NotNull] File file)
        {
            if (file == null || file.Type != FileType.Image)
                throw new ArgumentException(nameof(file));
            Photo = file;
        }

        public void AddInterests(IEnumerable<Interest> interests)
        {
            if (Interests == null || !Interests.Any())
            {
                Interests = new List<Interest>(interests);
            }
            else
            {
                foreach (var interest in interests)
                {
                    Interests.Add(interest);
                }
            }
        }

        public void AddKyc(File photo)
        {
            if (Kyc != null)
            {
                throw new HttpException(HttpStatusCode.BadRequest, "KYC document already exists");
            }
            Kyc = new Kyc.Kyc(this, photo);
        }
    }
}