﻿using System;
using System.Collections.Generic;
using System.Linq;
using Force.Ddd;

namespace Curate.Domain.Entities
{
    public class UserNotification : HasIdBase
    {
        protected UserNotification() {}

        private UserNotification(bool isDisabled)
        {
            IsDisabled = isDisabled;
        }

        public UserNotification(int userId, int notificationId, bool isDisabled) : this(isDisabled)
        {
            UserId = userId;
            NotificationId = notificationId;
        }

        public UserNotification(User user, int notificationId, bool isDisabled) : this(isDisabled)
        {
            User = user ?? throw new ArgumentNullException(nameof(user));
            NotificationId = notificationId;
        }
        
        public int UserId { get; protected set; }
        
        public virtual User User { get; protected set; }
        
        public int NotificationId { get; protected set; }
        
        public virtual Notification Notification { get; protected set; }
        
        public bool IsDisabled { get; protected set; }

        public void Toggle(bool isDisabled) => IsDisabled = isDisabled;

        public static IEnumerable<UserNotification> CreateDefaultNotifications(User user) =>
            Notification.GetDefaultNotifications
                .Select(x => new UserNotification(user, x.Id, false));
    }
}