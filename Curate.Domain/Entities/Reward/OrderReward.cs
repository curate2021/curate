﻿using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace Curate.Domain.Entities.Reward
{
    [Table("OrderRewards")]
    public class OrderReward : Reward
    {
        protected OrderReward()
        {
        }

        public OrderReward(Order.Order order, decimal rewardRate, User userTo) : base(RewardType.Order, userTo)
        {
            RewardRate = rewardRate;
            Order = order ?? throw new ArgumentNullException(nameof(order));
            TotalTokens = rewardRate * order.Quantity * order.Listing.Price;
            Description = order.Listing.Title;
        }

        public int OrderId { get; protected set; }

        public virtual Order.Order Order { get; protected set; }
        
        public decimal RewardRate { get; protected set; }

        public static OrderReward[] CreateOrderRewards(Order.Order order, OrderRewardRate orderOrderRewardRate) => new[]
        {
            new OrderReward(order, orderOrderRewardRate.BuyerRatePercent, order.Buyer),
            new OrderReward(order, orderOrderRewardRate.SellerRatePercent, order.Listing.Seller.User)
        };
    }
}