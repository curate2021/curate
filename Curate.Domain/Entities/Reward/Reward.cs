﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Curate.Domain.Base;
using Force.Ddd;

namespace Curate.Domain.Entities.Reward
{
    [Table("Rewards")]
    public abstract class Reward : HasIdBase, IHasCreateDate
    {
        protected Reward() {}
        
        protected Reward(RewardType rewardType, User userTo)
        {
            UserTo = userTo ?? throw new ArgumentNullException(nameof(userTo));
            State = RewardState.Unpaid;
            CreateDate = DateTime.UtcNow;
            RewardType = rewardType;
        }

        public RewardState State { get; protected set; }

        public DateTime? PayDate { get; protected set; }

        public int UserToId { get; protected set; }

        [Required] 
        public User UserTo { get; protected set; }

        [Required] 
        public decimal TotalTokens { get; protected set; }

        public RewardType RewardType { get; protected set; }

        public DateTime CreateDate { get; protected set; }
        
        public string Description { get; protected set; }

        public void Pay()
        {
            State = RewardState.Paid;
            PayDate = DateTime.UtcNow;
        }

        public static Spec<Reward> InState(RewardState state) => new Spec<Reward>(x => x.State == state);
    }
}