﻿namespace Curate.Domain.Entities.Reward
{
    public enum RewardState : byte
    {
        Unpaid,
        Paid
    }
}