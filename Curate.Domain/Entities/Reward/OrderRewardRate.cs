﻿using System;
using Curate.Domain.Base;
using Force.Ddd;

namespace Curate.Domain.Entities.Reward
{
    public class OrderRewardRate : HasIdBase, IHasModificationDate
    {
        protected OrderRewardRate()
        {
        }

        public OrderRewardRate(decimal sellerRatePercent, decimal buyerRatePercent)
        {
            Update(sellerRatePercent, buyerRatePercent);
        }

        public decimal SellerRatePercent { get; protected set; }

        public decimal BuyerRatePercent { get; protected set; }

        public DateTime ModificationDate { get; protected set; }

        public void Update(decimal sellerRatePercent, decimal buyerRatePercent)
        {
            if (sellerRatePercent <= 0) throw new ArgumentException(nameof(sellerRatePercent));

            if (buyerRatePercent <= 0) throw new ArgumentException(nameof(buyerRatePercent));

            SellerRatePercent = sellerRatePercent;
            BuyerRatePercent = buyerRatePercent;

            ModificationDate = DateTime.UtcNow;
        }
    }
}