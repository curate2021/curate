﻿using System;
using System.ComponentModel.DataAnnotations;
using Curate.Domain.Base;
using Force.Ddd;

namespace Curate.Domain.Entities
{
    public class Comment : HasIdBase<long>, IHasCreateDate
    {
        protected Comment()
        {
            
        }
        
        public Comment(string text, int senderId, int listingId)
        {
            Text = text;
            SenderId = senderId;
            ListingId = listingId;
            CreateDate = DateTime.UtcNow;
        }
        
        public int SenderId { get; protected set; }
        
        [Required]
        public virtual User Sender { get; protected set; }

        public int ListingId { get; protected set; }
        
        [Required]
        public virtual Listing.Listing Listing { get; protected set; }

        [Required]
        [MaxLength(byte.MaxValue)]
        public string Text { get; protected set; }

        public DateTime CreateDate { get; protected set; }
    }
}