﻿using System;
using Curate.Domain.Base;
using Force.Ddd;

namespace Curate.Domain.Entities
{
    public class Firebase : HasIdBase, ISoftDelete
    {
        protected Firebase()
        {
        }

        private Firebase(string token)
        {
            SetToken(token);
        }
        
        public Firebase(int userId, string token) : this(token)
        {
            UserId = userId;
        }
        
        public Firebase(User user, string token) : this(token)
        {
            User = user ?? throw new ArgumentNullException(nameof(user));
        }

        public int UserId { get; protected set; }

        public virtual User User { get; protected set; }

        public string Token { get; protected set; }
        
        public bool IsDeleted { get; protected set; }

        public static Spec<Firebase> HasToken => new Spec<Firebase>(x => !string.IsNullOrEmpty(x.Token));

        public void SetToken(string token)
        {
            Token = token ?? throw new ArgumentNullException(nameof(token));
            IsDeleted = false;
        }

        public void Delete()
        {
            IsDeleted = true;
        }
    }
}