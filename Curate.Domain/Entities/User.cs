﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using Force.Ddd;
using Microsoft.AspNetCore.Identity;

namespace Curate.Domain.Entities
{
    public class User : IdentityUser<int>
    {
        protected User()
        {
        }

        public User(string name, string surname, string email, int countryId) : base(email)
        {
            Email = !string.IsNullOrEmpty(email) ? email : throw new ArgumentNullException(nameof(email));
            Profile = new Profile(name, surname, countryId, this);
            Firebase = new Firebase(this, string.Empty);
            CreateDefaultNotifications();
        }

        public void CreateDefaultNotifications()
        {
            if(UserNotifications == null || UserNotifications.Count == 0)
            {
                UserNotifications = new List<UserNotification>(UserNotification.CreateDefaultNotifications(this));
            }
        }

        public virtual Profile Profile { get; protected set; }

        public virtual Firebase Firebase { get; protected set; }

        public virtual ICollection<UserNotification> UserNotifications { get; protected set; }

        public static Spec<User> CanReceiveNotification(NotificationType notificationType) => new Spec<User>(x =>
            x.UserNotifications.Any(n => n.Notification.NotificationType == notificationType && !n.IsDisabled));
    }
}