﻿using System;
using System.Collections.Generic;
using System.Linq;
using Curate.Domain.Base;
using Force.Ddd;

namespace Curate.Domain.Entities
{
    public class Category : HasNameBase
    {
        public string IconId { get; protected set; }

        public virtual File Icon { get; protected set; }
        
        public virtual ICollection<Listing.Listing> Listings { get; protected set; }

        protected Category()
        {
        }

        public Category(string name, File icon)
        {
            Name = name ?? throw new ArgumentNullException(nameof(name));
            Icon = icon;
        }

        public Category(string name, string iconId)
        {
            Name = name ?? throw new ArgumentNullException(nameof(name));
            IconId = iconId;
        }
    }
}