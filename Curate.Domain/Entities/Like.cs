﻿using System;
using System.ComponentModel.DataAnnotations;
using Curate.Domain.Base;
using Force.Ddd;

namespace Curate.Domain.Entities
{
    public class Like : HasIdBase<long>, IHasCreateDate
    {
        public int UserId { get; protected set; }
        
        [Required]
        public virtual User User { get; protected set; }

        public int ListingId { get; protected set; }
        
        [Required]
        public virtual Listing.Listing Listing { get; protected set; }

        public DateTime CreateDate { get; protected set; }

        protected Like()
        {
            
        }

        public Like(int userId, int listingId)
        {
            UserId = userId;
            ListingId = listingId;
            CreateDate = DateTime.UtcNow;
        }
    }
}