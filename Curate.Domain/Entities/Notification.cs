﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Force.Ddd;

namespace Curate.Domain.Entities
{
    public class Notification : HasIdBase
    {
        [Key]
        [Required]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public override int Id { get; set; }

        protected Notification()
        {
        }

        public Notification(int id, NotificationType notificationType)
        {
            Id = id;
            NotificationType = notificationType;
        }

        public virtual ICollection<UserNotification> UserNotifications { get; protected set; }

        public NotificationType NotificationType { get; protected set; }

        public static IEnumerable<Notification> GetDefaultNotifications => new[]
        {
            new Notification(1, NotificationType.Like),
            new Notification(2, NotificationType.FollowingPublication),
        };
    }

    public enum NotificationType : byte
    {
        Like,
        FollowingPublication
    }
}