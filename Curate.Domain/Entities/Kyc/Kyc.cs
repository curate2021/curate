﻿using System;
using System.ComponentModel.DataAnnotations.Schema;
using Curate.Domain.Base;
using Force.Ddd;

namespace Curate.Domain.Entities.Kyc
{
    public class Kyc : HasIdBase, IHasCreateDate
    {
        protected Kyc(){ }

        public Kyc(Profile profile, File photo)
        {
            Profile = profile;

            Photo = photo;
            DocumentType = KycDocumentType.DriverLicence;
            CreateDate = DateTime.UtcNow;
            IsConfirmed = false;
        }
        
        [ForeignKey(nameof(Profile))]
        public int? ProfileId { get; protected set; }
        
        public virtual Profile Profile { get; protected set; }
        public string PhotoId { get; protected set; }
        
        public virtual File Photo { get; protected set; }
        
        public KycDocumentType DocumentType { get; protected set; }
        
        public DateTime CreateDate { get; protected set; }
        
        public bool IsConfirmed { get; protected set; }

        public void Confirm()
        {
            IsConfirmed = true;
        }
    }
}