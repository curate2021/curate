﻿using System;
using System.Collections.Generic;
using Force.Ddd;

namespace Curate.Domain.Entities
{
    public class Interest : HasNameBase
    {
        protected Interest() {}

        public Interest(string name, string description)
        {
            Name = name ?? throw new ArgumentNullException(nameof(name));
            Description = description ?? throw new ArgumentNullException(nameof(description));
        }
        
        public string Description { get; protected set; }

        public virtual ICollection<Profile> Profiles { get; protected set; }
    }
}