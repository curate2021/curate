﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using Curate.Domain.Base;
using Curate.Domain.Entities;
using Force.Ddd;
using Force.Expressions;

namespace Curate.Domain.Listing
{
    public partial class Listing : HasIdBase<int>, IHasCreateDate, ISoftDelete
    {
        protected Listing()
        {
        }

        protected Listing(string title, string description, int quantity, decimal price,
            IEnumerable<File> images, IEnumerable<Category> categories)
        {
            Update(title, description, quantity, price, images, categories);

            CreateDate = DateTime.UtcNow;
        }

        public Listing(string title, string description, int quantity, decimal price, Profile seller,
            IEnumerable<File> images, IEnumerable<Category> categories) : this(title, description, quantity, price,
            images, categories)
        {
            Seller = seller;
        }

        public Listing(string title, string description, int quantity, decimal price, int sellerId,
            IEnumerable<File> images, IEnumerable<Category> categories) : this(title, description, quantity, price,
            images, categories)
        {
            SellerId = sellerId;
        }

        [Required]
        public string Title { get; set; }

        [Required]
        public string Description { get; set; }

        [Range(0, int.MaxValue)]
        public int Quantity { get; set; }

        [Range(typeof(decimal), Constants.MinPriceAmountStr, Constants.MaxPriceAmountStr)]
        public decimal Price { get; set; }

        public bool IsDeleted { get; protected set; }

        public bool IsArchived { get; protected set; }

        public bool CanBeDeliveredToAnyCountry { get; protected set; }

        public DateTime CreateDate { get; private set; }

        public int SellerId { get; protected set; }

        [Required]
        public virtual Profile Seller { get; private set; }

        public virtual ICollection<File> Files { get; private set; }

        [NotMapped]
        public IEnumerable<File> Images => Files.Where(f => f.Type == FileType.Image);

        [NotMapped]
        public IEnumerable<string> ImageIds => Images.Select(i => i.Id);

        public virtual ICollection<Like> Likes { get; private set; }

        public virtual ICollection<Category> Categories { get; protected set; }

        public virtual ICollection<Comment> Comments { get; private set; }

        public virtual ICollection<DeliveryCountry> DeliveryCountries { get; protected set; }

        public virtual ICollection<Order.Order> Orders { get; protected set; }

        public static Spec<Listing> IsNotDeleted =>
            new Spec<Listing>(x => !x.IsDeleted);

        public static Spec<Listing> IsArchivedSpec =>
            new Spec<Listing>(x => x.IsArchived);

        public static Spec<Listing> InRange(decimal from, decimal to) =>
            new Spec<Listing>(x => x.Price >= from && x.Price <= to);

        public static Spec<Listing> IsSeller(int profileId) =>
            new Spec<Listing>(x => x.SellerId == profileId);

        public static Spec<Listing> CanBeDeliveredToCountry(int countryId) =>
            new Spec<Listing>(x =>
                x.DeliveryCountries.Any(x => x.CountryId == countryId) || x.CanBeDeliveredToAnyCountry);

        public bool CheckHasDeliveryTo(int countryId) =>
            DeliveryCountries
                .Select(c => c.CountryId)
                .Contains(countryId);

        public void AddDeliveryCountries(IEnumerable<DeliveryCountry> deliveryCountries)
        {
            DeliveryCountries = new List<DeliveryCountry>(deliveryCountries);
        }

        public void Delete()
        {
            IsDeleted = true;
        }

        public void Archive()
        {
            IsArchived = true;
        }

        public void Update(string title, string description, int quantity, decimal price,
            IEnumerable<File> images, IEnumerable<Category> categories)
        {
            Title = title ?? throw new ArgumentNullException(nameof(title));
            Description = description ?? throw new ArgumentNullException(nameof(description));
            if (quantity <= 0)
            {
                throw new ArgumentException($"{nameof(quantity)} should be more than zero");
            }

            Quantity = quantity;

            if (price <= 0)
            {
                throw new ArgumentException($"{nameof(price)} should be more than zero");
            }

            Price = price;
            Files?.Clear();
            Files = images as List<File> ?? new List<File>(images);
            Categories?.Clear();
            Categories = categories as List<Category> ?? new List<Category>(categories);
        }

        public void Like(int userId)
        {
            Likes.Add(new Like(userId, Id));
        }

        public void Unlike(int userId)
        {
            var myLike = Likes.FirstOrDefault(x => x.UserId == userId);
            if (myLike != null)
            {
                Likes.Remove(myLike);
            }
        }

        public int GetAvailableQuantity()
            => Quantity - Orders
                .Where(Order.Order.IsActual.IsSatisfiedBy)
                .Count();

        public bool CheckHasEnoughQuantity(int requested)
            => GetAvailableQuantity() - requested >= 0;

        public void MakeAvailableToAnyCountry()
        {
            CanBeDeliveredToAnyCountry = true;
        }
    }
}