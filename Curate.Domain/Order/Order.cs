﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Diagnostics.CodeAnalysis;
using Curate.Domain.Base;
using Curate.Domain.Entities;
using Curate.Domain.Order.State;
using Curate.Domain.Payment;
using Force.Ddd;

namespace Curate.Domain.Order
{
    public class Order : HasIdBase<int>, IHasCreateDate, IOrderState
    {
        public Order(int listingId, int buyerId, int deliveryCountryId, int paymentMethodId, int quantity,
            string deliveryAddress = null)
        {
            if (buyerId <= 0)
                throw new ArgumentException(nameof(buyerId));

            if (deliveryCountryId <= 0)
                throw new ArgumentException(nameof(deliveryCountryId));

            if (quantity <= 0)
                throw new ArgumentException(nameof(quantity));

            if (paymentMethodId <= 0)
                throw new ArgumentException(nameof(quantity));

            ListingId = listingId;
            BuyerId = buyerId;
            DeliveryCountryId = deliveryCountryId;
            Payment = new Payment.Payment(this, paymentMethodId);
            DeliveryAddress = deliveryAddress;

            // TODO: For MVP version.
            Quantity = quantity;

            Status = OrderStatus.Pending;

            CreateDate = DateTime.Now;
        }

        public Order([NotNull] Listing.Listing listing, [NotNull] User buyer, [NotNull] DeliveryCountry deliveryCountry,
            [NotNull] PaymentMethod paymentMethod, int quantity, string deliveryAddress = null)
        {
            Listing = listing;
            Buyer = buyer;
            DeliveryCountry = deliveryCountry;
            Payment = new Payment.Payment(this, paymentMethod);
            DeliveryAddress = deliveryAddress;

            // TODO: For MVP version.
            Quantity = quantity;

            Status = OrderStatus.Pending;

            CreateDate = DateTime.Now;
        }

        protected Order()
        {
        }

        public int ListingId { get; protected set; }
        public virtual Listing.Listing Listing { get; protected set; }

        public int BuyerId { get; protected set; }
        public virtual User Buyer { get; protected set; }

        public int Quantity { get; protected set; }

        public int DeliveryCountryId { get; protected set; }
        public virtual DeliveryCountry DeliveryCountry { get; protected set; }

        public virtual Payment.Payment Payment { get; protected set; }

        [Required]
        public string DeliveryAddress { get; protected set; }

        // Need internal set for changing status with state pattern.
        public OrderStatus Status { get; internal set; }

        private IOrderStateful _state;

        [NotMapped]
        public IOrderStateful State
        {
            get => _state ??= OrderState.New(Status, this);
            set => _state = value;
        }

        public void Confirm(decimal amount, int paymentMethodId) => State.Confirm(amount, paymentMethodId);

        public void Fail() => State.Fail();

        public void Sent(string trackingNumber) => State.Sent(trackingNumber);

        public void Cancel() => State.Cancel();

        public void Receive() => State.Receive();

        public void Complete() => State.Complete();

        public void ApplyTrackingNumber([NotNull] string trackingNumber)
        {
            if (string.IsNullOrEmpty(trackingNumber))
                throw new ArgumentException(nameof(trackingNumber));

            TrackingNumber = trackingNumber;
        }

        public bool CheckInStatus(OrderStatus value) => State.Status == value;

        public string TrackingNumber { get; protected set; }

        [Required]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Number { get; protected set; }

        public DateTime CreateDate { get; protected set; }

        #region Specs

        public static Spec<Order> IsActual =>
            new Spec<Order>(o => o.Status != OrderStatus.Failed && o.Status != OrderStatus.Canceled);

        public static Spec<Order> IsAvailableListing =>
            Domain.Listing.Listing.IsNotDeleted
                .From<Order>(p => p.Listing);

        public static Spec<Order> IsListingSeller(int sellerId) =>
            Domain.Listing.Listing.IsSeller(sellerId)
                .From<Order>(p => p.Listing);

        public static Spec<Order> IsBuyer(int userId) => new Spec<Order>(o => o.BuyerId == userId);

        #endregion
    }
}