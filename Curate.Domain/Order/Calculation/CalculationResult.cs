﻿namespace Curate.Domain.Order.Calculation
{
    public class CalculationResult
    {
        public decimal TotalAmount { get; set; }

        public decimal FeeAmount { get; set; }

        public decimal FeePercent { get; set; }

        public decimal DeliveryCost { get; set; }
    }
}