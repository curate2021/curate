﻿using System;
using Curate.Domain.Base;

namespace Curate.Domain.Order.Calculation
{
    public class CalculateAmountDomainHandler : IDomainHandler<Order, CalculationResult>
    {
        public CalculationResult Handle(Order input)
        {
            var listingAmount = input.Quantity * input.Listing.Price;

            var fee = input.Payment.PaymentMethod.Fee;

            var deliveryAmount = input.DeliveryCountry.Cost;

            var originalAmount = listingAmount + deliveryAmount;

            var feeAmount = fee != null
                ? (decimal) fee * originalAmount
                : 0;

            var totalAmount = originalAmount + feeAmount;
            var totalAmountCeiling = Math.Ceiling(totalAmount);
            var totalAmountDiff = totalAmountCeiling - totalAmount;

            var feeFinalAmount = feeAmount + totalAmountDiff;
            var feeFinalPercent = Math.Round(feeFinalAmount / originalAmount * 10000) / 10000 * 100;

            return new CalculationResult
            {
                DeliveryCost = deliveryAmount,
                FeeAmount = feeFinalAmount,
                FeePercent = feeFinalPercent,
                TotalAmount = totalAmountCeiling
            };
        }
    }
}