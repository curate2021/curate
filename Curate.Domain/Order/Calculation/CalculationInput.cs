﻿namespace Curate.Domain.Order.Calculation
{
    public class CalculationInput
    {
        public CalculationInput(int quantity, decimal price, decimal deliveryCost, decimal? fee = null)
        {
            Quantity = quantity;
            Price = price;
            Fee = fee;
            DeliveryCost = deliveryCost;
        }

        public int Quantity { get; }

        public decimal Price { get; }

        public decimal? Fee { get; }

        public decimal DeliveryCost { get; }
    }
}