﻿using System.Linq;
using System.Net;
using Curate.Core;
using Curate.Domain.Base;
using Curate.Domain.Entities;
using Curate.Domain.Payment;
using Curate.Exceptions;
using Force.Linq;
using Microsoft.EntityFrameworkCore;

namespace Curate.Domain.Order
{
    public class CreateOrderDomainHandler : IDomainHandler<CreateOrderInfo, Order>
    {
        private readonly DbContext _dbContext;
        private readonly IUserContext<User> _userContext;

        public CreateOrderDomainHandler(DbContext dbContext,
            IUserContext<User> userContext)
        {
            _dbContext = dbContext;
            _userContext = userContext;
        }

        public Order Handle(CreateOrderInfo input)
        {
            var listing = _dbContext
                .Set<Listing.Listing>()
                .FirstOrDefaultById(input.ListingId) ?? throw new HttpException(HttpStatusCode.NotFound,
                $"Listing with id {input.ListingId} not found.");

            var userCountryId = _userContext.User.Profile.Country.Id;

            if (!listing.CheckHasDeliveryTo(userCountryId))
                throw new HttpException(HttpStatusCode.BadRequest, "Delivery in this country is not available.");

            var isEnoughQuantity = listing.CheckHasEnoughQuantity(input.Quantity);

            if (!isEnoughQuantity)
                throw new HttpException(HttpStatusCode.BadRequest, "This quantity is not available.");

            var paymentMethod = _dbContext
                .Set<PaymentMethod>()
                .FirstOrDefaultById(input.PaymentMethodId);

            if (paymentMethod == null)
                throw new HttpException(HttpStatusCode.NotFound,
                    "Payment method not found or unavailable.");

            var deliveryCountry = listing
                .DeliveryCountries
                .First(dc => dc.CountryId == userCountryId);

            return new Order(listing, _userContext.User, deliveryCountry, paymentMethod, input.Quantity,
                    input.DeliveryAddress);
        }
    }
}