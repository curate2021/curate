﻿namespace Curate.Domain.Order.State
{
    public class Pending : OrderState
    {
        public override void Confirm(decimal amount, int paymentMethodId)
        {
            Order.Payment.Pay(amount, paymentMethodId);
            Become(New(OrderStatus.AwaitingDispatch, Order));
        }

        public override void Fail()
        {
            Become(New(OrderStatus.Failed, Order));
        }

        public Pending(Order order, OrderStatus status) : base(order, status)
        {
        }
    }
}