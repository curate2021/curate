﻿namespace Curate.Domain.Order.State
{
    public class Failed : OrderState
    {
        public override void Fail()
        {
            Become(New(OrderStatus.Completed, Order));
        }

        public Failed(Order order, OrderStatus status) : base(order, status)
        {
        }
    }
}