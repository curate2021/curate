﻿namespace Curate.Domain.Order.State
{
    public class Sent : OrderState
    {
        public override void Receive()
        {
            Become(New(OrderStatus.Received, Order));
        }

        public Sent(Order order, OrderStatus status) : base(order, status)
        {
        }
    }
}