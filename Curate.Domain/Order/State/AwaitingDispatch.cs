﻿namespace Curate.Domain.Order.State
{
    public class AwaitingDispatch : OrderState
    {
        public override void Sent(string trackingNumber)
        {
            Order.ApplyTrackingNumber(trackingNumber);
            Become(New(OrderStatus.Sent, Order));
        }

        public override void Cancel()
        {
            Become(New(OrderStatus.Canceled, Order));
        }

        public AwaitingDispatch(Order order, OrderStatus status) : base(order, status)
        {
        }
    }
}