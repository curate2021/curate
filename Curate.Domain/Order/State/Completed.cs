﻿namespace Curate.Domain.Order.State
{
    public class Completed : OrderState
    {

        public Completed(Order order, OrderStatus status) : base(order, status)
        {
        }
    }
}