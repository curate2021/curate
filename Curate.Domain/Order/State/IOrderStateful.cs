﻿using Curate.Domain.Base;

namespace Curate.Domain.Order.State
{
    public interface IOrderStateful : IStateful<Order>, IOrderState
    {
    }
}