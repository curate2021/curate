﻿namespace Curate.Domain.Order.State
{
    public class Canceled : OrderState
    {
        public Canceled(Order order, OrderStatus status) : base(order, status)
        {
        }
    }
}