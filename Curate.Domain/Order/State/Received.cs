﻿namespace Curate.Domain.Order.State
{
    public class Received : OrderState
    {

        public override void Complete()
        {
            Become(New(OrderStatus.Completed, Order));
        }

        public Received(Order order, OrderStatus status) : base(order, status)
        {
        }
    }
}