﻿using System;
using Curate.Domain.Base;

namespace Curate.Domain.Order.State
{
    public abstract class OrderState : IOrderStateful
    {
        protected OrderState(Order order, OrderStatus status)
        {
            Order = order;
            Status = status;

            OnEnter();
        }

        protected Order Order { get; }

        public OrderStatus Status { get; }

        public virtual void Confirm(decimal amount, int paymentMethodId) => throw new InvalidOperationException();

        public virtual void Fail() => throw new InvalidOperationException();

        public virtual void Sent(string trackingNumber) => throw new InvalidOperationException();

        public virtual void Cancel() => throw new InvalidOperationException();

        public virtual void Receive() => throw new InvalidOperationException();

        public virtual void Complete() => throw new InvalidOperationException();

        public void OnEnter()
        {
            Order.Status = Status;
        }

        public void Become(IStateful<Order> next)
        {
            Order.State = (IOrderStateful) next;
        }

        public static IOrderStateful New(OrderStatus status, Order order)
            => status switch
            {
                OrderStatus.Pending => new Pending(order, status),
                OrderStatus.AwaitingDispatch => new AwaitingDispatch(order, status),
                OrderStatus.Sent => new Sent(order, status),
                OrderStatus.Received => new Received(order, status),
                OrderStatus.Completed => new Completed(order, status),
                OrderStatus.Failed => new Failed(order, status),
                OrderStatus.Canceled => new Canceled(order, status),
                _ => throw new ArgumentOutOfRangeException(nameof(status), status, null)
            };
    }
}