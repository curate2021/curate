﻿namespace Curate.Domain.Order.State
{
    public interface IOrderState
    {
        OrderStatus Status { get; }
        void Confirm(decimal amount, int paymentMethodId);
        void Fail();
        void Sent(string trackingNumber);
        void Cancel();
        void Receive();
        void Complete();
    }
}