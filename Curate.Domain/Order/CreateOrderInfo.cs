﻿namespace Curate.Domain.Order
{
    public class CreateOrderInfo
    {
        public int ListingId { get; set; }

        public int PaymentMethodId { get; set; }

        public int Quantity { get; set; }

        public string DeliveryAddress { get; set; }
    }
}