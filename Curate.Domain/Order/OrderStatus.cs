﻿namespace Curate.Domain.Order
{
    public enum OrderStatus : byte
    {
        Pending,
        AwaitingDispatch,
        Sent,
        Received,
        Completed,
        Failed,
        Canceled
    }
}