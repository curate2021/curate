namespace Curate.Domain.Base
{
    public interface ISoftDelete
    {
        public bool IsDeleted { get; }
    }
}