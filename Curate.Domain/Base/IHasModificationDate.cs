﻿using System;

namespace Curate.Domain.Base
{
    public interface IHasModificationDate
    {
        public DateTime ModificationDate { get; }
    }
}