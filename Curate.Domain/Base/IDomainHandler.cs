﻿namespace Curate.Domain.Base
{
    public interface IDomainHandler<in TIn, out TOut>
    {
        public TOut Handle(TIn input);
    }
}