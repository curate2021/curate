﻿namespace Curate.Domain.Base
{
    public interface IStateful<T>
    {
        void OnEnter();

        void Become(IStateful<T> next);
    }
}