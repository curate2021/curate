﻿using System;

namespace Curate.Domain.Base
{
    public interface IHasCreateDate
    {
        public DateTime CreateDate { get; }
    }
}