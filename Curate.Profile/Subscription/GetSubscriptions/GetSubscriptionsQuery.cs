﻿using Curate.Linq;
using Curate.Profile.Subscription.Common;
using Force.Cqrs;
using Force.Linq.Pagination;

namespace Curate.Profile.Subscription.GetSubscriptions
{
    public class GetSubscriptionsQuery : IQuery<PaginationResult<SubscriptionListItem>>, IPaging
    {
        public int Page { get; } = 1;

        public int Take { get; } = 10;
    }
}
