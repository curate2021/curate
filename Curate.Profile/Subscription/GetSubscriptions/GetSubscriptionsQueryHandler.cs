﻿using AutoMapper;
using AutoMapper.QueryableExtensions;
using Curate.Core;
using Curate.Domain.Entities;
using Curate.Linq;
using Curate.Profile.Subscription.Common;
using Force.Cqrs;
using Microsoft.EntityFrameworkCore;
using System.Linq;

namespace Curate.Profile.Subscription.GetSubscriptions
{
    public class GetSubscriptionsQueryHandler : IQueryHandler<GetSubscriptionsQuery, PaginationResult<SubscriptionListItem>>
    {
        private readonly IUserContext<User> _userContext;
        private readonly DbContext _dbContext;
        private readonly IConfigurationProvider _configurationProvider;

        public GetSubscriptionsQueryHandler(IUserContext<User> userContext, DbContext dbContext, IConfigurationProvider configurationProvider)
        {
            _userContext = userContext;
            _dbContext = dbContext;
            _configurationProvider = configurationProvider;
        }

        public PaginationResult<SubscriptionListItem> Handle(GetSubscriptionsQuery input) => _dbContext
                .Set<Domain.Entities.Profile>()
                .Where(p => p.Id == _userContext.UserId)
                .SelectMany(p => p.Subscribers.Select(s => s.From))
                .OrderBy(s => s.Name)
                .ThenBy(s => s.Surname)
                .ProjectTo<SubscriptionListItem>(_configurationProvider)
                .ToPaginationResult(input);
    }
}
