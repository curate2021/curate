﻿namespace Curate.Profile.Subscription.Common
{
    public class SubscriptionListItem
    {
        public int UserId { get; set; }

        public string FullName { get; set; }

        public string PhotoId { get; set; }

        public int FollowersCount { get; set; }

        public int FollowingCount { get; set; }
    }
}
