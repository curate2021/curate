﻿using System.Net;
using System.Threading.Tasks;
using Curate.Domain.Entities;
using Curate.Exceptions;
using Force.Cqrs;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;

namespace Curate.Profile.Subscription.Subscrube
{
    public class SubscribeCommandHandler : ICommandHandler<SubscribeCommand, Task<SubscriptionDetails>>
    {
        private readonly UserManager<User> _userManager;
        private readonly IHttpContextAccessor _contextAccessor;
        private readonly DbContext _dbContext;

        public SubscribeCommandHandler(UserManager<User> userManager, IHttpContextAccessor contextAccessor,
            DbContext dbContext)
        {
            _userManager = userManager;
            _contextAccessor = contextAccessor;
            _dbContext = dbContext;
        }

        public async Task<SubscriptionDetails> Handle(SubscribeCommand input)
        {
            var current = await _userManager.GetUserAsync(_contextAccessor.HttpContext.User);

            var target = await _userManager.FindByIdAsync(input.UserId.ToString()) ??
                         throw new HttpException(HttpStatusCode.NotFound, "User was not found.");

            if (current.Id == target.Id)
                throw new HttpException(HttpStatusCode.BadRequest, "User cannot subscribe to yourself.");

            var isSubscribed = await _dbContext
                .Set<Domain.Entities.Subscription>()
                .AnyAsync(s => s.FromId == current.Id && s.ToId == target.Id);

            if (isSubscribed)
                throw new HttpException(HttpStatusCode.BadRequest, "Subscription already exists.");

            var subscription = new Domain.Entities.Subscription(target.Id, current.Id);

            await _dbContext
                .Set<Domain.Entities.Subscription>()
                .AddAsync(subscription);

            await _dbContext.SaveChangesAsync();

            return new SubscriptionDetails
            {
                UserIdTo = subscription.ToId,
                UserIdFrom = subscription.FromId
            };
        }
    }
}