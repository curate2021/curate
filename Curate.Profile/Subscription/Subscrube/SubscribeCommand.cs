﻿using System.Threading.Tasks;
using Force.Cqrs;
using Microsoft.AspNetCore.Mvc;

namespace Curate.Profile.Subscription.Subscrube
{
    public class SubscribeCommand : ICommand<Task<SubscriptionDetails>>
    {
        [FromRoute]
        public int UserId { get; set; }
    }
}