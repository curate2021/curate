﻿namespace Curate.Profile.Subscription.Subscrube
{
    public class SubscriptionDetails
    {
        public int UserIdTo { get; set; }

        public int UserIdFrom { get; set; }
    }
}