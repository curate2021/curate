﻿using System.Threading.Tasks;
using Force.Cqrs;
using Microsoft.AspNetCore.Mvc;

namespace Curate.Profile.Subscription.Unsubscribe
{
    public class UnsubscribeCommand : ICommand<Task>
    {
        [FromRoute]
        public int UserId { get; set; }
    }
}