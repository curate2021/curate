﻿using System.Net;
using System.Threading.Tasks;
using Curate.Domain.Entities;
using Curate.Exceptions;
using Force.Cqrs;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;

namespace Curate.Profile.Subscription.Unsubscribe
{
    public class UnsubscribeCommandHandler : ICommandHandler<UnsubscribeCommand, Task>
    {
        private readonly UserManager<User> _userManager;
        private readonly IHttpContextAccessor _contextAccessor;
        private readonly DbContext _dbContext;

        public UnsubscribeCommandHandler(UserManager<User> userManager, IHttpContextAccessor contextAccessor,
            DbContext dbContext)
        {
            _userManager = userManager;
            _contextAccessor = contextAccessor;
            _dbContext = dbContext;
        }

        public async Task Handle(UnsubscribeCommand input)
        {
            var current = await _userManager.GetUserAsync(_contextAccessor.HttpContext.User);

            var target = await _userManager.FindByIdAsync(input.UserId.ToString()) ??
                         throw new HttpException(HttpStatusCode.NotFound, "User was not found.");

            if (current.Id == target.Id)
                throw new HttpException(HttpStatusCode.BadRequest, "Cannot apply for yourself.");

            var subscription = await _dbContext
                .Set<Domain.Entities.Subscription>()
                .FirstOrDefaultAsync(s => s.FromId == current.Id && s.ToId == target.Id);

            if (subscription == null)
                return;

            _dbContext
                .Set<Domain.Entities.Subscription>()
                .Remove(subscription);

            await _dbContext.SaveChangesAsync();
        }
    }
}