﻿using System.Threading.Tasks;
using Curate.Linq;
using Curate.Mvc;
using Curate.Mvc.ControllerHelpers;
using Curate.Profile.KYC.Add;
using Curate.Profile.KYC.Get;
using Curate.Profile.Profile.Get;
using Curate.Profile.Profile.GetBrief;
using Curate.Profile.Profile.Update;
using Curate.Profile.Subscription.Common;
using Curate.Profile.Subscription.GetSubscribers;
using Curate.Profile.Subscription.GetSubscriptions;
using Curate.Profile.Subscription.Subscrube;
using Curate.Profile.Subscription.Unsubscribe;
using Force.Cqrs;
using Force.Extensions;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace Curate.Profile
{
    [Route("/api/[controller]")]
    public class ProfileController : AuthControllerBase
    {
        [HttpGet("short")]
        [Produces(MimeTypes.Application.Json)]
        [ProducesResponseType(typeof(ShortProfileDetails), StatusCodes.Status200OK)]
        public async Task<IActionResult> Get(
            [FromServices] IQueryHandler<GetShortProfileQuery, Task<ShortProfileDetails>> handler)
            => Ok(await handler.Handle(new GetShortProfileQuery()));

        [HttpGet("short/{userId}")]
        [Produces(MimeTypes.Application.Json)]
        [ProducesResponseType(typeof(ShortProfileDetails), StatusCodes.Status200OK)]
        public async Task<IActionResult> Get([FromRoute] int userId,
            [FromServices] IQueryHandler<GetShortProfileQuery, Task<ShortProfileDetails>> handler)
            => Ok(await handler.Handle(new GetShortProfileQuery { UserId = userId }));

        [HttpGet]
        [Produces(MimeTypes.Application.Json)]
        [ProducesResponseType(typeof(ProfileDetails), StatusCodes.Status200OK)]
        [ProducesResponseType(typeof(ErrorResponse), StatusCodes.Status404NotFound)]
        public async Task<IActionResult> Get(
            [FromServices] IQueryHandler<GetProfileQuery, Task<ProfileDetails>> handler)
            => Ok(await handler.Handle(new GetProfileQuery()));

        [HttpPut]
        [Produces(MimeTypes.Application.Json)]
        [ProducesResponseType(typeof(ProfileUpdateDetails), StatusCodes.Status200OK)]
        [ProducesResponseType(typeof(ErrorResponse), StatusCodes.Status404NotFound)]
        public async Task<IActionResult> Put(
            [FromForm] UpdateProfileCommand command,
            [FromServices] ICommandHandler<UpdateProfileCommand, Task<ProfileUpdateDetails>> handler)
            => Ok(await handler.Handle(command));

        [HttpPost("subscribe/{userId}")]
        [Produces(MimeTypes.Application.Json)]
        [ProducesResponseType(typeof(SubscriptionDetails), StatusCodes.Status200OK)]
        [ProducesResponseType(typeof(ErrorResponse), StatusCodes.Status404NotFound)]
        public async Task<IActionResult> Subscribe(
            [FromRoute] SubscribeCommand command,
            [FromServices] ICommandHandler<SubscribeCommand, Task<SubscriptionDetails>> handler)
            => Ok(await handler.Handle(command));

        [HttpPost("unsubscribe/{userId}")]
        [Produces(MimeTypes.Application.Json)]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(typeof(ErrorResponse), StatusCodes.Status404NotFound)]
        public async Task<IActionResult> Unsubscribe(
            [FromRoute] UnsubscribeCommand command,
            [FromServices] ICommandHandler<UnsubscribeCommand, Task> handler)
        {
            await handler.Handle(command);
            return Ok();
        }

        [HttpGet("myfollowers")]
        [Produces(MimeTypes.Application.Json)]
        [ProducesResponseType(typeof(PaginationResult<SubscriptionListItem>), StatusCodes.Status200OK)]
        public ActionResult<PaginationResult<SubscriptionListItem>> Get(
            [FromQuery] GetSubscriptionsQuery query,
            [FromServices] IQueryHandler<GetSubscriptionsQuery, PaginationResult<SubscriptionListItem>> handler)
            => Ok(handler.Handle(query));

        [HttpGet("myfollowing")]
        [Produces(MimeTypes.Application.Json)]
        [ProducesResponseType(typeof(PaginationResult<SubscriptionListItem>), StatusCodes.Status200OK)]
        public ActionResult<PaginationResult<SubscriptionListItem>> Get(
            [FromQuery] GetSubscribersQuery query,
            [FromServices] IQueryHandler<GetSubscribersQuery, PaginationResult<SubscriptionListItem>> handler)
            => Ok(handler.Handle(query));

        [HttpPost("kyc")]
        [Produces(MimeTypes.Application.Json)]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        public async Task<IActionResult> AddKyc([FromForm] AddKycCommand command,
            [FromServices] AddKycCommandHandler handler)
        {
            await handler.Handle(command);
            return NoContent();
        }

        [HttpGet("kyc")]
        [ProducesResponseType(typeof(KycItem), StatusCodes.Status200OK)]
        public IActionResult GetKyc([FromServices] GetKycQueryHandler handler) =>
            handler.Handle(new GetKycQuery()).PipeTo(Ok);
    }
}