﻿using System.Linq;
using Curate.Domain.Entities;
using Curate.Profile.Profile.Get;
using Curate.Profile.Profile.GetBrief;
using Curate.Profile.Profile.Update;
using Curate.Profile.Subscription.Common;

namespace Curate.Profile
{
    public class Mapping : AutoMapper.Profile
    {
        public Mapping()
        {
            var userId = 0;
            CreateMap<Domain.Entities.Profile, ShortProfileDetails>()
                .ForMember(dest => dest.UserId, x => x.MapFrom(src => src.Id))
                .ForMember(dest => dest.AvatarId, x => x.MapFrom(src => src.Photo.Id))
                .ForMember(dest => dest.SubscriptionsCount, x => x.MapFrom(src => src.Subscriptions.Count))
                .ForMember(dest => dest.SubscribersCount, x => x.MapFrom(src => src.Subscribers.Count))
                .ForMember(dest => dest.IsSubscribed, x => x.MapFrom(src => src.Subscribers.Any(x => x.FromId == userId)));

            CreateMap<User, ProfileDetails>();

            CreateMap<Domain.Entities.Profile, ProfileDetails>();

            CreateMap<UpdateProfileCommand, User>()
                .ForMember(dest => dest.Profile, x => x.MapFrom(src => src))
                .ForAllMembers(opts => opts.Condition((src, dest, srcMember) => srcMember != null));

            CreateMap<UpdateProfileCommand, Domain.Entities.Profile>()
                .ForMember(dest => dest.Photo, x => x.Ignore())
                .ForAllMembers(opts => opts.Condition((src, dest, srcMember) => srcMember != null));

            CreateMap<Domain.Entities.Profile, SubscriptionListItem>()
                 .ForMember(dest => dest.UserId, x => x.MapFrom(src => src.Id))
                 .ForMember(dest => dest.FollowersCount, x => x.MapFrom(src => src.Subscribers.Count))
                 .ForMember(dest => dest.FollowingCount, x => x.MapFrom(src => src.Subscriptions.Count));
        }
    }
}