﻿using System.Threading.Tasks;
using Curate.Core;
using Curate.Domain.Entities;
using Force.Cqrs;
using Microsoft.EntityFrameworkCore;

namespace Curate.Profile.KYC.Add
{
    public class AddKycCommandHandler : ICommandHandler<AddKycCommand, Task>
    {
        private readonly IUserContext<User> _userContext;
        private readonly DbContext _dbContext;
        private readonly IFileClient _fileClient;

        public AddKycCommandHandler(IUserContext<User> userContext, DbContext dbContext, IFileClient fileClient)
        {
            _userContext = userContext;
            _dbContext = dbContext;
            _fileClient = fileClient;
        }

        public async Task Handle(AddKycCommand input)
        {
            var fileId = await _fileClient.SaveFile(input.Photo);

            var file = new File(fileId, FileType.KycDocument);

            _userContext.User.Profile.AddKyc(file);

            await _dbContext.SaveChangesAsync();
        }
    }
}