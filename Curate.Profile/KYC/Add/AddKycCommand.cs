﻿using System.Threading.Tasks;
using Force.Cqrs;
using Microsoft.AspNetCore.Http;

namespace Curate.Profile.KYC.Add
{
    public class AddKycCommand : ICommand<Task>
    {
        public IFormFile Photo { get; set; }
    }
}