﻿using Curate.Core;
using Curate.Domain.Entities;
using FluentValidation;
using Microsoft.AspNetCore.Http;

namespace Curate.Profile.KYC.Add
{
    public class AddKycCommandValidator : AbstractValidator<AddKycCommand>
    {
        public AddKycCommandValidator(IUserContext<User> userContext)
        {
            RuleFor(x => x.Photo)
                .NotNull()
                .NotEmpty();

            RuleFor(x => x)
                .Must(e => userContext.User.Profile.Kyc == null)
                .WithMessage("You already uploaded KYC")
                .WithErrorCode(StatusCodes.Status400BadRequest.ToString());
        }
    }
}