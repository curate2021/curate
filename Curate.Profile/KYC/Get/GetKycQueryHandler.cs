﻿using Curate.Core;
using Curate.Domain.Entities;
using Force.Cqrs;

namespace Curate.Profile.KYC.Get
{
    public class GetKycQueryHandler : IQueryHandler<GetKycQuery, KycItem>
    {
        private readonly IUserContext<User> _userContext;

        public GetKycQueryHandler(IUserContext<User> userContext)
        {
            _userContext = userContext;
        }

        public KycItem Handle(GetKycQuery input) =>
            new KycItem
            {
                PhotoId = _userContext.User.Profile.Kyc.PhotoId
            };
    }
}