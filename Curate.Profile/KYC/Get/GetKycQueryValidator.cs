﻿using Curate.Core;
using Curate.Domain.Entities;
using FluentValidation;
using Microsoft.AspNetCore.Http;

namespace Curate.Profile.KYC.Get
{
    public class GetKycQueryValidator : AbstractValidator<GetKycQuery>
    {
        public GetKycQueryValidator(IUserContext<User> userContext)
        {
            RuleFor(x => x)
                .Must(_ => userContext.User.Profile.Kyc != null)
                .WithErrorCode(StatusCodes.Status404NotFound.ToString());
        }
    }
}