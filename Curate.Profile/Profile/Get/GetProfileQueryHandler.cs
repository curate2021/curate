﻿using System.Net;
using System.Threading.Tasks;
using AutoMapper;
using Curate.Domain.Entities;
using Curate.Exceptions;
using Force.Cqrs;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;

namespace Curate.Profile.Profile.Get
{
    public class GetProfileQueryHandler : IQueryHandler<GetProfileQuery, Task<ProfileDetails>>
    {
        private readonly UserManager<User> _userManager;
        private readonly IHttpContextAccessor _contextAccessor;
        private readonly IMapper _mapper;

        public GetProfileQueryHandler(
            UserManager<User> userManager,
            IHttpContextAccessor contextAccessor,
            IMapper mapper)
        {
            _userManager = userManager;
            _contextAccessor = contextAccessor;
            _mapper = mapper;
        }

        public async Task<ProfileDetails> Handle(GetProfileQuery input)
        {
            var user = await _userManager.GetUserAsync(_contextAccessor.HttpContext.User);

            if (user == null)
                throw new HttpException(HttpStatusCode.NotFound, "User was not found.");

            return _mapper.Map(user.Profile, _mapper.Map<ProfileDetails>(user));
        }
    }
}