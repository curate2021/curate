﻿namespace Curate.Profile.Profile.Get
{
    public class ProfileDetails
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public string Surname { get; set; }

        public string Email { get; set; }

        public int? CountryId { get; set; }

        public string PhotoId { get; set; }
    }
}