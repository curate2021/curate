﻿using System.Linq;
using System.Net;
using System.Threading.Tasks;
using AutoMapper;
using AutoMapper.QueryableExtensions;
using Curate.Core;
using Curate.Domain.Entities;
using Curate.Exceptions;
using Force.Cqrs;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;

namespace Curate.Profile.Profile.GetBrief
{
    public class GetShortProfileQueryHandler : IQueryHandler<GetShortProfileQuery, Task<ShortProfileDetails>>
    {
        private readonly IUserContext<User> _userContext;
        private readonly DbContext _dbContext;
        private readonly IMapper _mapper;

        public GetShortProfileQueryHandler(
            IUserContext<User> userContext,
            DbContext dbContext,
            IMapper mapper)
        {
            _userContext = userContext;
            _dbContext = dbContext;
            _mapper = mapper;
        }

        public async Task<ShortProfileDetails> Handle(GetShortProfileQuery input)
        {
            if (!input.UserId.HasValue)
                return _mapper.Map<ShortProfileDetails>(_userContext.User.Profile);

            var user = await _dbContext.Set<User>()
                .Where(x => x.Id == input.UserId.Value)
                .Select(x => x.Profile)
                .ProjectTo<ShortProfileDetails>(_mapper.ConfigurationProvider, new {userId = _userContext.UserId})
                .FirstOrDefaultAsync();

            return user ?? throw new HttpException(HttpStatusCode.NotFound, "User was not found.");
        }
    }
}