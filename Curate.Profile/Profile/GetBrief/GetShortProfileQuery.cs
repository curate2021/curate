﻿using System.Threading.Tasks;
using Force.Cqrs;

namespace Curate.Profile.Profile.GetBrief
{
    public class GetShortProfileQuery : IQuery<Task<ShortProfileDetails>>
    {
        public int? UserId { get; set; }
    }
}