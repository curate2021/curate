﻿namespace Curate.Profile.Profile.GetBrief
{
    public class ShortProfileDetails
    {
        public string UserId { get; set; }

        public string AvatarId { get; set; }

        public int SubscriptionsCount { get; set; }

        public string FullName { get; set; }

        public int SubscribersCount { get; set; }

        public bool IsSubscribed { get; set; }
    }
}