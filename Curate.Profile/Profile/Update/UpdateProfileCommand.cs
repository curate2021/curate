﻿using System.ComponentModel.DataAnnotations;
using System.Threading.Tasks;
using Curate.Mvc;
using Force.Cqrs;
using Microsoft.AspNetCore.Http;

namespace Curate.Profile.Profile.Update
{
    public class UpdateProfileCommand : ICommand<Task<ProfileUpdateDetails>>
    {
        [MaxLength(byte.MaxValue)]
        public string Name { get; set; }

        [MaxLength(byte.MaxValue)]
        public string Surname { get; set; }

        [EmailAddress]
        [MaxLength(byte.MaxValue)]
        public string Email { get; set; }

        public int? CountryId { get; set; }

        [DataType(DataType.Upload)]
        [MaxFileSize(5 * 1024 * 1024)]
        [AllowedExtensions(".jpg", ".png")]
        public IFormFile Photo { get; set; }
    }
}