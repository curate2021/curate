﻿using System.Net;
using System.Threading.Tasks;
using AutoMapper;
using Curate.Core;
using Curate.Domain.Entities;
using Curate.Exceptions;
using Force.Cqrs;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;

namespace Curate.Profile.Profile.Update
{
    public class UpdateProfileCommandHandler : ICommandHandler<UpdateProfileCommand, Task<ProfileUpdateDetails>>
    {
        private readonly UserManager<User> _userManager;
        private readonly IHttpContextAccessor _contextAccessor;
        private readonly IFileClient _fileClient;
        private readonly IMapper _mapper;

        public UpdateProfileCommandHandler(
            UserManager<User> userManager,
            IHttpContextAccessor contextAccessor,
            IFileClient fileClient,
            IMapper mapper)
        {
            _userManager = userManager;
            _contextAccessor = contextAccessor;
            _fileClient = fileClient;
            _mapper = mapper;
        }

        public async Task<ProfileUpdateDetails> Handle(UpdateProfileCommand input)
        {
            var user = await _userManager.GetUserAsync(_contextAccessor.HttpContext.User);

            if (user == null)
                throw new HttpException(HttpStatusCode.NotFound, "User was not found.");

            _mapper.Map(input, user);

            if (input.Photo != null)
            {
                var photoId = await _fileClient.SaveFile(input.Photo);
                user.Profile.AddPhoto(new File(photoId, FileType.Image));
            }

            await _userManager.UpdateAsync(user);

            return new ProfileUpdateDetails {UserId = user.Id};
        }
    }
}