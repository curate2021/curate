﻿using System.Text.Json;
using System.Threading.Tasks;
using Curate.Exceptions;
using Curate.Mvc;
using Microsoft.AspNetCore.Http;

namespace Curate.Middlewares
{
    internal class HttpExceptionMiddleware : IMiddleware
    {
        public async Task InvokeAsync(HttpContext context, RequestDelegate next)
        {
            try
            {
                await next.Invoke(context);
            }
            catch (HttpException httpException)
            {
                context.Response.StatusCode = httpException.StatusCode;
                context.Response.ContentType = "application/json";
                await context.Response.WriteAsync(JsonSerializer.Serialize(new ErrorResponse(httpException.Message)));
            }
        }
    }
}