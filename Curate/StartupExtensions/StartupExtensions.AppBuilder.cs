﻿using Curate.Middlewares;
using Microsoft.AspNetCore.Builder;

namespace Curate.StartupExtensions
{
    public static partial class StartupExtensions
    {
        public static void UseHttpException(this IApplicationBuilder applicationBuilder)
            => applicationBuilder.UseMiddleware<HttpExceptionMiddleware>();
    }
}