﻿using System;
using System.Linq;
using Curate.Core;
using Curate.Core.Notification;
using Curate.DataAccess;
using Curate.DataAccess.Initializers;
using Curate.Domain.Base;
using Curate.EmailManager;
using Curate.EmailManager.Manager;
using Curate.EmailManager.Message;
using Curate.FileAccess;
using Curate.FirebaseMessaging;
using Curate.FirebaseMessaging.FirebaseClient;
using Curate.Identity;
using Curate.Middlewares;
using Curate.Wallet.Rpc;
using Curate.Wallet.Rpc.Options;
using Force.Cqrs;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace Curate.StartupExtensions
{
    public static partial class StartupExtensions
    {
        public static void ConfigureServices(this IServiceCollection services, IConfiguration configuration)
        {
            services.AddScoped<DbContext, CurateContext>();
            services.AddScoped<IUserContext<Domain.Entities.User>, UserContext<Domain.Entities.User>>();
            services.AddScoped<IRpcClient, RpcClient>();
            services.AddScoped<IEmailManager, EmailManager.Manager.EmailManager>();
            services.AddScoped<RpcHttpClient>();
            services.AddSingleton<IFirebaseClient, FirebaseClient>();
            
            services.RegisterOptions(configuration);

            services
                .Configure<StaticFilesOptions>(configuration.GetSection(nameof(StaticFilesOptions)))
                .AddScoped<IFileClient, LocalFileClient>();

            services.AddHttpExceptions();
            services.RegisterHandlers();
            services.RegisterAutoMapper();
            services.RegisterInitializers();
        }

        private static void RegisterOptions(this IServiceCollection services, IConfiguration configuration)
        {
            services.Configure<RpcOptions>(configuration.GetSection(nameof(RpcOptions)));
            services.Configure<FirebaseMessagingOptions>(configuration.GetSection(nameof(FirebaseMessagingOptions)));
            services.Configure<EmailManagerOptions>(configuration.GetSection(nameof(EmailManagerOptions)));
            services.Configure<EmailMessageOptions>(configuration.GetSection(nameof(EmailMessageOptions)));
            services.Configure<FirebaseNotificationOptions>(
                configuration.GetSection(nameof(FirebaseNotificationOptions)));
        }

        private static void RegisterInitializers(this IServiceCollection services)
        {
            services.AddAsyncInitializer<IdentityInitializer>();
            services.AddAsyncInitializer<StoreInitializer>();
            services.AddAsyncInitializer<NotificationsInitializer>();
            services.AddAsyncInitializer<RewardsInitializer>();
        }

        private static void RegisterHandlers(this IServiceCollection services)
        {
            Type[] autoRegistersHandlers =
            {
                typeof(IHandler<>),
                typeof(IHandler<,>),
                typeof(ICommandHandler<>),
                typeof(ICommandHandler<,>),
                typeof(IDomainHandler<,>),
                typeof(IQueryHandler<,>),
                typeof(INotificationHandler<>),
            };
            services.Scan(x => x.FromApplicationDependencies(xx => !xx.IsDynamic)
                .AddClasses(xx =>
                {
                    xx.AssignableToAny(autoRegistersHandlers);
                    xx.Where(xxx => !xxx.IsAbstract);
                })
                .AsSelfWithInterfaces()
                .WithScopedLifetime());
        }

        private static IServiceCollection RegisterAutoMapper(this IServiceCollection services)
        {
            var assemblies = AppDomain.CurrentDomain.GetAssemblies();

            var autoMapperAssemblies = assemblies
                .Where(x => x.FullName != null
                            && !x.FullName.Contains("Microsoft")
                            && !x.FullName.Contains("System")
                            && !x.FullName.Contains("IdentityServer4")
                            && !x.FullName.Contains("AutoMapper"))
                .SelectMany(x => x.GetTypes())
                .Where(x => !x.IsInterface)
                .Where(type => typeof(AutoMapper.Profile).IsAssignableFrom(type))
                .ToArray();

            services.AddAutoMapper(autoMapperAssemblies);

            return services;
        }

        private static IServiceCollection AddHttpExceptions(this IServiceCollection services)
            => services.AddTransient<HttpExceptionMiddleware>();
    }
}