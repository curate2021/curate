﻿using Curate.Auth.Infrastructure;
using Curate.Auth.Infrastructure.Identity;
using Curate.Auth.Infrastructure.Identity.Claims;
using Curate.Auth.Infrastructure.Identity.Jwt;
using Curate.DataAccess;
using Curate.DataAccess.Initializers;
using Curate.Domain;
using Curate.Domain.Entities;
using Curate.Identity;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;

namespace Curate.StartupExtensions
{
    public static partial class StartupExtensions
    {
        public static void ConfigureAuth(this IServiceCollection services, IConfiguration configuration,
            IWebHostEnvironment environment)
        {
            services.AddAuthorization(options =>
            {
                options.AddPolicy(PolicyConstants.AdminPolicy,
                    policy => policy.RequireRole(nameof(RoleNames.Admin)));
                options.AddPolicy(PolicyConstants.UserPolicy,
                    policy => policy.RequireRole(nameof(RoleNames.User)));
            });

            services.AddScoped<JwtTokenProvider>();
            services.AddScoped<ClaimsProvider>();
            services.Configure<AuthOptions>(configuration.GetSection("AuthOptions"));
            services.ConfigureOptions<ConfigureIdentityOptions>();
            services.ConfigureOptions<ConfigureJwtBearerOptions>();

            services.AddAuthentication(x =>
                {
                    x.DefaultAuthenticateScheme = JwtBearerDefaults.AuthenticationScheme;
                    x.DefaultChallengeScheme = JwtBearerDefaults.AuthenticationScheme;
                })
                .AddJwtBearer();

            services.AddIdentity<User, Role>()
                .AddEntityFrameworkStores<CurateContext>()
                .AddDefaultTokenProviders();

            if (environment.IsDevelopment())
            {
                services.AddAsyncInitializer<UserDataInitializer>();
            }
        }
    }
}