using System;
using System.IO;
using System.Text.Json.Serialization;
using Curate.Auth.Features.SignUp;
using Curate.DataAccess;
using Curate.FileAccess;
using Curate.Interest.Handlers.AddUserInterests;
using Curate.Listing.Listing.CreateListing;
using Curate.Order.ConfirmOrderDelivered;
using Curate.StartupExtensions;
using FluentValidation.AspNetCore;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.HttpOverrides;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.FileProviders;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Options;
using Serilog;

namespace Curate
{
    public class Startup
    {
        public Startup(IConfiguration configuration, IWebHostEnvironment environment)
        {
            Configuration = configuration;
            Environment = environment;
        }

        public IConfiguration Configuration { get; }
        public IWebHostEnvironment Environment { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddHealthChecks();
            services.AddDbContext<CurateContext>(options =>
            {
                options.UseLazyLoadingProxies();
                options.UseNpgsql(Configuration.GetConnectionString("DefaultConnection"));
            });

            services.ConfigureServices(Configuration);
            services.ConfigureAuth(Configuration, Environment);
            services.AddHttpClient("Rpc", c => c.BaseAddress = new Uri(Configuration["RpcOptions:EndPoint"]));

            services.AddRouting(options => options.LowercaseUrls = true);
            services.AddControllers()
                .AddJsonOptions(options =>
                {
                    options.JsonSerializerOptions.Converters.Add(new JsonStringEnumConverter());
                })
                .AddFluentValidation(options =>
                {
                    options.RegisterValidatorsFromAssemblyContaining<SignUpCommandValidator>();
                    options.RegisterValidatorsFromAssemblyContaining<AddUserInterestsCommandValidator>();
                    options.RegisterValidatorsFromAssemblyContaining<ConfirmOrderReceiveCommandValidator>();
                    options
                        .RegisterValidatorsFromAssemblyContaining<CreateListingCommandValidator>();
                });
            services.Configure<ApiBehaviorOptions>(options => options.SuppressModelStateInvalidFilter = true);
            services.ConfigureSwagger();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env,
            IOptions<StaticFilesOptions> staticFilesOptions)
        {
            app.UseDeveloperExceptionPage();

            app.UseSwagger();
            app.UseSwaggerUI(c => c.SwaggerEndpoint("/swagger/v1/swagger.json", "Curate v1"));

            app.UseHttpException();
            app.UseForwardedHeaders(new ForwardedHeadersOptions {
                ForwardedHeaders = ForwardedHeaders.XForwardedFor | ForwardedHeaders.XForwardedProto
            });
            
            app.UseRouting();

            app.UseAuthentication();
            app.UseAuthorization();

            app.UseStaticFiles(new StaticFileOptions
            {
                FileProvider = new PhysicalFileProvider(Path.Combine(env.ContentRootPath, staticFilesOptions.Value.LocationPath)),
                RequestPath = staticFilesOptions.Value.RequestPath
            });

            app.UseSerilogRequestLogging();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
                endpoints.MapHealthChecks("/health");
            });
        }
    }
}