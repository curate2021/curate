﻿using AutoMapper;
using Curate.Domain.Base;
using Curate.Domain.Order;
using Curate.Order.Common;
using Force.Cqrs;
using Microsoft.EntityFrameworkCore;

namespace Curate.Order.CreateOrder
{
    public class CreateOrderCommandHandler : ICommandHandler<CreateOrderCommand, OrderStateChangeResult>
    {
        private readonly DbContext _dbContext;
        private readonly IMapper _mapper;
        private readonly IDomainHandler<CreateOrderInfo, Domain.Order.Order> _domainHandler;

        public CreateOrderCommandHandler(DbContext dbContext,
            IMapper mapper,
            IDomainHandler<CreateOrderInfo, Domain.Order.Order> domainHandler)
        {
            _dbContext = dbContext;
            _mapper = mapper;
            _domainHandler = domainHandler;
        }

        public OrderStateChangeResult Handle(CreateOrderCommand input)
        {
            var order = _domainHandler.Handle(_mapper.Map<CreateOrderInfo>(input));

            _dbContext.Set<Domain.Order.Order>().Add(order);
            _dbContext.SaveChanges();

            return new OrderStateChangeResult
            {
                OrderId = order.Id
            };
        }
    }
}