﻿namespace Curate.Order.CreateOrder
{
    public class CreateOrderResult
    {
        public int OrderId { get; set; }
    }
}