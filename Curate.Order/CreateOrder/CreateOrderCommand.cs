﻿using System.ComponentModel.DataAnnotations;
using Curate.Order.Common;
using Force.Cqrs;

namespace Curate.Order.CreateOrder
{
    public class CreateOrderCommand : ICommand<OrderStateChangeResult>
    {
        public int ListingId { get; set; }

        public int PaymentMethodId { get; set; }

        [Range(0, int.MaxValue)]
        [Required]
        public int Quantity { get; set; } = 1;

        [Required]
        public string DeliveryAddress { get; set; }
    }
}