﻿using System;
using System.Text.Json.Serialization;
using Curate.Domain.Base;
using Curate.Domain.Order;
using Force.Ddd;

namespace Curate.Order.Common
{
    public class OrderItemBase : IHasId<int>, IHasCreateDate
    {
        object IHasId.Id => Id;

        [JsonPropertyName("orderId")]
        public int Id { get; set; }

        public string Number { get; set; }

        public string TrackingNumber { get; set; }

        public OrderStatus OrderStatus { get; set; }

        public DateTime CreateDate { get; set; }

        public decimal TotalAmount { get; set; }
    }
}