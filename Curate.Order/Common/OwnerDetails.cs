﻿using Force.Ddd;

namespace Curate.Order.Common
{
    public class OwnerDetails : IHasId<int>
    {
        object IHasId.Id => Id;

        public int Id { get; set; }

        public string PhotoId { get; set; }

        public string FullName { get; set; }
    }
}