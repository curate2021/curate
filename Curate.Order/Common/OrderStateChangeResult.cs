﻿namespace Curate.Order.Common
{
    public class OrderStateChangeResult
    {
        public int OrderId { get; set; }
    }
}
