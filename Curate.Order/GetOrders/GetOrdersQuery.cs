﻿using Curate.Linq;
using Force.Cqrs;
using Force.Linq.Pagination;

namespace Curate.Order.GetOrders
{
    public class GetOrdersQuery : IQuery<PaginationResult<OrderItem>>, IPaging
    {
        public int Page { get; set; } = 1;

        public int Take { get; set; } = 10;
    }
}