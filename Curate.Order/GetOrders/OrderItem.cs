﻿using Curate.Order.Common;

namespace Curate.Order.GetOrders
{
    public class OrderItem : OrderItemBase
    {
        public OrderListingDetails ListingDetails { get; set; }

        public OwnerDetails ListingOwnerDetails { get; set; }

        public decimal DeliveryCost { get; set; }

        public string ListingIsUnavailableMessage { get; set; }
    }
}