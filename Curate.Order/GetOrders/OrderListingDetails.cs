﻿using System.Collections.Generic;
using Force.Ddd;

namespace Curate.Order.GetOrders
{
    public class OrderListingDetails : IHasId<int>
    {
        object IHasId.Id => Id;

        public int Id { get; set; }

        public IEnumerable<string> ImageIds { get; set; }

        public string Title { get; set; }
    }
}