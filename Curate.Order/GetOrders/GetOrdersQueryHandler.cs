﻿using System.Linq;
using AutoMapper;
using AutoMapper.QueryableExtensions;
using Curate.Core;
using Curate.Domain.Entities;
using Curate.Linq;
using Force.Cqrs;
using Microsoft.EntityFrameworkCore;

namespace Curate.Order.GetOrders
{
    public class GetOrdersQueryHandler : IQueryHandler<GetOrdersQuery, PaginationResult<OrderItem>>
    {
        private readonly DbContext _dbContext;
        private readonly IConfigurationProvider _configurationProvider;
        private readonly IUserContext<User> _userContext;

        public GetOrdersQueryHandler(DbContext dbContext, IConfigurationProvider configurationProvider,
            IUserContext<User> userContext)
        {
            _dbContext = dbContext;
            _configurationProvider = configurationProvider;
            _userContext = userContext;
        }

        public PaginationResult<OrderItem> Handle(GetOrdersQuery input)
        {
            return _dbContext
                .Set<Domain.Order.Order>()
                .Include(o => o.Listing)
                .ThenInclude(o => o.Files)
                .IgnoreQueryFilters()
                .Where(Domain.Order.Order.IsBuyer(_userContext.UserId))
                .ProjectTo<OrderItem>(_configurationProvider)
                .OrderByDescending(o => o.CreateDate)
                .ToPaginationResult(input);
        }
    }
}