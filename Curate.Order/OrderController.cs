﻿using System.Threading.Tasks;
using Curate.Linq;
using Curate.Mvc;
using Curate.Mvc.ControllerHelpers;
using Curate.Order.Checkout;
using Curate.Order.Common;
using Curate.Order.ConfirmOrderDelivered;
using Curate.Order.CreateOrder;
using Curate.Order.GetListingOrders;
using Curate.Order.GetOrder;
using Curate.Order.GetOrders;
using Curate.Order.SentOrder;
using Force.Cqrs;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace Curate.Order
{
    [Route("api/[controller]")]
    public class OrderController : AuthControllerBase
    {
        [HttpGet("list")]
        [Produces(MimeTypes.Application.Json)]
        [ProducesResponseType(typeof(PaginationResult<OrderItem>), StatusCodes.Status200OK)]
        public IActionResult List(
            [FromQuery] GetOrdersQuery query,
            [FromServices] IQueryHandler<GetOrdersQuery, PaginationResult<OrderItem>> handler)
            => Ok(handler.Handle(query));

        [HttpGet("list/{listingId}")]
        [Produces(MimeTypes.Application.Json)]
        [ProducesResponseType(typeof(PaginationResult<ListingOrderItem>), StatusCodes.Status200OK)]
        [ProducesResponseType(typeof(ErrorResponse), StatusCodes.Status404NotFound)]
        public IActionResult ListingOrdersList(
            [FromRoute] int listingId,
            [FromQuery] GetListingOrdersQuery query,
            [FromServices] IQueryHandler<GetListingOrdersQuery, PaginationResult<ListingOrderItem>> handler)
        {
            query.ListingId = listingId;
            return Ok(handler.Handle(query));
        }

        [HttpGet("checkout/{listingId}/{paymentMethodId}")]
        [Produces(MimeTypes.Application.Json)]
        [ProducesResponseType(typeof(CheckoutDetails), StatusCodes.Status200OK)]
        [ProducesResponseType(typeof(ErrorResponse), StatusCodes.Status404NotFound)]
        public IActionResult Checkout(
            [FromRoute] int listingId,
            [FromRoute] int paymentMethodId,
            [FromQuery] GetCheckoutDetailsQuery query,
            [FromServices] IQueryHandler<GetCheckoutDetailsQuery, CheckoutDetails> handler)
        {
            query.ListingId = listingId;
            query.PaymentMethodId = paymentMethodId;
            return Ok(handler.Handle(query));
        }

        [HttpGet("{orderId}")]
        [Produces(MimeTypes.Application.Json)]
        [ProducesResponseType(typeof(OrderItem), StatusCodes.Status200OK)]
        [ProducesResponseType(typeof(ErrorResponse), StatusCodes.Status404NotFound)]
        public IActionResult Get(
            [FromRoute] int orderId,
            [FromServices] IQueryHandler<GetOrderQuery, OrderItem> handler)
            => Ok(handler.Handle(new GetOrderQuery {OrderId = orderId}));

        [HttpPost]
        [Produces(MimeTypes.Application.Json)]
        [ProducesResponseType(typeof(OrderStateChangeResult), StatusCodes.Status200OK)]
        [ProducesResponseType(typeof(ErrorResponse), StatusCodes.Status404NotFound)]
        public IActionResult Post(
            [FromBody] CreateOrderCommand command,
            [FromServices] ICommandHandler<CreateOrderCommand, OrderStateChangeResult> handler)
            => Ok(handler.Handle(command));

        [HttpPost("sent/{orderId}")]
        [Produces(MimeTypes.Application.Json)]
        [ProducesResponseType(typeof(OrderStateChangeResult), StatusCodes.Status200OK)]
        [ProducesResponseType(typeof(ErrorResponse), StatusCodes.Status404NotFound)]
        public IActionResult Sent(
            [FromRoute] int orderId,
            [FromBody] SentOrderCommand command,
            [FromServices] ICommandHandler<SentOrderCommand, OrderStateChangeResult> handler)
        {
            command.OrderId = orderId;
            return Ok(handler.Handle(command));
        }

        [HttpPost("approve/{orderId}")]
        [Produces(MimeTypes.Application.Json)]
        [ProducesResponseType(typeof(OrderStateChangeResult), StatusCodes.Status200OK)]
        [ProducesResponseType(typeof(ErrorResponse), StatusCodes.Status404NotFound)]
        public async Task<IActionResult> Approve(
            [FromRoute] ConfirmOrderReceiveCommand command,
            [FromServices] ICommandHandler<ConfirmOrderReceiveCommand, Task<OrderStateChangeResult>> handler)
        {
            return Ok(await handler.Handle(command));
        }
    }
}