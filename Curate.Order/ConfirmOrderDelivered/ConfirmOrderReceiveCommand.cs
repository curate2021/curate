﻿using System.Threading.Tasks;
using Curate.Order.Common;
using Force.Cqrs;

namespace Curate.Order.ConfirmOrderDelivered
{
    public class ConfirmOrderReceiveCommand : ICommand<Task<OrderStateChangeResult>>
    {
        public int OrderId { get; set; }
    }
}