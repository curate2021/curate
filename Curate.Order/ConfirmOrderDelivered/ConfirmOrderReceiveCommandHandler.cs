﻿using System.Linq;
using System.Threading.Tasks;
using Curate.Core;
using Curate.Domain.Entities;
using Curate.Domain.Entities.Reward;
using Curate.Order.Common;
using Force.Cqrs;
using Force.Linq;
using Microsoft.EntityFrameworkCore;

namespace Curate.Order.ConfirmOrderDelivered
{
    public class
        ConfirmOrderReceiveCommandHandler : ICommandHandler<ConfirmOrderReceiveCommand, Task<OrderStateChangeResult>>
    {
        private readonly DbContext _dbContext;
        private readonly IUserContext<User> _userContext;

        public ConfirmOrderReceiveCommandHandler(DbContext dbContext, IUserContext<User> userContext)
        {
            _dbContext = dbContext;
            _userContext = userContext;
        }

        public async Task<OrderStateChangeResult> Handle(ConfirmOrderReceiveCommand input)
        {
            var order = _dbContext
                .Set<Domain.Order.Order>()
                .IgnoreQueryFilters()
                .Where(Domain.Order.Order.IsAvailableListing)
                .Where(Domain.Order.Order.IsBuyer(_userContext.UserId))
                .FirstOrDefaultById(input.OrderId);

            order.State.Receive();

            var orderRewardRateInfo = await _dbContext.Set<OrderRewardRate>()
                .FirstOrDefaultAsync();

            var orderRewards = OrderReward.CreateOrderRewards(order, orderRewardRateInfo);

            await _dbContext.Set<OrderReward>()
                .AddRangeAsync(orderRewards);

            await _dbContext.SaveChangesAsync();

            return new OrderStateChangeResult {OrderId = order.Id};
        }
    }
}