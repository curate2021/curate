﻿using System.Net;
using Curate.Core;
using Curate.Domain.Entities;
using Curate.Exceptions;
using FluentValidation;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;

namespace Curate.Order.ConfirmOrderDelivered
{
    public class ConfirmOrderReceiveCommandValidator : AbstractValidator<ConfirmOrderReceiveCommand>
    {
        public ConfirmOrderReceiveCommandValidator(DbContext dbContext, IUserContext<User> userContext)
        {
            RuleFor(x => x.OrderId)
                .NotEmpty();

            RuleFor(x => x)
                .MustAsync(async (e, ct) =>
                {
                    var order = await dbContext.FindAsync<Domain.Order.Order>(e.OrderId);
                    if (order == null)
                    {
                        throw new HttpException(HttpStatusCode.NotFound);
                    }
                    return order.BuyerId == userContext.UserId;
                })
                .WithErrorCode(StatusCodes.Status403Forbidden.ToString());
        }
    }
}