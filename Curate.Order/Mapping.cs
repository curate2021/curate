﻿using System;
using Curate.Domain.Listing;
using Curate.Domain.Order;
using Curate.Domain.Order.Calculation;
using Curate.Order.Checkout;
using Curate.Order.Common;
using Curate.Order.CreateOrder;
using Curate.Order.GetListingOrders;
using Curate.Order.GetOrders;
using Profile = AutoMapper.Profile;

namespace Curate.Order
{
    public class Mapping : Profile
    {
        public Mapping()
        {
            CreateMap<Domain.Entities.Profile, OwnerDetails>();

            CreateMap<Listing, OrderListingDetails>();

            CreateMap<Domain.Order.Order, OrderItemBase>()
                .ForMember(dest => dest.OrderStatus, x => x.MapFrom(src => src.Status))
                .ForMember(dest => dest.TotalAmount, x => x.MapFrom(src => src.Payment.Amount));

            CreateMap<Domain.Order.Order, OrderItem>()
                .IncludeBase<Domain.Order.Order, OrderItemBase>()
                .ForMember(dest => dest.DeliveryCost, x => x.MapFrom(src => src.DeliveryCountry.Cost))
                .ForMember(dest => dest.ListingOwnerDetails,
                    opt => opt.MapFrom(src => !src.Listing.IsDeleted ? src.Listing.Seller : null))
                .ForMember(dest => dest.ListingDetails,
                    opt => opt.MapFrom(src => !src.Listing.IsDeleted ? src.Listing : null))
                .ForMember(dest => dest.ListingIsUnavailableMessage,
                    opt => opt.MapFrom(src => !src.Listing.IsDeleted ? null : "Listing is unavailable"));

            CreateMap<Domain.Order.Order, ListingOrderItem>()
                .IncludeBase<Domain.Order.Order, OrderItemBase>()
                .ForMember(dest => dest.DeliveryCountry, x => x.MapFrom(src => src.DeliveryCountry.Country.Name))
                .ForMember(dest => dest.OrderOwnerDetails, x => x.MapFrom(src => src.Buyer.Profile));

            CreateMap<GetCheckoutDetailsQuery, CreateOrderInfo>()
                .ForMember(dest => dest.ListingId, x => x.MapFrom(src => src.ListingId))
                .ForMember(dest => dest.PaymentMethodId, x => x.MapFrom(src => src.PaymentMethodId))
                .ForMember(dest => dest.Quantity, x => x.MapFrom(src => src.Quantity));

            CreateMap<Listing, CheckoutDetails>()
                .ForMember(dest => dest.Images, x => x.MapFrom(src => src.ImageIds));

            CreateMap<CalculationResult, CheckoutDetails>()
                .ForMember(dest => dest.Fee, x => x.MapFrom(src => src.FeeAmount))
                .ForMember(dest => dest.Total, x => x.MapFrom(src => src.TotalAmount));

            CreateMap<CreateOrderCommand, CreateOrderInfo>();
        }
    }
}