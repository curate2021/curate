﻿using AutoMapper;
using Curate.Domain.Base;
using Curate.Domain.Order;
using Curate.Domain.Order.Calculation;
using Force.Cqrs;

namespace Curate.Order.Checkout
{
    public class GetCheckoutDetailsQueryHandler : IQueryHandler<GetCheckoutDetailsQuery, CheckoutDetails>
    {
        private readonly IMapper _mapper;
        private readonly IDomainHandler<Domain.Order.Order, CalculationResult> _orderCalculateHandler;
        private readonly IDomainHandler<CreateOrderInfo, Domain.Order.Order> _orderCreateHandler;

        public GetCheckoutDetailsQueryHandler(IMapper mapper,
            IDomainHandler<Domain.Order.Order, CalculationResult> orderCalculateHandler,
            IDomainHandler<CreateOrderInfo, Domain.Order.Order> orderCreateHandler)
        {
            _mapper = mapper;
            _orderCalculateHandler = orderCalculateHandler;
            _orderCreateHandler = orderCreateHandler;
        }

        public CheckoutDetails Handle(GetCheckoutDetailsQuery input)
        {
            var order = _orderCreateHandler.Handle(_mapper.Map<CreateOrderInfo>(input));
            var checkoutDetails = _mapper.Map<CheckoutDetails>(_orderCalculateHandler.Handle(order));
            return _mapper.Map(order.Listing, checkoutDetails);
        }
    }
}