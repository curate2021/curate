﻿using System.Collections.Generic;
using System.Text.Json.Serialization;

namespace Curate.Order.Checkout
{
    public class CheckoutDetails
    {
        [JsonPropertyName("ListingId")]
        public int Id { get; set; }

        public IEnumerable<string> Images { get; set; }

        public string Title { get; set; }

        public string Description { get; set; }

        public decimal Price { get; set; }

        public decimal DeliveryCost { get; set; }

        public decimal Fee { get; set; }

        public decimal FeePercent { get; set; }

        public decimal Total { get; set; }
    }
}