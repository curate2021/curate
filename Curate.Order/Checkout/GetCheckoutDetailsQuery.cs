﻿using System.ComponentModel.DataAnnotations;
using Force.Cqrs;

namespace Curate.Order.Checkout
{
    public class GetCheckoutDetailsQuery : IQuery<CheckoutDetails>
    {
        internal int ListingId { get; set; }

        internal int PaymentMethodId { get; set; }

        [Range(0, int.MaxValue)]
        [Required]
        public int Quantity { get; set; } = 1;
    }
}