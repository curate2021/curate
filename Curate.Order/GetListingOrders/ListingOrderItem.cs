﻿using Curate.Order.Common;

namespace Curate.Order.GetListingOrders
{
    public class ListingOrderItem : OrderItemBase
    {
        public string DeliveryAddress { get; set; }

        public string DeliveryCountry { get; set; }

        public OwnerDetails OrderOwnerDetails { get; set; }
    }
}