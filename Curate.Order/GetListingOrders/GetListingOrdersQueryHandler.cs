﻿using System.Linq;
using System.Net;
using AutoMapper;
using AutoMapper.QueryableExtensions;
using Curate.Core;
using Curate.Domain.Entities;
using Curate.Domain.Listing;
using Curate.Exceptions;
using Curate.Linq;
using Curate.Order.Common;
using Force.Cqrs;
using Force.Linq;
using Microsoft.EntityFrameworkCore;

namespace Curate.Order.GetListingOrders
{
    public class GetListingOrdersQueryHandler : IQueryHandler<GetListingOrdersQuery, PaginationResult<ListingOrderItem>>
    {
        private readonly DbContext _dbContext;
        private readonly IConfigurationProvider _configurationProvider;
        private readonly IUserContext<User> _userContext;

        public GetListingOrdersQueryHandler(DbContext dbContext, IConfigurationProvider configurationProvider,
            IUserContext<User> userContext)
        {
            _dbContext = dbContext;
            _configurationProvider = configurationProvider;
            _userContext = userContext;
        }

        public PaginationResult<ListingOrderItem> Handle(GetListingOrdersQuery input)
        {
            var listing = _dbContext.Set<Listing>()
                              .IgnoreQueryFilters()
                              .Include(l => l.Files)
                              .Where(Listing.IsNotDeleted)
                              .Where(Listing.IsSeller(_userContext.UserId))
                              .FirstOrDefaultById(input.ListingId) ??
                          throw new HttpException(HttpStatusCode.NotFound, "Listing for current user not found.");

            return _dbContext
                .Entry(listing)
                .Collection(l => l.Orders)
                .Query()
                .Include(o => o.Buyer)
                .ThenInclude(b => b.Profile)
                .ProjectTo<ListingOrderItem>(_configurationProvider)
                .OrderByDescending(o => o.CreateDate)
                .ToPaginationResult(input);
        }
    }
}