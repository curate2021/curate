﻿using Curate.Linq;
using Curate.Order.Common;
using Force.Cqrs;
using Force.Linq.Pagination;

namespace Curate.Order.GetListingOrders
{
    public class GetListingOrdersQuery : IQuery<PaginationResult<ListingOrderItem>>, IPaging
    {
        internal int ListingId { get; set; }

        public int Page { get; } = 1;

        public int Take { get; } = 10;
    }
}