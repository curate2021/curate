﻿using System.ComponentModel.DataAnnotations;
using Curate.Order.Common;
using Force.Cqrs;

namespace Curate.Order.SentOrder
{
    public class SentOrderCommand : ICommand<OrderStateChangeResult>
    {
        internal int OrderId { get; set; }

        [Required]
        public string TrackingNumber { get; set; }
    }
}