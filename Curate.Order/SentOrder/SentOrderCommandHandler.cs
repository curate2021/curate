﻿using System.Linq;
using System.Net;
using Curate.Core;
using Curate.Domain.Entities;
using Curate.Domain.Order;
using Curate.Exceptions;
using Curate.Order.Common;
using Force.Cqrs;
using Force.Linq;
using Microsoft.EntityFrameworkCore;

namespace Curate.Order.SentOrder
{
    public class SentOrderCommandHandler : ICommandHandler<SentOrderCommand, OrderStateChangeResult>
    {
        private readonly DbContext _dbContext;
        private readonly IUserContext<User> _userContext;

        public SentOrderCommandHandler(DbContext dbContext, IUserContext<User> userContext)
        {
            _dbContext = dbContext;
            _userContext = userContext;
        }

        public OrderStateChangeResult Handle(SentOrderCommand input)
        {
            var order = _dbContext
                            .Set<Domain.Order.Order>()
                            .IgnoreQueryFilters()
                            .Where(Domain.Order.Order.IsAvailableListing)
                            .Where(Domain.Order.Order.IsListingSeller(_userContext.UserId))
                            .FirstOrDefaultById(input.OrderId) ??
                        throw new HttpException(HttpStatusCode.NotFound, "Order was not found.");

            if (!order.CheckInStatus(OrderStatus.AwaitingDispatch))
                throw new HttpException(HttpStatusCode.BadRequest, "Order has not available state for sending.");

            order.Sent(input.TrackingNumber);

            _dbContext.SaveChanges();

            return new OrderStateChangeResult {OrderId = order.Id};
        }
    }
}