﻿using System.Linq;
using System.Net;
using AutoMapper;
using AutoMapper.QueryableExtensions;
using Curate.Core;
using Curate.Domain.Entities;
using Curate.Exceptions;
using Curate.Order.GetOrders;
using Force.Cqrs;
using Force.Linq;
using Microsoft.EntityFrameworkCore;

namespace Curate.Order.GetOrder
{
    public class GetOrderQueryHandler : IQueryHandler<GetOrderQuery, OrderItem>
    {
        private readonly DbContext _dbContext;
        private readonly IConfigurationProvider _configurationProvider;
        private readonly IUserContext<User> _userContext;

        public GetOrderQueryHandler(DbContext dbContext, IConfigurationProvider configurationProvider,
            IUserContext<User> userContext)
        {
            _dbContext = dbContext;
            _configurationProvider = configurationProvider;
            _userContext = userContext;
        }

        public OrderItem Handle(GetOrderQuery input)
            => _dbContext
                   .Set<Domain.Order.Order>()
                   .Include(o => o.Listing)
                   .ThenInclude(o => o.Files)
                   .IgnoreQueryFilters()
                   .Where(Domain.Order.Order.IsAvailableListing)
                   .Where(Domain.Order.Order.IsListingSeller(_userContext.UserId) || Domain.Order.Order.IsBuyer(_userContext.UserId))
                   .ProjectTo<OrderItem>(_configurationProvider)
                   .FirstOrDefaultById(input.OrderId) ??
               throw new HttpException(HttpStatusCode.NotFound, "Order was not found.");
    }
}