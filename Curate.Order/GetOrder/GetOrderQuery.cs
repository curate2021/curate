﻿using Curate.Order.GetOrders;
using Force.Cqrs;

namespace Curate.Order.GetOrder
{
    public class GetOrderQuery : IQuery<OrderItem>
    {
        public int OrderId { get; set; }
    }
}