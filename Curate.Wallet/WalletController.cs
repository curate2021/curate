﻿using System.Threading.Tasks;
using Curate.Identity;
using Curate.Mvc;
using Curate.Mvc.ControllerHelpers;
using Curate.Wallet.Rpc;
using Curate.Wallet.Rpc.Dtos;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace Curate.Wallet
{
    [Authorize(Policy = PolicyConstants.UserPolicy, AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
    public class WalletController : AuthControllerBase
    {
        private readonly IRpcClient _rpcClient;

        public WalletController(IRpcClient rpcClient)
        {
            _rpcClient = rpcClient;
        }

        [HttpGet]
        [ProducesResponseType(typeof(RpcAccountWalletInfo), StatusCodes.Status200OK)]
        public async Task<IActionResult> Get([FromQuery] RpcUserInfoBase query) => Ok(await _rpcClient.GetWallet(query));

        [HttpPost]
        [Produces(MimeTypes.Application.Json)]
        [ProducesResponseType(typeof(RpcWalletInfo), StatusCodes.Status200OK)]
        public async Task<IActionResult> Post(RpcUserInfoBase query) => Ok(await _rpcClient.CreateWallet(query));

        [HttpPost]
        [Produces(MimeTypes.Application.Json)]
        [ProducesResponseType(typeof(RpcUserInfoBase), StatusCodes.Status200OK)]
        public async Task<IActionResult> SignUp() =>
            Ok(await _rpcClient.SignUp());
    }
}