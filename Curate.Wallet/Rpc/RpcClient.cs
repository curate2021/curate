﻿using System;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using Curate.Core;
using Curate.Domain.Entities;
using Curate.Exceptions;
using Curate.Wallet.Rpc.Dtos;
using Curate.Wallet.Rpc.Options;
using Microsoft.Extensions.Options;
using Serilog;

namespace Curate.Wallet.Rpc
{
    public class RpcClient : IRpcClient
    {
        private readonly RpcHttpClient _rpcHttpClient;
        private readonly RpcOptions _rpcOptions;
        private readonly IUserContext<User> _userContext;
        private readonly ILogger _logger;

        public RpcClient(IOptions<RpcOptions> rpcOptions, RpcHttpClient rpcHttpClient,
            IUserContext<User> userContext, ILogger logger)
        {
            _rpcHttpClient = rpcHttpClient;
            _userContext = userContext;
            _logger = logger;
            _rpcOptions = rpcOptions.Value;
        }

        public async Task<RpcUserInfoBase> SignUp()
        {
            var info = new RpcUserInfo {Email = _userContext.User.Email};
            ValidateProperties(new[] {nameof(info.Email)}, info.Email);

            return await TryCatch(async () =>
                    await _rpcHttpClient.Post<RpcUserInfoBase>(_rpcOptions.SignUpPath, info),
                "Cannot create access key in crypto system");
        }

        public async Task<RpcUserInfo> SignIn(RpcUserInfo info)
        {
            ValidateProperties(new[] {nameof(info.Email), nameof(info.AccessKey)}, info.Email, info.AccessKey);

            return await TryCatch(
                async () => await _rpcHttpClient.Post<RpcUserInfo>(_rpcOptions.SignInPath, info,
                    info.AccessKey),
                "Cannot sign in to wallet");
        }

        public async Task<RpcWalletInfo> CreateWallet(RpcUserInfoBase info)
        {
            var rpcUserInfo = new RpcUserInfo()
            {
                Email = _userContext.User.Email,
                AccessKey = info.AccessKey
            };

            ValidateProperties(new[] {nameof(rpcUserInfo.Email), nameof(rpcUserInfo.AccessKey)}, rpcUserInfo.Email,
                rpcUserInfo.AccessKey);

            return await TryCatch(async () => await _rpcHttpClient.Post<RpcWalletInfo>(
                _rpcOptions.CreateWalletPath, info,
                info.AccessKey), "Cannot create wallet");
        }

        public async Task<RpcAccountWalletInfo> GetWallet(RpcUserInfoBase info)
        {
            var accessKey = info.AccessKey;
            ValidateProperties(new[] {nameof(accessKey)}, accessKey);

            return await TryCatch(
                async () => await _rpcHttpClient.Get<RpcAccountWalletInfo>(_rpcOptions.GetWalletPath,
                    accessKey), "Cannot get wallet");
        }

        private void ValidateProperties(string[] propNames, params string[] props)
        {
            if (props.Any(string.IsNullOrEmpty))
                throw new HttpException(HttpStatusCode.UnprocessableEntity,
                    $"Fields - {string.Join(", ", propNames)} should not be empty");
        }

        private async Task<TResponse> TryCatch<TResponse>(Func<Task<TResponse>> action, string errorMessage)
        {
            try
            {
                return await action();
            }
            catch (Exception e)
            {
                _logger.Error(e.Message);
                throw new HttpException(HttpStatusCode.UnprocessableEntity,
                    $"{errorMessage}, exception message(will be deleted) - {e.Message}");
            }
        }
    }
}