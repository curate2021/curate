﻿namespace Curate.Wallet.Rpc.Dtos
{
    public class RpcUserInfoBase 
    {
        public string AccessKey { get; set; }
    }
}