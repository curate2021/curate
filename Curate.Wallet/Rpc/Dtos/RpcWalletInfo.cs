﻿using System.Text.Json.Serialization;
using Newtonsoft.Json;

namespace Curate.Wallet.Rpc.Dtos
{
    public class RpcWalletInfo : RpcBase
    {
        [JsonProperty("words")]
        public string[] Words { get; set; }
    }
}