﻿using System.Collections.Generic;
using System.Text.Json.Serialization;
using Newtonsoft.Json;

namespace Curate.Wallet.Rpc.Dtos
{
    public class RpcAccountWalletInfo : RpcBase
    {
        [JsonProperty("account")]
        public RpcAccountInfo RpcAccountInfo { get; set; }
    }
    
    public class RpcAccountInfo
    {
        [JsonProperty("address")]
        public string Address { get; set; }

        [JsonProperty("eth")]
        public WalletAccountToken AccountToken { get; set; }
        
        [JsonProperty("accountTokens")]
        public IEnumerable<WalletAccountToken> AccountTokens { get; set; }
    }

    public class WalletAccountToken
    {
        [JsonPropertyName("symbol")]
        public string Symbol { get; set; }
        [JsonPropertyName("balance")]
        public decimal Balance { get; set; }
    }
}