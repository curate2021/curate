﻿using Newtonsoft.Json;

namespace Curate.Wallet.Rpc.Dtos
{
    public class RpcUserInfo : RpcUserInfoBase
    {
        public string Email { get; set; }
    }
}