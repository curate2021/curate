﻿using System.Text.Json.Serialization;
using Newtonsoft.Json;

namespace Curate.Wallet.Rpc.Dtos
{
    public class RpcBase
    {
        [JsonProperty("success")]
        public bool Success { get; set; }
        
        [JsonProperty("message")]
        public string Message { get; set; }
    }
}