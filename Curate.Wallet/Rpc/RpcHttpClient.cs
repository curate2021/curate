﻿using System;
using System.Net.Http;
using System.Net.Mime;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace Curate.Wallet.Rpc
{
    public class RpcHttpClient
    {
        private readonly HttpClient _httpClient;

        public RpcHttpClient(IHttpClientFactory clientFactory)
        {
            _httpClient = clientFactory.CreateClient("Rpc");
        }

        public async Task<TResponse> Get<TResponse>(string url, string accessKey)
        {
            var request = new HttpRequestMessage(HttpMethod.Get, url);

            request.Headers.Add(nameof(accessKey), accessKey);

            var response = await _httpClient.SendAsync(request);
            var responseBody = await response.Content.ReadAsStringAsync();

            if (!response.IsSuccessStatusCode)
            {
                throw new Exception(responseBody);
            }

            return JsonConvert.DeserializeObject<TResponse>(responseBody);
        }

        public async Task<TResponse> Post<TResponse>(string path, object body, string accessKey = null)
        {
            var request = new HttpRequestMessage(HttpMethod.Post, path)
            {
                Content = new StringContent(JsonConvert.SerializeObject(body, new JsonSerializerSettings
                {
                    NullValueHandling = NullValueHandling.Ignore
                }), Encoding.UTF8, MediaTypeNames.Application.Json)
            };

            if (accessKey != null) request.Headers.Add(nameof(accessKey), accessKey);

            var response = await _httpClient.SendAsync(request);
            var responseBody = await response.Content.ReadAsStringAsync();

            if (!response.IsSuccessStatusCode)
            {
                throw new Exception($"{responseBody} - returned status code -  {response.StatusCode}");
            }

            return JsonConvert.DeserializeObject<TResponse>(responseBody);
        }
    }
}