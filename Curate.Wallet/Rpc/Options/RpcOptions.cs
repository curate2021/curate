﻿namespace Curate.Wallet.Rpc.Options
{
    public class RpcOptions
    {
        public string EndPoint { get; set; }

        public string SignUpPath { get; set; }

        public string SignInPath { get; set; }

        public string CreateWalletPath { get; set; }

        public string GetWalletPath { get; set; }
    }
}