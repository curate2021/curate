﻿using System.Threading.Tasks;
using Curate.Wallet.Rpc.Dtos;

namespace Curate.Wallet.Rpc
{
    public class DummyRpcClient : IRpcClient
    {
        public async Task<RpcWalletInfo> CreateWallet(RpcUserInfoBase info)
        {
            var result = new RpcWalletInfo();
            return await Task.FromResult(result);
        }

        public async Task<RpcUserInfoBase> SignUp()
        {
            return await Task.FromResult(new RpcUserInfo());
        }

        public async Task<RpcUserInfo> SignIn(RpcUserInfo info)
        {
            return await Task.FromResult(new RpcUserInfo());
        }

        public async Task<RpcAccountWalletInfo> GetWallet(RpcUserInfoBase info)
        {
            return await Task.FromResult(new RpcAccountWalletInfo());
        }
    }
}