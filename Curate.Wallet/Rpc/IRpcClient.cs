﻿using System.Threading.Tasks;
using Curate.Wallet.Rpc.Dtos;

namespace Curate.Wallet.Rpc
{
    public interface IRpcClient
    {
        Task<RpcUserInfoBase> SignUp();
        Task<RpcUserInfo> SignIn(RpcUserInfo info);

        Task<RpcWalletInfo> CreateWallet(RpcUserInfoBase info);
        Task<RpcAccountWalletInfo> GetWallet(RpcUserInfoBase info);
    }
}