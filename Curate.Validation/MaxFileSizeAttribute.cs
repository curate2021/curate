﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using Microsoft.AspNetCore.Http;

namespace Curate.Validation
{
    public class MaxFileSizeAttribute : ValidationAttribute
    {
        private readonly int _maxFileSize;

        public MaxFileSizeAttribute(int maxFileSize)
        {
            _maxFileSize = maxFileSize;
        }

        protected override ValidationResult IsValid(
            object value, ValidationContext validationContext)
        {
            if (!(value is IFormFile file)) return ValidationResult.Success;
            return file.Length > _maxFileSize
                ? new ValidationResult(string.Format(ValidationMessages.MaxFileErrorMessage, _maxFileSize))
                : ValidationResult.Success;
        }
    }

    public class ManyFilesMaxSizeAttribute : ValidationAttribute
    {
        private readonly int _maxFileSize;

        public ManyFilesMaxSizeAttribute(int maxFileSize)
        {
            _maxFileSize = maxFileSize;
        }

        protected override ValidationResult IsValid(
            object value, ValidationContext validationContext)
        {
            if (!(value is IEnumerable<IFormFile> files)) return ValidationResult.Success;
            return files.Any(file => file.Length > _maxFileSize)
                ? new ValidationResult(string.Format(ValidationMessages.MaxFileErrorMessage, _maxFileSize))
                : ValidationResult.Success;
        }
    }
}