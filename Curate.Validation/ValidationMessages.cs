﻿namespace Curate.Validation
{
    public class ValidationMessages
    {
        public const string MaxFileErrorMessage = "Maximum allowed file size is {0} bytes.";
        
        public const string AllowedExtensionErrorMessage = "This extension is not allowed!";
    }
}