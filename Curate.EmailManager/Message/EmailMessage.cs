﻿namespace Curate.EmailManager.Message
{
    public class EmailMessage
    {
        public string To { get; set; }
        
        public string Subject { get; set; }
        
        public string Body { get; set; }
        
        public bool IsHtmlBody { get; set; }
    }
}