﻿using System.Linq;

namespace Curate.EmailManager.Message
{
    public static class EmailMessageOptionsExtensions
    {
        public static EmailMessageTemplate GetEmailMessageTemplate(this EmailMessageOptions emailMessageOptions,
            EmailMessageTemplateType emailMessageTemplateType) =>
            emailMessageOptions.EmailMessageTemplates.FirstOrDefault(x =>
                x.EmailMessageTemplateType == emailMessageTemplateType);
    }
}