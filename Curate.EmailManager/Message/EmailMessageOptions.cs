﻿namespace Curate.EmailManager.Message
{
    public class EmailMessageOptions
    {
        public EmailMessageTemplate[] EmailMessageTemplates { get; set; }
    }

    public class EmailMessageTemplate
    {
        public string Subject { get; set; }
        
        public string Body { get; set; }
        
        public EmailMessageTemplateType EmailMessageTemplateType { get; set; }
    }

    public enum EmailMessageTemplateType
    {
        EmailConfirmation,
        ForgotPassword
    }
}