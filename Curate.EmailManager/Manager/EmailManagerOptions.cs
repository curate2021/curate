﻿namespace Curate.EmailManager.Manager
{
    public class EmailManagerOptions
    {
        public string Domain { get; set; }
        
        public int Port { get; set; }
        
        public string Email { get; set; }
        
        public string Password { get; set; }
    }
}