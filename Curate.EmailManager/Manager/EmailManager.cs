﻿using System.Net;
using System.Net.Mail;
using System.Threading.Tasks;
using Curate.EmailManager.Message;
using Microsoft.Extensions.Options;

namespace Curate.EmailManager.Manager
{
    public class EmailManager : IEmailManager
    {
        private readonly EmailManagerOptions _emailManagerOptions;

        public EmailManager(IOptions<EmailManagerOptions> emailManagerOptions)
        {
            _emailManagerOptions = emailManagerOptions.Value;
        }

        public async Task Send(EmailMessage message)
        {
            var mailMessage = new MailMessage(_emailManagerOptions.Email, message.To)
            {
                Subject = message.Subject,
                Body = message.Body,
                IsBodyHtml = message.IsHtmlBody
            };

            using var smtpClient = ConfigureSmtp();
            await smtpClient.SendMailAsync(mailMessage);
        }
        
        private SmtpClient ConfigureSmtp() =>
            new SmtpClient
            {
                Host = _emailManagerOptions.Domain,
                Port = _emailManagerOptions.Port,
                EnableSsl = true,
                Credentials = new NetworkCredential(_emailManagerOptions.Email, _emailManagerOptions.Password)
            };
    }
}