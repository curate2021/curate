﻿using System.Threading.Tasks;
using Curate.EmailManager.Message;

namespace Curate.EmailManager.Manager
{
    public interface IEmailManager
    {
        Task Send(EmailMessage message);
    }
}