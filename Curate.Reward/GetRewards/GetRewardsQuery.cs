﻿using System.Threading.Tasks;
using Curate.Linq;
using Force.Cqrs;
using Force.Linq.Pagination;

namespace Curate.Reward.GetRewards
{
    public class GetRewardsQuery : Paging, IQuery<Task<TotalRewardsInfo>>
    {
        
    }
}