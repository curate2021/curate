﻿using System;
using Curate.Domain.Entities.Reward;
using Curate.Linq;

namespace Curate.Reward.GetRewards
{
    public class RewardItem
    {
        public decimal TotalTokens { get; set; }

        public RewardType RewardType { get; set; }

        public DateTime CreateDate { get; set; }

        public string Description { get; set; }

        public DateTime? PayDate { get; set; }

        public RewardState State { get; set; }
    }

    public class TotalRewardsInfo
    {
        public decimal TotalRewardsPaid { get; set; }
        
        public decimal TotalRewardsUnpaid { get; set; }
        
        public PaginationResult<RewardItem> RewardItems { get; set; }
    }
}