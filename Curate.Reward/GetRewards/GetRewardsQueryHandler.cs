﻿using System;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using AutoMapper.QueryableExtensions;
using Curate.Core;
using Curate.Domain.Entities;
using Curate.Domain.Entities.Reward;
using Curate.Linq;
using Force.Cqrs;
using Microsoft.EntityFrameworkCore;

namespace Curate.Reward.GetRewards
{
    public class GetRewardsQueryHandler : IQueryHandler<GetRewardsQuery, Task<TotalRewardsInfo>>
    {
        private readonly IUserContext<User> _userContext;
        private readonly DbContext _dbContext;
        private readonly IMapper _mapper;

        public GetRewardsQueryHandler(IUserContext<User> userContext, DbContext dbContext, IMapper mapper)
        {
            _userContext = userContext;
            _dbContext = dbContext;
            _mapper = mapper;
        }

        public async Task<TotalRewardsInfo> Handle(GetRewardsQuery input)
        {
            var totalTokensUnpaid = await GetTotalTokensByState(RewardState.Unpaid);

            var totalTokensPaid = await GetTotalTokensByState(RewardState.Paid);

            var rewardItems = _dbContext.Set<Domain.Entities.Reward.Reward>()
                .Where(x => x.UserToId == _userContext.UserId)
                .ProjectTo<RewardItem>(_mapper.ConfigurationProvider)
                .OrderByDescending(x => x.State)
                .ThenByDescending(x => x.PayDate ?? DateTime.MaxValue)
                .ThenByDescending(x => x.CreateDate)
                .ToPaginationResult(input);

            return new TotalRewardsInfo
            {
                TotalRewardsPaid = totalTokensPaid,
                TotalRewardsUnpaid = totalTokensUnpaid,
                RewardItems = rewardItems
            };
        }

        private async Task<decimal> GetTotalTokensByState(RewardState state) =>
            await _dbContext.Set<Domain.Entities.Reward.Reward>()
                .Where(x => x.UserToId == _userContext.UserId)
                .Where(Domain.Entities.Reward.Reward.InState(state))
                .SumAsync(x => x.TotalTokens);
    }
}