﻿using AutoMapper;
using Curate.Reward.GetRewards;

namespace Curate.Reward
{
    public class RewardsProfile : Profile
    {
        public RewardsProfile()
        {
            CreateMap<Domain.Entities.Reward.Reward, RewardItem>();
        }
    }
}