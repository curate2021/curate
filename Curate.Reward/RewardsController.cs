﻿using System.Threading.Tasks;
using Curate.Mvc;
using Curate.Mvc.ControllerHelpers;
using Curate.Reward.GetRewards;
using Force.Extensions;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace Curate.Reward
{
    [Microsoft.AspNetCore.Components.Route("api/[controller]")]
    public class RewardsController : AuthControllerBase
    {
        [HttpGet]
        [Produces(MimeTypes.Application.Json)]
        [ProducesResponseType(typeof(TotalRewardsInfo), StatusCodes.Status200OK)]
        public async Task<IActionResult> Get([FromQuery] GetRewardsQuery query,
            [FromServices] GetRewardsQueryHandler handler) =>
            (await handler.Handle(query)).PipeTo(Ok);
    }
}