﻿using Force.Cqrs;
using Microsoft.AspNetCore.Mvc;

namespace Curate.Payment.PayWithPayPal
{
    public class PayWithPayPalCommand : ICommand<PayWithPayPalResult>
    {
        [FromRoute]
        public int OrderId { get; set; }
    }
}