﻿namespace Curate.Payment.PayWithPayPal
{
    public class PayWithPayPalResult
    {
        public int OrderId { get; set; }
    }
}