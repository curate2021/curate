﻿using System.Linq;
using System.Net;
using Curate.Core;
using Curate.Domain.Base;
using Curate.Domain.Entities;
using Curate.Domain.Order;
using Curate.Domain.Order.Calculation;
using Curate.Domain.Payment;
using Curate.Exceptions;
using Force.Cqrs;
using Force.Linq;
using Microsoft.EntityFrameworkCore;

namespace Curate.Payment.PayWithPayPal
{
    public class PayWithPayPalCommandHandler : ICommandHandler<PayWithPayPalCommand, PayWithPayPalResult>
    {
        private readonly DbContext _dbContext;
        private readonly IUserContext<User> _userContext;
        private readonly IDomainHandler<Order, CalculationResult> _calcOrderDomainHandler;

        public PayWithPayPalCommandHandler(
            DbContext dbContext,
            IDomainHandler<Order, CalculationResult> calcOrderDomainHandler, IUserContext<User> userContext)
        {
            _dbContext = dbContext;
            _calcOrderDomainHandler = calcOrderDomainHandler;
            _userContext = userContext;
        }

        public PayWithPayPalResult Handle(PayWithPayPalCommand input)
        {
            var payPalPaymentMethod = _dbContext
                .Set<PaymentMethod>()
                .First(PaymentMethod.IsPayPal);

            var order = _dbContext
                .Set<Order>()
                .IgnoreQueryFilters()
                .Where(Order.IsAvailableListing)
                .Where(Order.IsBuyer(_userContext.UserId))
                .FirstOrDefaultById(input.OrderId) ?? throw new HttpException(HttpStatusCode.NotFound,
                $"Order with id - {input.OrderId} not found.");

            if (!order.CheckInStatus(OrderStatus.Pending))
                throw new HttpException(HttpStatusCode.BadRequest, "This order cannot be paid.");

            order.Confirm(_calcOrderDomainHandler.Handle(order).TotalAmount, payPalPaymentMethod.Id);

            _dbContext.SaveChanges();

            return new PayWithPayPalResult {OrderId = input.OrderId};
        }
    }
}