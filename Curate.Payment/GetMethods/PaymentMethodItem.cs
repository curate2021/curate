﻿namespace Curate.Payment.GetMethods
{
    public class PaymentMethodItem
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public string IconId { get; set; }
    }
}