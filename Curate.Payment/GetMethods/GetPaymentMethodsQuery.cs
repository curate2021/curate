﻿using System.Collections.Generic;
using Force.Cqrs;

namespace Curate.Payment.GetMethods
{
    public class GetPaymentMethodsQuery : IQuery<IEnumerable<PaymentMethodItem>>
    {

    }
}