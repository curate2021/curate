﻿using System.Collections.Generic;
using System.Linq;
using AutoMapper;
using AutoMapper.QueryableExtensions;
using Curate.Domain.Payment;
using Force.Cqrs;
using Microsoft.EntityFrameworkCore;

namespace Curate.Payment.GetMethods
{
    public class GetPaymentMethodsQueryHandler : IQueryHandler<GetPaymentMethodsQuery, IEnumerable<PaymentMethodItem>>
    {
        private readonly DbContext _dbContext;
        private readonly IConfigurationProvider _configurationProvider;

        public GetPaymentMethodsQueryHandler(DbContext dbContext, IConfigurationProvider configurationProvider)
        {
            _dbContext = dbContext;
            _configurationProvider = configurationProvider;
        }

        public IEnumerable<PaymentMethodItem> Handle(GetPaymentMethodsQuery input)
            => _dbContext
                .Set<PaymentMethod>()
                .ProjectTo<PaymentMethodItem>(_configurationProvider)
                .ToList();
    }
}