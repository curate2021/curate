﻿using AutoMapper;
using Curate.Domain.Payment;
using Curate.Payment.GetMethods;

namespace Curate.Payment
{
    public class Mapping : Profile
    {
        public Mapping()
        {
            CreateMap<PaymentMethod, PaymentMethodItem>();
        }
    }
}