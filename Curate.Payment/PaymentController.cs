﻿using System.Collections.Generic;
using Curate.Mvc;
using Curate.Mvc.ControllerHelpers;
using Curate.Payment.GetMethods;
using Curate.Payment.PayWithPayPal;
using Force.Cqrs;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace Curate.Payment
{
    public class PaymentController : AuthControllerBase
    {
        [HttpGet]
        [Produces(MimeTypes.Application.Json)]
        [ProducesResponseType(typeof(IEnumerable<PaymentMethodItem>), StatusCodes.Status200OK)]
        public IActionResult Methods(
            [FromServices] IQueryHandler<GetPaymentMethodsQuery, IEnumerable<PaymentMethodItem>> handler)
            => Ok(handler.Handle(new GetPaymentMethodsQuery()));

        [HttpPost]
        [Produces(MimeTypes.Application.Json)]
        [ProducesResponseType(typeof(PayWithPayPalResult), StatusCodes.Status200OK)]
        [ProducesResponseType(typeof(ErrorResponse), StatusCodes.Status404NotFound)]
        public IActionResult PayPal(int orderId,
            [FromServices] ICommandHandler<PayWithPayPalCommand, PayWithPayPalResult> handler)
            => Ok(handler.Handle(new PayWithPayPalCommand {OrderId = orderId}));
    }
}