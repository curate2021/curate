﻿using System;
using System.IO;
using System.Threading.Tasks;
using Curate.Core;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Options;

namespace Curate.FileAccess
{
    public class LocalFileClient : IFileClient
    {
        private readonly StaticFilesOptions _staticFilesOptions;

        private const string FileNotFoundMessage = "File was not found.";

        private string GetFullFilePath(string id) => Path.Combine(_staticFilesOptions.LocationPath, id);

        public LocalFileClient(IOptions<StaticFilesOptions> fileStorageOptions)
        {
            _staticFilesOptions = fileStorageOptions.Value;
        }

        public async Task SaveFile(string id, IFormFile formFile)
        {
            Directory.CreateDirectory(_staticFilesOptions.LocationPath);

            await using var fs = new FileStream(GetFullFilePath(id), FileMode.Create);

            await formFile.CopyToAsync(fs);
        }

        public async Task<string> SaveFile(IFormFile formFile)
        {
            var uniqueFileId = $"{Guid.NewGuid()}{Path.GetExtension(formFile.FileName)}";
            await SaveFile(uniqueFileId, formFile);
            return uniqueFileId;
        }

        public async Task<Stream> GetFile(string id)
        {
            var outStream = new MemoryStream();

            await using var fs = new FileStream(GetFullFilePath(id), FileMode.Open);

            await fs.CopyToAsync(outStream);

            outStream.Position = 0;

            return outStream;
        }

        public Task DeleteFile(string id)
        {
            var filePath = GetFullFilePath(id);

            var fileInfo = new FileInfo(filePath);

            if (!fileInfo.Exists) throw new FileNotFoundException(FileNotFoundMessage, id);

            fileInfo.Delete();

            return Task.CompletedTask;
        }

        public Task<bool> FileExists(string id) => Task.FromResult(System.IO.File.Exists(GetFullFilePath(id)));
    }
}