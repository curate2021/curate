﻿namespace Curate.FileAccess
{
    public class StaticFilesOptions
    {
        public string LocationPath { get; set; }

        public string RequestPath { get; set; }
    }
}