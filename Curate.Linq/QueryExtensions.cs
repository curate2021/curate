﻿using System;
using System.Linq;
using System.Linq.Expressions;
using Force.Linq.Pagination;

namespace Curate.Linq
{
    public static class QueryExtensions
    {
        public static PaginationResult<T> ToPaginationResult<T>(this IQueryable<T> queryable, IPaging paging)
            => new PaginationResult<T>(queryable
                    .Skip((paging.Page - 1) * paging.Take)
                    .Take(paging.Take),
                queryable.Count());
    }
}