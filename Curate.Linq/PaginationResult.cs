﻿using System.Collections.Generic;

namespace Curate.Linq
{
    public class PaginationResult<T>
    {
        public PaginationResult(IEnumerable<T> items, long total)
        {
            Items = items;
            Total = total;
        }

        public IEnumerable<T> Items { get; }

        public long Total { get; }
    }
}