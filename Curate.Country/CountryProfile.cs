﻿using AutoMapper;
using Curate.Country.Features.GetCountries;

namespace Curate.Country
{
    public class CountryProfile : Profile
    {
        public CountryProfile()
        {
            CreateMap<Domain.Entities.Country, CountryItem>();
        }
    }
}