﻿using System.Collections.Generic;
using Curate.Country.Features.GetCountries;
using Curate.Mvc.ControllerHelpers;
using Force.Extensions;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace Curate.Country
{
    [Route("/api/[controller]")]
    public class CountryController : ApiControllerBase
    {
        [HttpGet]
        [ProducesResponseType(typeof(IEnumerable<CountryItem>), StatusCodes.Status200OK)]
        public IActionResult Get([FromQuery] GetCountriesQuery query,
            [FromServices] GetCountriesQueryHandler handler) =>
            handler.Handle(query).PipeTo(Ok);
    }
}