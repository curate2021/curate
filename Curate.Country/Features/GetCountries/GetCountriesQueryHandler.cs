﻿using System.Collections.Generic;
using AutoMapper;
using AutoMapper.QueryableExtensions;
using Force.Cqrs;
using Microsoft.EntityFrameworkCore;

namespace Curate.Country.Features.GetCountries
{
    public class GetCountriesQueryHandler : IQueryHandler<GetCountriesQuery, IEnumerable<CountryItem>>
    {
        private readonly DbContext _dbContext;
        private readonly IMapper _mapper;

        public GetCountriesQueryHandler(DbContext dbContext, IMapper mapper)
        {
            _dbContext = dbContext;
            _mapper = mapper;
        }

        public IEnumerable<CountryItem> Handle(GetCountriesQuery input) =>
            _dbContext.Set<Domain.Entities.Country>()
                .ProjectTo<CountryItem>(_mapper.ConfigurationProvider);
    }
}