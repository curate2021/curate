﻿namespace Curate.Country.Features.GetCountries
{
    public class CountryItem
    {
        public int Id { get; set; }
        
        public string Name { get; set; }
    }
}