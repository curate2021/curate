﻿using System.ComponentModel.DataAnnotations;
using System.Threading.Tasks;
using Curate.Linq;
using Curate.Listing.Listing.Common.Dtos;
using Force.Cqrs;
using Force.Linq.Pagination;

namespace Curate.Listing.Listing.GetUserListings
{
    public class GetUserListingsQuery : IQuery<Task<PaginationResult<ListingItem>>>, IPaging
    {
        internal int UserId { get; set; }

        [Range(0, int.MaxValue)]
        public int Page { get; set; } = 1;

        [Range(0, int.MaxValue)]
        public int Take { get; set; } = 10;
    }
}