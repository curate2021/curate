﻿using System.Linq;
using System.Net;
using System.Threading.Tasks;
using AutoMapper;
using AutoMapper.QueryableExtensions;
using Curate.Core;
using Curate.Domain.Entities;
using Curate.Exceptions;
using Curate.Linq;
using Curate.Listing.Listing.Common.Dtos;
using Curate.Listing.Listing.GetMyAds;
using Force.Cqrs;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Profile = Curate.Domain.Entities.Profile;

namespace Curate.Listing.Listing.GetUserListings
{
    public class GetUserListingsQueryHandler : IQueryHandler<GetUserListingsQuery, Task<PaginationResult<ListingItem>>>
    {
        private readonly DbContext _dbContext;
        private readonly UserManager<User> _userManager;
        private readonly IUserContext<User> _userContext;
        private readonly IConfigurationProvider _configurationProvider;

        public GetUserListingsQueryHandler(DbContext dbContext,
            UserManager<User> userManager,
            IUserContext<User> userContext,
            IConfigurationProvider configurationProvider)
        {
            _dbContext = dbContext;
            _userManager = userManager;
            _userContext = userContext;
            _configurationProvider = configurationProvider;
        }

        public async Task<PaginationResult<ListingItem>> Handle(GetUserListingsQuery input)
        {
            if (await _userManager.FindByIdAsync(input.UserId.ToString()) == null)
                throw new HttpException(HttpStatusCode.NotFound, "User not found.");

            return _dbContext
                .Set<Domain.Listing.Listing>()
                .Where(l => l.SellerId == input.UserId)
                .ProjectTo<ListingItem>(_configurationProvider, new {userId = _userContext.UserId})
                .OrderByDescending(x => x.IsCurrentUserLiked)
                .ThenByDescending(x => x.LikesCount)
                .ThenByDescending(x => x.CreateDate)
                .ToPaginationResult(input);
        }
    }
}