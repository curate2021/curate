﻿using System.Text.Json.Serialization;
using System.Threading.Tasks;
using Curate.Listing.Listing.Common;
using Force.Cqrs;

namespace Curate.Listing.Listing.UpdateListing
{
    public class UpdateListingCommand : CreateUpdateListingBase, ICommand<Task>
    {
        internal int ListingId { get; set; }
    }
}