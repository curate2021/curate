﻿using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using Curate.Core;
using Curate.Domain.Entities;
using Curate.Exceptions;
using Force.Cqrs;
using Microsoft.EntityFrameworkCore;

namespace Curate.Listing.Listing.UpdateListing
{
    public class UpdateListingCommandHandler : ICommandHandler<UpdateListingCommand, Task>
    {
        private readonly DbContext _dbContext;
        private readonly IFileClient _fileClient;

        public UpdateListingCommandHandler(DbContext dbContext, IFileClient fileClient)
        {
            _dbContext = dbContext;
            _fileClient = fileClient;
        }

        public async Task Handle(UpdateListingCommand input)
        {
            var listing = await _dbContext.Set<Domain.Listing.Listing>()
                .FindAsync(input.ListingId);

            if (listing == null)
            {
                throw new HttpException(HttpStatusCode.BadRequest, "Listing not found");
            }

            var categoryIds = input.CategoryIds
                .ToArray();

            var categories = _dbContext.Set<Category>()
                .Where(x => categoryIds.Contains(x.Id))
                .ToList();

            var images = new List<File>();

            foreach (var image in input.Images)
            {
                var imageId = await _fileClient.SaveFile(image);
                images.Add(new File(imageId, FileType.Image));
            }

            listing!.Update(input.Title, input.Description, input.Quantity, input.Price, images, categories);

            if (!input.CanBeDeliveredToAnyCountry)
            {
                var deliveryCountries = input.DeliveryCountries
                    .Select(x => new DeliveryCountry(x.CountryId, listing, x.Cost))
                    .ToArray();
                listing.AddDeliveryCountries(deliveryCountries);   
            }
            else
            {
                listing.MakeAvailableToAnyCountry();
            }
            
            await _dbContext.SaveChangesAsync();
        }
    }
}