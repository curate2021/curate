﻿using Curate.Listing.Listing.Common;
using Curate.Listing.Listing.Common.Validators;
using FluentValidation;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;

namespace Curate.Listing.Listing.UpdateListing
{
    public class UpdateListingCommandValidator : AbstractValidator<UpdateListingCommand>
    {
        public UpdateListingCommandValidator(DbContext dbContext)
        {
            RuleFor(x => x)
                .SetValidator(new CreateUpdateListingValidatorBase(dbContext));
        }
    }
}