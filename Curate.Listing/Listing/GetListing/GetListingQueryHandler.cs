﻿using System.Linq;
using System.Net;
using AutoMapper;
using AutoMapper.QueryableExtensions;
using Curate.Core;
using Curate.Domain.Entities;
using Curate.Exceptions;
using Curate.Listing.Listing.Common.Dtos;
using Force.Cqrs;
using Microsoft.EntityFrameworkCore;

namespace Curate.Listing.Listing.GetListing
{
    public class GetListingQueryHandler : IQueryHandler<GetListingQuery, ListingFullInfoItem>
    {
        private readonly DbContext _dbContext;
        private readonly IMapper _mapper;
        private readonly IUserContext<User> _userContext;

        public GetListingQueryHandler(DbContext dbContext, IMapper mapper, IUserContext<User> userContext)
        {
            _dbContext = dbContext;
            _mapper = mapper;
            _userContext = userContext;
        }

        public ListingFullInfoItem Handle(GetListingQuery input)
        {
            var listing = _dbContext.Set<Domain.Listing.Listing>()
                .Where(x => x.Id == input.Id)
                .ProjectTo<ListingFullInfoItem>(_mapper.ConfigurationProvider, new {userId = _userContext.UserId})
                .FirstOrDefault();

            if (listing == null) throw new HttpException(HttpStatusCode.NotFound, "Cannot find listing");

            return listing;
        }
    }
}