﻿using System.ComponentModel.DataAnnotations;
using Curate.Listing.Listing.Common.Dtos;
using Force.Cqrs;

namespace Curate.Listing.Listing.GetListing
{
    public class GetListingQuery : IQuery<ListingFullInfoItem>
    {
        [Required]
        public int Id { get; set; }
    }
}