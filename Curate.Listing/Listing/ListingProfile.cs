﻿using System.Linq;
using Curate.Listing.Listing.Common.Dtos;

namespace Curate.Listing.Listing
{
    public class ListingProfile : AutoMapper.Profile
    {
        public ListingProfile()
        {
            int userId = 0;
            CreateMap<Domain.Listing.Listing, ListingItemBase>()
                .ForMember(dest => dest.OwnerId, opt =>
                    opt.MapFrom(src => src.Seller.Id))
                .ForMember(dest => dest.LikesCount, opt =>
                    opt.MapFrom(src => src.Likes.Count))
                .ForMember(dest => dest.IsCurrentUserLiked, opt =>
                    opt.MapFrom(src => src.Likes.Any(x => x.UserId == userId)))
                .ForMember(dest => dest.FileIds, opt =>
                    opt.MapFrom(src => src.Files.Select(x => x.Id)));

            CreateMap<Domain.Listing.Listing, ListingItem>()
                .IncludeBase<Domain.Listing.Listing, ListingItemBase>();

            CreateMap<Domain.Listing.Listing, ListingFullInfoItem>()
                .IncludeBase<Domain.Listing.Listing, ListingItemBase>();

            CreateMap<Domain.Listing.Listing, ProfileListingItem>()
                .ForMember(dest => dest.CommentsCount, x => x.MapFrom(src => src.Comments.Count))
                .ForMember(dest => dest.CreateTime, x => x.MapFrom(src => src.CreateDate))
                .ForMember(dest => dest.LeftQuantity, x => x.MapFrom(src => src.GetAvailableQuantity()))
                .ForMember(dest => dest.LastOrderCreateTime,
                    x => x.MapFrom(src =>
                        src.Orders.Select(o => o.CreateDate).OrderByDescending(_ => _).FirstOrDefault()))
                .ForMember(dest => dest.TotalOrdersCount, x => x.MapFrom(src => src.Orders.Count));
        }
    }
}