﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Curate.Core;
using Curate.Core.Notification;
using Curate.Domain.Entities;
using Force.Cqrs;
using Microsoft.EntityFrameworkCore;

namespace Curate.Listing.Listing.CreateListing
{
    public class CreateListingCommandHandler : ICommandHandler<CreateListingCommand, Task<int>>
    {
        private readonly INotificationHandler<SendListingCreatedNotification.SendListingCreatedNotification>
            _createdListingNotification;

        private readonly DbContext _dbContext;
        private readonly IFileClient _fileClient;
        private readonly IUserContext<User> _userContext;

        public CreateListingCommandHandler(DbContext dbContext, IUserContext<User> userContext, IFileClient fileClient,
            INotificationHandler<SendListingCreatedNotification.SendListingCreatedNotification>
                createdListingNotification)
        {
            _dbContext = dbContext;
            _userContext = userContext;
            _fileClient = fileClient;
            _createdListingNotification = createdListingNotification;
        }

        public async Task<int> Handle(CreateListingCommand input)
        {
            var categoryIds = input.CategoryIds
                .ToArray();

            var categories = _dbContext.Set<Category>()
                .Where(x => categoryIds.Contains(x.Id))
                .ToList();

            var images = new List<File>();

            foreach (var image in input.Images)
            {
                var imageId = await _fileClient.SaveFile(image);
                images.Add(new File(imageId, FileType.Image));
            }

            var listing = new Domain.Listing.Listing(input.Title, input.Description, input.Quantity, input.Price,
                _userContext.UserId, images, categories);

            if (!input.CanBeDeliveredToAnyCountry)
            {
                var deliveryCountries = input.DeliveryCountries
                    .Select(x => new DeliveryCountry(x.CountryId, listing, x.Cost))
                    .ToArray();

                listing.AddDeliveryCountries(deliveryCountries);
            }
            else
            {
                listing.MakeAvailableToAnyCountry();
            }

            _dbContext.Add(listing);

            await _dbContext.SaveChangesAsync();

            await _createdListingNotification.Handle(new SendListingCreatedNotification.SendListingCreatedNotification
            {
                ListingId = listing.Id
            });

            return listing.Id;
        }
    }
}