﻿using System.Threading.Tasks;
using Curate.Listing.Listing.Common;
using Force.Cqrs;

namespace Curate.Listing.Listing.CreateListing
{
    public class CreateListingCommand : CreateUpdateListingBase, ICommand<Task<int>>
    {
    }
}