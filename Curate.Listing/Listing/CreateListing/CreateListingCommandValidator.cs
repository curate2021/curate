﻿using Curate.Listing.Listing.Common;
using Curate.Listing.Listing.Common.Validators;
using FluentValidation;
using Microsoft.EntityFrameworkCore;

namespace Curate.Listing.Listing.CreateListing
{
    public class CreateListingCommandValidator : AbstractValidator<CreateListingCommand>
    {
        public CreateListingCommandValidator(DbContext dbContext)
        {
            RuleFor(x => x)
                .SetValidator(new CreateUpdateListingValidatorBase(dbContext));
        }
    }
}