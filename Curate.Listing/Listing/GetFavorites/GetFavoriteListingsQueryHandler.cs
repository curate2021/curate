﻿using System.Linq;
using Curate.Core;
using Curate.Domain.Base;
using Curate.Domain.Entities;
using Curate.Linq;
using Curate.Listing.Listing.Common.Cqrs;
using Curate.Listing.Listing.Common.Dtos;
using Force.Cqrs;
using Microsoft.EntityFrameworkCore;

namespace Curate.Listing.Listing.GetFavorites
{
    public class GetFavoritesQueryHandler : IQueryHandler<GetListingsQuery, PaginationResult<ListingItem>>
    {
        private readonly DbContext _dbContext;
        private readonly IDomainHandler<(GetListingsQuery, IQueryable<Domain.Listing.Listing>), PaginationResult<ListingItem>> _domainHandler;
        private readonly IUserContext<User> _userContext;

        public GetFavoritesQueryHandler(DbContext dbContext,
            IDomainHandler<(GetListingsQuery, IQueryable<Domain.Listing.Listing>), PaginationResult<ListingItem>>
                domainHandler, 
            IUserContext<User> userContext)
        {
            _dbContext = dbContext;
            _domainHandler = domainHandler;
            _userContext = userContext;
        }

        public PaginationResult<ListingItem> Handle(GetListingsQuery input)
        {
            {
                var listings = _dbContext.Set<Domain.Entities.Like>()
                    .Where(l => l.UserId == _userContext.UserId)
                    .Select(l => l.Listing);

                return _domainHandler.Handle((input, listings));
            }
        }
    }
}