﻿using System.Linq;
using AutoMapper;
using AutoMapper.QueryableExtensions;
using Curate.Core;
using Curate.Domain.Entities;
using Curate.Linq;
using Curate.Listing.Listing.Common.Dtos;
using Force.Cqrs;
using Microsoft.EntityFrameworkCore;

namespace Curate.Listing.Listing.GetMyAds
{
    public class GetMyAdsQueryHandler : IQueryHandler<GetMyAdsQuery, PaginationResult<ProfileListingItem>>
    {
        private readonly IUserContext<User> _userContext;
        private readonly DbContext _dbContext;
        private readonly IConfigurationProvider _configurationProvider;

        public GetMyAdsQueryHandler(IUserContext<User> userContext,
            DbContext dbContext,
            IConfigurationProvider configurationProvider)
        {
            _userContext = userContext;
            _dbContext = dbContext;
            _configurationProvider = configurationProvider;
        }

        public PaginationResult<ProfileListingItem> Handle(GetMyAdsQuery input)
            => _dbContext.Set<Domain.Listing.Listing>()
                .Include(x => x.Files)
                .Include(x => x.Orders)
                .Where(Domain.Listing.Listing.IsSeller(_userContext.UserId))
                .ProjectTo<ProfileListingItem>(_configurationProvider)
                .OrderByDescending(ad => ad.LastOrderCreateTime)
                .ThenByDescending(ad => ad.CreateTime)
                .ToPaginationResult(input);
    }
}