﻿using System.ComponentModel.DataAnnotations;
using Curate.Linq;
using Curate.Listing.Listing.Common.Dtos;
using Force.Cqrs;
using Force.Linq.Pagination;

namespace Curate.Listing.Listing.GetMyAds
{
    public class GetMyAdsQuery : IQuery<PaginationResult<ProfileListingItem>>, IPaging
    {
        [Range(0, int.MaxValue)]
        public int Page { get; set; } = 1;

        [Range(0, int.MaxValue)]
        public int Take { get; set; } = 10;
    }
}