﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Curate.Mvc;
using Microsoft.AspNetCore.Http;

namespace Curate.Listing.Listing.Common
{
    public class CreateUpdateListingBase
    {
        public string Title { get; set; }

        public string Description { get; set; }

        public int Quantity { get; set; }

        public decimal Price { get; set; }
        
        public bool CanBeDeliveredToAnyCountry { get; set; }

        [DataType(DataType.Upload)]
        [ManyFilesMaxSize(5 * 1024 * 1024)]
        [AllowedManyFilesExtensions(".jpg", ".png")]
        public IEnumerable<IFormFile> Images { get; set; }

        public IEnumerable<int> CategoryIds { get; set; }

        public IEnumerable<DeliveryCountryItem> DeliveryCountries { get; set; }
    }

    public class DeliveryCountryItem
    {
        public int CountryId { get; set; }

        public decimal Cost { get; set; }
    }
}