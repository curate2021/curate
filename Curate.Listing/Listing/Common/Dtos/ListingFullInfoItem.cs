﻿namespace Curate.Listing.Listing.Common.Dtos
{
    public class ListingFullInfoItem : ListingItemBase
    {
        public string Description { get; set; }
    }
}