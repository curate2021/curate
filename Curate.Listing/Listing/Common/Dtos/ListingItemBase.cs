﻿using System;
using System.Collections.Generic;
using Force.Linq;

namespace Curate.Listing.Listing.Common.Dtos
{
    public class ListingItemBase
    {
        public int Id { get; set; }

        [SearchBy(SearchKind.Contains)]
        public string Title { get; set; }

        public decimal Price { get; set; }

        public int Quantity { get; set; }

        public int LikesCount { get; set; }

        public IEnumerable<string> FileIds { get; set; }

        public DateTime CreateDate { get; set; }
        
        public bool IsCurrentUserLiked { get; set; }
        
        public int OwnerId { get; set; }
    }
}