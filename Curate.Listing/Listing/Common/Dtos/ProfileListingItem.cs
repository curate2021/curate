﻿using System;
using System.Collections.Generic;
using Force.Ddd;

namespace Curate.Listing.Listing.Common.Dtos
{
    public class ProfileListingItem : IHasId<int>
    {
        public int Id { get; set; }

        object IHasId.Id => Id;

        public IEnumerable<string> ImageIds { get; set; }

        public string Title { get; set; }

        public int CommentsCount { get; set; }

        public DateTime CreateTime { get; set; }

        public decimal Price { get; set; }

        public int LeftQuantity { get; set; }

        public int TotalOrdersCount { get; set; }

        public DateTime? LastOrderCreateTime { get; set; }
    }
}