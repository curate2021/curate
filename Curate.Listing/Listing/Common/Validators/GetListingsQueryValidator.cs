﻿using Curate.Listing.Listing.Common.Cqrs;
using FluentValidation;

namespace Curate.Listing.Listing.Common.Validators
{
    public class GetListingsQueryValidator : AbstractValidator<GetListingsQuery>
    {
        public GetListingsQueryValidator()
        {
            RuleFor(x => x.From)
                .GreaterThan(0);
            RuleFor(x => x.To)
                .GreaterThan(1);
        }
    }
}