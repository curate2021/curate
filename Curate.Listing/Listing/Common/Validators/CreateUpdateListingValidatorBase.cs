﻿using System.Linq;
using System.Net;
using Curate.Domain.Entities;
using Curate.Exceptions;
using FluentValidation;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;

namespace Curate.Listing.Listing.Common.Validators
{
    public class CreateUpdateListingValidatorBase : AbstractValidator<CreateUpdateListingBase>
    {
        public CreateUpdateListingValidatorBase(DbContext dbContext)
        {
            RuleFor(x => x.Title)
                .NotEmpty()
                .MaximumLength(byte.MaxValue);
            ;
            RuleFor(x => x.Description)
                .NotEmpty()
                .MaximumLength(byte.MaxValue);
            RuleFor(x => x.Quantity)
                .NotEmpty()
                .GreaterThan(0);
            ;
            RuleFor(x => x.Price)
                .NotEmpty()
                .GreaterThan(0);

            RuleFor(x => x.Images)
                .NotEmpty();

            RuleFor(x => x.CategoryIds)
                .NotEmpty();

            RuleFor(x => x.CategoryIds)
                .MustAsync(async (e, ct) =>
                {
                    var categoryIds = e.ToArray();

                    if (categoryIds.Length != categoryIds.Distinct().Count())
                        throw new HttpException(HttpStatusCode.BadRequest, "Duplicate of category");

                    var existingCategoriesLength = await dbContext
                        .Set<Category>()
                        .CountAsync(x => categoryIds.Contains(x.Id), ct);

                    return categoryIds.Length == existingCategoriesLength;
                })
                .WithMessage("Some of the categories does not exist")
                .WithErrorCode(StatusCodes.Status400BadRequest.ToString());

            RuleForEach(x => x.DeliveryCountries)
                .SetValidator(new DeliveryCountryItemValidator());

            RuleFor(x => x)
                .MustAsync(async (e, ct) =>
                {
                    if (e.CanBeDeliveredToAnyCountry) return true;
                    
                    var countryIds = e.DeliveryCountries
                        .Select(x => x.CountryId)
                        .ToArray();

                    if (countryIds.Length != countryIds.Distinct().Count())
                        throw new HttpException(HttpStatusCode.BadRequest, "Duplicate of countries");

                    var existingCountriesLength = await dbContext
                        .Set<Country>()
                        .CountAsync(x => countryIds.Contains(x.Id), ct);

                    return countryIds.Length == existingCountriesLength;
                })
                .WithMessage("Some of the countries does not exist")
                .WithErrorCode(StatusCodes.Status400BadRequest.ToString());
        }
    }

    public class DeliveryCountryItemValidator : AbstractValidator<DeliveryCountryItem>
    {
        public DeliveryCountryItemValidator()
        {
            RuleFor(x => x.Cost)
                .NotEmpty()
                .GreaterThan(0);

            RuleFor(x => x.CountryId)
                .GreaterThan(0);
        }
    }
}