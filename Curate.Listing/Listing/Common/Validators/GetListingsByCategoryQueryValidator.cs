﻿using Curate.Listing.Listing.GetListings;
using FluentValidation;

namespace Curate.Listing.Listing.Common.Validators
{
    public class GetListingsByCategoryQueryValidator : AbstractValidator<GetListingsByCategoryQuery>
    {
        public GetListingsByCategoryQueryValidator()
        {
            Include(new GetListingsQueryValidator());
        }
    }
}