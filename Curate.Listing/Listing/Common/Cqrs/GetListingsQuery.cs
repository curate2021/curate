﻿using System.Linq;
using Curate.Core;
using Curate.Linq;
using Curate.Listing.Listing.Common.Dtos;
using Force.Cqrs;
using Force.Linq.Pagination;

namespace Curate.Listing.Listing.Common.Cqrs
{
    public class GetListingsQuery : FilterQueryBase<ListingItem>,
        IPaging,
        IQuery<PaginationResult<ListingItem>>
        , IHasRange
    {
        public decimal From { get; set; } = 1;

        public decimal To { get; set; } = int.MaxValue;

        public int Page { get; set; } = 1;
        public int Take { get; set; } = 10;

        public override IOrderedQueryable<ListingItem> Sort(IQueryable<ListingItem> queryable) =>
            string.IsNullOrEmpty(Order) || Order == nameof(ListingItem.CreateDate)
                ? queryable.OrderByDescending(x => x.CreateDate)
                : base.Sort(queryable);

        public override IQueryable<ListingItem> Filter(IQueryable<ListingItem> queryable) =>
            string.IsNullOrEmpty(Search)
                ? base.Filter(queryable)
                : queryable.Where(x => x.Title.ToUpper().Contains(Search.ToUpper()));
    }
}