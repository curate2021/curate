﻿using System.Diagnostics.CodeAnalysis;
using System.Linq;
using AutoMapper;
using AutoMapper.QueryableExtensions;
using Curate.Core;
using Curate.Domain.Base;
using Curate.Domain.Entities;
using Curate.Linq;
using Curate.Listing.Listing.Common.Dtos;
using Force.Linq;

namespace Curate.Listing.Listing.Common.Cqrs
{
    public class FilterSortPaginateListingDomainHandler : IDomainHandler<(GetListingsQuery,
        IQueryable<Domain.Listing.Listing>), PaginationResult<ListingItem>>
    {
        private readonly IMapper _mapper;
        private readonly IUserContext<User> _userContext;

        public FilterSortPaginateListingDomainHandler(IMapper mapper, IUserContext<User> userContext)
        {
            _mapper = mapper;
            _userContext = userContext;
        }

        public PaginationResult<ListingItem> Handle(
            [NotNull] (GetListingsQuery, IQueryable<Domain.Listing.Listing>) input) =>
            input.Item2
                .Where(Domain.Listing.Listing.InRange(input.Item1.From, input.Item1.To))
                .Where(Domain.Listing.Listing.CanBeDeliveredToCountry(_userContext.User.Profile.CountryId))
                .ProjectTo<ListingItem>(_mapper.ConfigurationProvider, new {userId = _userContext.UserId})
                .FilterAndSort(input.Item1)
                .ToPaginationResult(input.Item1);
    }
}