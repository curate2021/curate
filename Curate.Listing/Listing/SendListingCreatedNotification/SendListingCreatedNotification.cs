﻿using Curate.Core.Notification;

namespace Curate.Listing.Listing.SendListingCreatedNotification
{
    public class SendListingCreatedNotification : INotification
    {
        public int ListingId { get; set; }
    }
}