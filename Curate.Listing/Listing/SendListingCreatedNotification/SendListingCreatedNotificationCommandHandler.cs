﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Curate.Core.Notification;
using Curate.Domain.Entities;
using Curate.FirebaseMessaging;
using Curate.FirebaseMessaging.FirebaseClient;
using FirebaseAdmin.Messaging;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Options;
using Notification = FirebaseAdmin.Messaging.Notification;

namespace Curate.Listing.Listing.SendListingCreatedNotification
{
    public class
        SendListingCreatedNotificationCommandHandler : INotificationHandler<SendListingCreatedNotification>
    {
        private readonly DbContext _dbContext;
        private readonly IFirebaseClient _firebaseClient;
        private readonly FirebaseNotificationOptions _firebaseNotificationDataOptions;
        private readonly NotificationData _notificationData;

        public SendListingCreatedNotificationCommandHandler(IFirebaseClient firebaseClient, DbContext dbContext,
            IOptions<FirebaseNotificationOptions> firebaseNotificationDataOptions)
        {
            _firebaseClient = firebaseClient;
            _dbContext = dbContext;
            _firebaseNotificationDataOptions = firebaseNotificationDataOptions.Value;
            _notificationData = _firebaseNotificationDataOptions
                .GetDataByNotificationType((byte) NotificationType.FollowingPublication);
        }

        public async Task Handle(SendListingCreatedNotification input, CancellationToken cancellationToken)
        {
            try
            {
                var listing = await _dbContext.Set<Domain.Listing.Listing>()
                    .FindAsync(input.ListingId);

                if (listing == null) return;

                var messages = listing.Seller
                    .Subscribers
                    .Where(User.CanReceiveNotification(NotificationType.FollowingPublication)
                        .From<Subscription>(x => x.From.User).IsSatisfiedBy)
                    .Where(Firebase.HasToken.From<Subscription>(x => x.From.User.Firebase).IsSatisfiedBy)
                    .Select(x => new Message
                    {
                        Token = x.From.User.Firebase.Token,
                        Notification = new Notification
                        {
                            Title = string.Format(_notificationData.Title,
                                listing.Title),
                            Body = string.Format(_notificationData.Body, listing.Seller.FullName),
                            ImageUrl =
                                $"{_firebaseNotificationDataOptions.ImagesUrl}/{listing.Files.FirstOrDefault()?.Id}"
                        },
                        Data = new Dictionary<string, string>(new[]
                        {
                            new KeyValuePair<string, string>(nameof(input.ListingId), input.ListingId.ToString()),
                            new KeyValuePair<string, string>(nameof(NotificationType),
                                NotificationType.FollowingPublication.ToString())
                        })
                    })
                    .ToArray();

                await _firebaseClient.SendNotifications(messages);
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
            }
        }
    }
}