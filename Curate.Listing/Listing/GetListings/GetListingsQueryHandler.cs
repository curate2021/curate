﻿using System.Linq;
using AutoMapper;
using AutoMapper.QueryableExtensions;
using Curate.Core;
using Curate.Domain.Entities;
using Curate.Linq;
using Curate.Listing.Listing.Common.Cqrs;
using Curate.Listing.Listing.Common.Dtos;
using Force.Cqrs;
using Force.Linq;
using Microsoft.EntityFrameworkCore;

namespace Curate.Listing.Listing.GetListings
{
    public class GetListingsQueryHandler : IQueryHandler<GetListingsByCategoryQuery, PaginationResult<ListingItem>>
    {
        private readonly DbContext _dbContext;
        private readonly IMapper _mapper;
        private readonly IUserContext<User> _userContext;

        public GetListingsQueryHandler(DbContext dbContext, IMapper mapper, IUserContext<User> userContext)
        {
            _dbContext = dbContext;
            _mapper = mapper;
            _userContext = userContext;
        }

        public PaginationResult<ListingItem> Handle(GetListingsByCategoryQuery input) =>
            _dbContext.Set<Category>()
                .WhereIf(input.CategoryId.HasValue, x => x.Id == input.CategoryId)
                .SelectMany(x => x.Listings)
                .Where(Domain.Listing.Listing.CanBeDeliveredToCountry(_userContext.User.Profile.CountryId))
                .Where(Domain.Listing.Listing.InRange(input.From, input.To))
                .ProjectTo<ListingItem>(_mapper.ConfigurationProvider, new {userId = _userContext.UserId})
                .FilterAndSort(input)
                .ToPaginationResult(input);
    }
}