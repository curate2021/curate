﻿using System.ComponentModel.DataAnnotations;
using Curate.Listing.Listing.Common.Cqrs;

namespace Curate.Listing.Listing.GetListings
{
    public class GetListingsByCategoryQuery : GetListingsQuery
    {
        public int? CategoryId { get; set; }
    }
}