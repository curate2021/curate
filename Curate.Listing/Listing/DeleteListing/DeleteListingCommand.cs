﻿using Force.Cqrs;

namespace Curate.Listing.Listing.DeleteListing
{
    public class DeleteListingCommand : ICommand
    {
        public int Id { get; set; }
    }
}