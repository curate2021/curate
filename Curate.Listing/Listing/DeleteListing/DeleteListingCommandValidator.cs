﻿using System.Net;
using Curate.Core;
using Curate.Domain.Entities;
using Curate.Exceptions;
using FluentValidation;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;

namespace Curate.Listing.Listing.DeleteListing
{
    public class DeleteListingCommandValidator : AbstractValidator<DeleteListingCommand>
    {
        public DeleteListingCommandValidator(DbContext dbContext, IUserContext<User> userContext)
        {
            RuleFor(x => x.Id)
                .NotEmpty();

            RuleFor(x => x)
                .Must(e =>
                {
                    var listing = dbContext.Set<Domain.Listing.Listing>()
                        .Find(e.Id);
                    return listing == null
                        ? throw new HttpException(HttpStatusCode.BadRequest, $"No listing with id = {e.Id} exists")
                        : listing.SellerId == userContext.UserId;
                })
                .WithMessage("You don't have access to this operation")
                .WithErrorCode(StatusCodes.Status403Forbidden.ToString());
        }
    }
}