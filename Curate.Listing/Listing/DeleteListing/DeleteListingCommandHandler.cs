﻿using System.Net;
using Curate.Exceptions;
using Force.Cqrs;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;

namespace Curate.Listing.Listing.DeleteListing
{
    public class DeleteListingCommandHandler : ICommandHandler<DeleteListingCommand>
    {
        private readonly DbContext _dbContext;

        public DeleteListingCommandHandler(DbContext dbContext)
        {
            _dbContext = dbContext;
        }

        public void Handle(DeleteListingCommand input)
        {
            var listing = _dbContext.Set<Domain.Listing.Listing>()
                .Find(input.Id);

            if (listing.Orders.Count != 0)
                throw new HttpException(HttpStatusCode.BadRequest, "Unable to delete a listing that has orders.");

            listing.Delete();

            _dbContext.SaveChanges();
        }
    }
}