﻿using System.Linq;
using System.Net;
using Curate.Core;
using Curate.Domain.Entities;
using Curate.Exceptions;
using Force.Cqrs;
using Force.Linq;
using Microsoft.EntityFrameworkCore;

namespace Curate.Listing.Listing.ArchiveListing
{
    public class ArchiveListingCommandHandler : ICommandHandler<ArchiveListingCommand>
    {
        private readonly DbContext _dbContext;
        private readonly IUserContext<User> _userContext;

        public ArchiveListingCommandHandler(DbContext dbContext, IUserContext<User> userContext)
        {
            _dbContext = dbContext;
            _userContext = userContext;
        }

        public void Handle(ArchiveListingCommand input)
        {
            var sellerId = _userContext.UserId;

            var listing = _dbContext.Set<Domain.Listing.Listing>()
                              .IgnoreQueryFilters()
                              .Where(Domain.Listing.Listing.IsNotDeleted)
                              .Where(Domain.Listing.Listing.IsSeller(sellerId))
                              .FirstOrDefaultById(input.Id) ??
                          throw new HttpException(HttpStatusCode.NotFound, "Listing not found");

            listing.Archive();

            _dbContext.SaveChanges();
        }
    }
}