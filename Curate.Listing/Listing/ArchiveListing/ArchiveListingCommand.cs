﻿using Force.Cqrs;
using Force.Ddd;

namespace Curate.Listing.Listing.ArchiveListing
{
    public class ArchiveListingCommand : ICommand, IHasId<int>
    {
        object IHasId.Id => Id;

        public int Id { get; set; }
    }
}