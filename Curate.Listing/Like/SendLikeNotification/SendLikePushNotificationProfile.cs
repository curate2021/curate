﻿using System.Collections.Generic;
using System.Linq;
using Curate.Domain.Entities;
using FirebaseAdmin.Messaging;
using Notification = FirebaseAdmin.Messaging.Notification;
using Profile = AutoMapper.Profile;

namespace Curate.Listing.Like.SendLikeNotification
{
    public class SendLikePushNotificationProfile : Profile
    {
        public SendLikePushNotificationProfile()
        {
            var userFromId = 0;
            var userFullName = string.Empty;
            var title = string.Empty;
            var imageUrl = string.Empty;
            
            CreateMap<Domain.Listing.Listing, Message>()
                .ForMember(dest => dest.Token, opt =>
                    opt.MapFrom(src => src.Seller.User.Firebase.Token))
                .ForMember(dest => dest.Notification, opt =>
                    opt.MapFrom(src => src))
                .ForMember(dest => dest.Data, opt =>
                    opt.MapFrom(src => new Dictionary<string, string>(new[]
                    {
                        new KeyValuePair<string, string>("listingId", src.Id.ToString()),
                        new KeyValuePair<string, string>(nameof(userFromId), userFromId.ToString()),
                        new KeyValuePair<string, string>(nameof(NotificationType), NotificationType.Like.ToString()), 
                    })))
                ;
            CreateMap<Domain.Listing.Listing, Notification>()
                .ForMember(dest => dest.Title, opt =>
                    opt.MapFrom(src => string.Format(title, userFullName)))
                .ForMember(dest => dest.ImageUrl, opt =>
                    opt.MapFrom(src => $"{imageUrl}{src.Files.FirstOrDefault().Id}"))
                ;
        }
    }
}