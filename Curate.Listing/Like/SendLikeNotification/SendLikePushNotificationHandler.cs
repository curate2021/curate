﻿using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using AutoMapper.QueryableExtensions;
using Curate.Core.Notification;
using Curate.Domain.Entities;
using Curate.FirebaseMessaging;
using Curate.FirebaseMessaging.FirebaseClient;
using FirebaseAdmin.Messaging;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Options;
using Serilog;

namespace Curate.Listing.Like.SendLikeNotification
{
    public class SendLikePushNotificationHandler : INotificationHandler<SendLikePushNotification>
    {
        private readonly DbContext _dbContext;
        private readonly IFirebaseClient _firebaseClient;
        private readonly FirebaseNotificationOptions _firebaseNotificationDataOptions;
        private readonly ILogger _logger;
        private readonly IMapper _mapper;
        private readonly NotificationData _notificationData;

        public SendLikePushNotificationHandler(IFirebaseClient firebaseClient, DbContext dbContext,
            IMapper mapper, IOptions<FirebaseNotificationOptions> firebaseNotificationDataOptions, ILogger logger)
        {
            _firebaseClient = firebaseClient;
            _dbContext = dbContext;
            _mapper = mapper;
            _logger = logger;
            _firebaseNotificationDataOptions = firebaseNotificationDataOptions.Value;
            _notificationData = _firebaseNotificationDataOptions
                .GetDataByNotificationType((byte) NotificationType.Like);
        }

        public async Task Handle(SendLikePushNotification input, CancellationToken cancellationToken)
        {
            try
            {
                var messages = _dbContext.Set<Domain.Listing.Listing>()
                    .Where(x => x.Id == input.ListingId)
                    .Where(User.CanReceiveNotification(NotificationType.Like)
                        .From<Domain.Listing.Listing>(x => x.Seller.User))
                    .Where(Firebase.HasToken.From<Domain.Listing.Listing>(x => x.Seller.User.Firebase))
                    .ProjectTo<Message>(_mapper.ConfigurationProvider,
                        new
                        {
                            userFromId = input.UserFromId,
                            userFullName = input.UserFullName,
                            imageUrl = _firebaseNotificationDataOptions.ImagesUrl,
                            title = _notificationData.Title
                        })
                    .ToArray();

                if (messages.Length > 0) await _firebaseClient.SendNotifications(messages);
            }
            catch (Exception e)
            {
                _logger.Error(e.Message);
            }
        }
    }
}