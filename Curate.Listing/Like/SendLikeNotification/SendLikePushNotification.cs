﻿using Curate.Core.Notification;

namespace Curate.Listing.Like.SendLikeNotification
{
    public class SendLikePushNotification : INotification
    {
        public int ListingId { get; set; }
        
        public int UserFromId { get; set; }
        
        public string UserFullName { get; set; }
    }
}