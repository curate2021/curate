﻿using System.ComponentModel.DataAnnotations;
using System.Threading.Tasks;
using Force.Cqrs;

namespace Curate.Listing.Like.LikeOrUnlikeListing
{
    public class LikeOrUnlikeListingCommand : ICommand<Task<int>>
    {
        [Required]
        public int Id { get; set; }
    }
}