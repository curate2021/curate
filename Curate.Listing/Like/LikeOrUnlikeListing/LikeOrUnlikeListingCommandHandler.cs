﻿using System.Linq;
using System.Threading.Tasks;
using Curate.Core;
using Curate.Core.Notification;
using Curate.Domain.Entities;
using Curate.Listing.Like.SendLikeNotification;
using Force.Cqrs;
using Microsoft.EntityFrameworkCore;

namespace Curate.Listing.Like.LikeOrUnlikeListing
{
    public class LikeOrUnlikeListingCommandHandler : ICommandHandler<LikeOrUnlikeListingCommand, Task<int>>
    {
        private readonly DbContext _dbContext;
        private readonly INotificationHandler<SendLikePushNotification> _likeNotificationHandler;
        private readonly IUserContext<User> _userContext;

        public LikeOrUnlikeListingCommandHandler(DbContext dbContext, IUserContext<User> userContext,
            INotificationHandler<SendLikePushNotification> likeNotificationHandler)
        {
            _dbContext = dbContext;
            _userContext = userContext;
            _likeNotificationHandler = likeNotificationHandler;
        }

        public async Task<int> Handle(LikeOrUnlikeListingCommand input)
        {
            var listing = await _dbContext.Set<Domain.Listing.Listing>()
                .FindAsync(input.Id);

            var currentUserId = _userContext.UserId;
            if (listing.Likes.Any(x => x.UserId == currentUserId))
            {
                listing.Unlike(currentUserId);
            }
            else
            {
                listing.Like(currentUserId);
                await _likeNotificationHandler.Handle(new SendLikePushNotification
                {
                    ListingId = listing.Id,
                    UserFromId = currentUserId,
                    UserFullName = _userContext.User.Profile.FullName
                });
            }

            await _dbContext.SaveChangesAsync();

            return listing.Likes.Count;
        }
    }
}