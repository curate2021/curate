﻿using System.ComponentModel.DataAnnotations;
using System.Threading.Tasks;
using Curate.Linq;
using Curate.Listing.Comment.AddComment;
using Curate.Listing.Comment.GetComments;
using Curate.Listing.Like.LikeOrUnlikeListing;
using Curate.Listing.Listing.ArchiveListing;
using Curate.Listing.Listing.Common.Cqrs;
using Curate.Listing.Listing.Common.Dtos;
using Curate.Listing.Listing.CreateListing;
using Curate.Listing.Listing.DeleteListing;
using Curate.Listing.Listing.GetArchive;
using Curate.Listing.Listing.GetListing;
using Curate.Listing.Listing.GetListings;
using Curate.Listing.Listing.GetMyAds;
using Curate.Listing.Listing.GetUserListings;
using Curate.Listing.Listing.UpdateListing;
using Curate.Mvc;
using Curate.Mvc.ControllerHelpers;
using Force.Cqrs;
using Force.Extensions;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace Curate.Listing
{
    [Route("api/[controller]")]
    public class ListingController : AuthControllerBase
    {
        [HttpGet("list")]
        [ProducesResponseType(typeof(PaginationResult<ListingItem>), StatusCodes.Status200OK)]
        public IActionResult
            GetAll([FromQuery] GetListingsByCategoryQuery byCategoryQuery,
                [FromServices] GetListingsQueryHandler handler) =>
            Ok(handler.Handle(byCategoryQuery));

        [HttpGet("list/{userId}")]
        [ProducesResponseType(typeof(PaginationResult<ListingItem>), StatusCodes.Status200OK)]
        public async Task<IActionResult> GetAll([FromRoute] int userId, [FromQuery] GetUserListingsQuery query,
            [FromServices] IQueryHandler<GetUserListingsQuery, Task<PaginationResult<ListingItem>>> handler)
        {
            query.UserId = userId;
            return Ok(await handler.Handle(query));
        }

        [HttpGet("{id}")]
        [ProducesResponseType(typeof(ListingFullInfoItem), StatusCodes.Status200OK)]
        [ProducesResponseType(typeof(ErrorResponse), StatusCodes.Status404NotFound)]
        public IActionResult Get([FromRoute] GetListingQuery query, [FromServices] GetListingQueryHandler handler) =>
            Ok(handler.Handle(query));

        [HttpPost]
        [Produces(MimeTypes.Application.Json)]
        [ProducesResponseType(typeof(ErrorResponse), StatusCodes.Status400BadRequest)]
        [ProducesResponseType(typeof(int), StatusCodes.Status200OK)]
        public async Task<IActionResult> Post([FromForm] CreateListingCommand command,
            [FromServices] CreateListingCommandHandler handler)
        {
            var listingId = await handler.Handle(command);

            return Ok(listingId);
        }

        [HttpPut("{id}")]
        [Produces(MimeTypes.Application.Json)]
        [ProducesResponseType(typeof(ErrorResponse), StatusCodes.Status400BadRequest)]
        public async Task<IActionResult> Put([FromRoute] [Required] int id,
            [FromForm] UpdateListingCommand command,
            [FromServices] UpdateListingCommandHandler handler)
        {
            command.ListingId = id;
            await handler.Handle(command);

            return NoContent();
        }

        [HttpDelete("{id}")]
        public IActionResult Delete([FromRoute] DeleteListingCommand command,
            [FromServices] DeleteListingCommandHandler handler)
        {
            handler.Handle(command);
            return NoContent();
        }

        [HttpPost("archive/{id}")]
        [ProducesResponseType(typeof(ErrorResponse), StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        public IActionResult Delete([FromRoute] ArchiveListingCommand command,
            [FromServices] ICommandHandler<ArchiveListingCommand> handler)
        {
            handler.Handle(command);
            return NoContent();
        }

        [HttpGet("myads")]
        [Produces(MimeTypes.Application.Json)]
        [ProducesResponseType(typeof(PaginationResult<ProfileListingItem>), StatusCodes.Status200OK)]
        public IActionResult Get([FromQuery] GetMyAdsQuery query,
            [FromServices] IQueryHandler<GetMyAdsQuery, PaginationResult<ProfileListingItem>> handler)
            => Ok(handler.Handle(query));

        [HttpGet("favorites")]
        [Produces(MimeTypes.Application.Json)]
        [ProducesResponseType(typeof(PaginationResult<ListingItem>), StatusCodes.Status200OK)]
        public IActionResult Favorites([FromQuery] GetListingsQuery query,
            [FromServices] IQueryHandler<GetListingsQuery, PaginationResult<ListingItem>> handler)
            => Ok(handler.Handle(query));

        [HttpGet("following")]
        [Produces(MimeTypes.Application.Json)]
        [ProducesResponseType(typeof(PaginationResult<ListingItem>), StatusCodes.Status200OK)]
        public IActionResult Following([FromQuery] GetListingsQuery query,
            [FromServices] IQueryHandler<GetListingsQuery, PaginationResult<ListingItem>> handler)
            => Ok(handler.Handle(query));

        [HttpGet("archive")]
        [Produces(MimeTypes.Application.Json)]
        [ProducesResponseType(typeof(PaginationResult<ProfileListingItem>), StatusCodes.Status200OK)]
        public IActionResult Get([FromQuery] GetArchiveQuery query,
            [FromServices] IQueryHandler<GetArchiveQuery, PaginationResult<ProfileListingItem>> handler)
            => Ok(handler.Handle(query));

        [HttpPost("like/{id}")]
        public async Task<IActionResult> LikeOrUnlike([FromRoute] LikeOrUnlikeListingCommand command,
            [FromServices] LikeOrUnlikeListingCommandHandler handler)
        {
            var likesCount = await handler.Handle(command);
            return Ok(likesCount);
        }

        [HttpGet("comments")]
        [ProducesResponseType(typeof(PaginationResult<CommentItem>), StatusCodes.Status200OK)]
        public IActionResult Get([FromQuery] GetCommentsQuery query, [FromServices] GetCommentsQueryHandler handler)
        {
            return handler.Handle(query).PipeTo(Ok);
        }

        [HttpPost("comments")]
        public async Task<IActionResult> Post(AddCommentCommand command,
            [FromServices] AddCommentCommandHandler handler)
        {
            await handler.Handle(command);
            return NoContent();
        }
    }
}