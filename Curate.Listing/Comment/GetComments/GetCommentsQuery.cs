﻿using System.ComponentModel.DataAnnotations;
using Curate.Linq;
using Force.Cqrs;
using Force.Linq.Pagination;

namespace Curate.Listing.Comment.GetComments
{
    public class GetCommentsQuery : Paging, IQuery<PaginationResult<CommentItem>>
    {
        [Required]
        public int ListingId { get; set; }
    }
}