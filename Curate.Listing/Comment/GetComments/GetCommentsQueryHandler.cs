﻿using System.Linq;
using AutoMapper;
using AutoMapper.QueryableExtensions;
using Curate.Linq;
using Force.Cqrs;
using Microsoft.EntityFrameworkCore;

namespace Curate.Listing.Comment.GetComments
{
    public class GetCommentsQueryHandler : IQueryHandler<GetCommentsQuery, PaginationResult<CommentItem>>
    {
        private readonly DbContext _dbContext;
        private readonly IMapper _mapper;

        public GetCommentsQueryHandler(DbContext dbContext, IMapper mapper)
        {
            _dbContext = dbContext;
            _mapper = mapper;
        }

        public PaginationResult<CommentItem> Handle(GetCommentsQuery input) =>
            _dbContext.Set<Domain.Listing.Listing>()
                .Where(x => x.Id == input.ListingId)
                .SelectMany(x => x.Comments)
                .ProjectTo<CommentItem>(_mapper.ConfigurationProvider)
                .OrderBy(x => x.CreateDate)
                .ToPaginationResult(input);
    }
}