﻿using System;

namespace Curate.Listing.Comment.GetComments
{
    public class CommentItem
    {
        public long Id { get; set; }
        
        public string AvatarId { get; set; }

        public string FullName { get; set; }

        public DateTime CreateDate { get; set; }

        public string Text { get; set; }
        
        public int UserId { get; set; }
    }
}