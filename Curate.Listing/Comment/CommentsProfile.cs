﻿using Curate.Listing.Comment.GetComments;

namespace Curate.Listing.Comment
{
    public class CommentsProfile : AutoMapper.Profile
    {
        public CommentsProfile()
        {
            CreateMap<Domain.Entities.Comment, CommentItem>()
                .ForMember(dest => dest.FullName, opt =>
                    opt.MapFrom(src => src.Sender.Profile.FullName))
                .ForMember(dest => dest.UserId, opt =>
                    opt.MapFrom(src => src.Sender.Id))
                .ForMember(dest => dest.AvatarId, opt =>
                    opt.MapFrom(src => src.Sender.Profile.PhotoId));
        }
    }
}