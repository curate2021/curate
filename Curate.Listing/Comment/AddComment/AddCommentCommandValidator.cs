﻿using FluentValidation;

namespace Curate.Listing.Comment.AddComment
{
    public class AddCommentCommandValidator : AbstractValidator<AddCommentCommand>
    {
        public AddCommentCommandValidator()
        {
            RuleFor(x => x.Text)
                .NotEmpty()
                .MaximumLength(byte.MaxValue);

            RuleFor(x => x.ListingId)
                .NotEmpty()
                .GreaterThan(0);
        }
    }
}