﻿using System.Threading.Tasks;
using Force.Cqrs;

namespace Curate.Listing.Comment.AddComment
{
    public class AddCommentCommand : ICommand<Task>
    {
        public int ListingId { get; set; }
        
        public string Text { get; set; }
    }
}