﻿using System.Threading.Tasks;
using Curate.Core;
using Curate.Domain.Entities;
using Force.Cqrs;
using Microsoft.EntityFrameworkCore;

namespace Curate.Listing.Comment.AddComment
{
    public class AddCommentCommandHandler : ICommandHandler<AddCommentCommand, Task>
    {
        private readonly DbContext _dbContext;
        private readonly IUserContext<User> _userContext;

        public AddCommentCommandHandler(DbContext dbContext, IUserContext<User> userContext)
        {
            _dbContext = dbContext;
            _userContext = userContext;
        }

        public async Task Handle(AddCommentCommand input)
        {
            await _dbContext.Set<Domain.Entities.Comment>()
                .AddAsync(new Domain.Entities.Comment(input.Text, _userContext.UserId, input.ListingId));

            await _dbContext.SaveChangesAsync();
        }
    }
}