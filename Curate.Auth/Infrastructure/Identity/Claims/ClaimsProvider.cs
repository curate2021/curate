﻿using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using Curate.Domain.Entities;
using Curate.Identity;
using Microsoft.AspNetCore.Identity;

namespace Curate.Auth.Infrastructure.Identity.Claims
{
    public class ClaimsProvider
    {
        private readonly UserManager<User> _userManager;

        public ClaimsProvider(UserManager<User> userManager)
        {
            _userManager = userManager;
        }

        public async Task<ClaimsIdentity> CreateUserClaimsAsync(User user)
        {
            var roles = await _userManager
                .GetRolesAsync(user);

            var claims = new List<Claim>
            {
                new Claim(ClaimTypes.NameIdentifier, user.Id.ToString()),
                new Claim(ClaimsIdentity.DefaultNameClaimType, user.Email),
                new Claim(ClaimsIdentity.DefaultRoleClaimType, roles.First())
            };

            var claimsIdentity =
                new ClaimsIdentity(
                    claims,
                    "Token",
                    ClaimsIdentity.DefaultNameClaimType,
                    ClaimsIdentity.DefaultRoleClaimType);

            return claimsIdentity;
        }
    }
}