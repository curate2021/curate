﻿using System;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Tokens;

namespace Curate.Auth.Infrastructure.Identity.Jwt
{
    public class JwtTokenProvider
    {
        private readonly AuthOptions _authOptions;

        public JwtTokenProvider(IOptions<AuthOptions> authOptions)
        {
            _authOptions = authOptions.Value;
        }

        public string GetToken(ClaimsIdentity identity)
        {
            var utcNow = DateTime.UtcNow;
            var jwt = new JwtSecurityToken(
                _authOptions.Issuer,
                _authOptions.Audience,
                identity.Claims,
                utcNow,
                utcNow.Add(TimeSpan.FromDays(_authOptions.Lifetime)),
                new SigningCredentials(_authOptions.JwtKey, SecurityAlgorithms.HmacSha256Signature));

            return new JwtSecurityTokenHandler()
                .WriteToken(jwt);
        }
    }
}