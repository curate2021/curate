﻿using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Tokens;

namespace Curate.Auth.Infrastructure.Identity.Jwt
{
    public class ConfigureJwtBearerOptions : IPostConfigureOptions<JwtBearerOptions>
    {
        private readonly IOptions<AuthOptions> _authOptions;

        public ConfigureJwtBearerOptions(IOptions<AuthOptions> authOptions)
        {
            _authOptions = authOptions;
        }

        public void PostConfigure(string name, JwtBearerOptions options)
        {
            var authOptions = _authOptions.Value;
            options.RequireHttpsMetadata = false;
            options.SaveToken = true;

            options.TokenValidationParameters = new TokenValidationParameters
            {
                IssuerSigningKey = authOptions.JwtKey,
                ValidateIssuerSigningKey = true,
                ValidAudience = authOptions.Audience,
                ValidateAudience = true,
                ValidIssuer = authOptions.Issuer,
                ValidateIssuer = true
            };
        }
    }
}