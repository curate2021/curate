﻿using System;
using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.Options;

namespace Curate.Auth.Infrastructure.Identity
{
    public class ConfigureIdentityOptions : IPostConfigureOptions<IdentityOptions>
    {
        public void PostConfigure(string name, IdentityOptions options)
        {
            options.Password.RequireDigit = false;
            options.Password.RequiredLength = 4;
            options.Password.RequireNonAlphanumeric = false;
            options.Password.RequireUppercase = false;
            options.Password.RequireLowercase = false;
            options.Password.RequiredUniqueChars = 1;
            options.SignIn.RequireConfirmedEmail = false;

            options.Lockout.DefaultLockoutTimeSpan = TimeSpan.FromMinutes(15);
            options.Lockout.MaxFailedAccessAttempts = 5;
            options.Lockout.AllowedForNewUsers = true;
        }
    }
}