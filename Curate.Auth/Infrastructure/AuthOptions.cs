﻿using System.Text;
using Microsoft.IdentityModel.Tokens;

namespace Curate.Auth.Infrastructure
{
    public class AuthOptions
    {
        public string Secret { get; set; }

        public string Audience { get; set; }
        
        public string Issuer { get; set; }
        
        public int Lifetime { get; set; }
        
        public SymmetricSecurityKey JwtKey => new SymmetricSecurityKey(Encoding.ASCII.GetBytes(Secret));
    }
}