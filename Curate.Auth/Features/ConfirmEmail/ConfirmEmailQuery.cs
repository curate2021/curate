﻿using System.Threading.Tasks;
using Force.Cqrs;

namespace Curate.Auth.Features.ConfirmEmail
{
    public class ConfirmEmailQuery : IQuery<Task>
    {
        public int UserId { get; set; }
        
        public string Token { get; set; }
    }
}