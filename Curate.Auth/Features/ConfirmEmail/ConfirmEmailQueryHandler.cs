﻿using System;
using System.Net;
using System.Text;
using System.Text.Unicode;
using System.Threading.Tasks;
using Curate.Domain.Entities;
using Curate.Exceptions;
using Force.Cqrs;
using Microsoft.AspNetCore.Identity;

namespace Curate.Auth.Features.ConfirmEmail
{
    public class ConfirmEmailQueryHandler : IQueryHandler<ConfirmEmailQuery, Task>
    {
        private readonly UserManager<User> _userManager;

        public ConfirmEmailQueryHandler(UserManager<User> userManager)
        {
            _userManager = userManager;
        }

        public async Task Handle(ConfirmEmailQuery input)
        {
            var user = await _userManager.FindByIdAsync(input.UserId.ToString());

            var confirmResult = await _userManager.ConfirmEmailAsync(user, input.Token.Decode());
            if (!confirmResult.Succeeded)
            {
                throw new HttpException(HttpStatusCode.BadRequest, string.Join(", ", confirmResult.Errors));
            }
        }
    }
}