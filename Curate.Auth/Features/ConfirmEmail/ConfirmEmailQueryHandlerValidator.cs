﻿using Curate.Domain.Entities;
using FluentValidation;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;

namespace Curate.Auth.Features.ConfirmEmail
{
    public class ConfirmEmailQueryHandlerValidator : AbstractValidator<ConfirmEmailQuery>
    {
        public ConfirmEmailQueryHandlerValidator(UserManager<User> userManager)
        {
            RuleFor(x => x.Token).NotEmpty();
            RuleFor(x => x.UserId).NotEmpty();
            RuleFor(x => x)
                .MustAsync(async (userInfo, ct) =>
                    await userManager.FindByIdAsync(userInfo.UserId.ToString()) != null)
                .WithErrorCode(StatusCodes.Status404NotFound.ToString());

        }
    }
}