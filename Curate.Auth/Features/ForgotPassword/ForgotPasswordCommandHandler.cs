﻿using System;
using System.Threading.Tasks;
using Curate.Domain.Entities;
using Curate.EmailManager.Manager;
using Curate.EmailManager.Message;
using Force.Cqrs;
using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.Options;
using Serilog;

namespace Curate.Auth.Features.ForgotPassword
{
    public class ForgotPasswordCommandHandler : ICommandHandler<ForgotPasswordCommand, Task>
    {
        private readonly UserManager<User> _userManager;
        private readonly IEmailManager _emailManager;
        private readonly ILogger _logger;
        private readonly EmailMessageTemplate _emailMessageTemplate;

        public ForgotPasswordCommandHandler(UserManager<User> userManager, IEmailManager emailManager,
            IOptions<EmailMessageOptions> emailMessageOptions, ILogger logger)
        {
            _userManager = userManager;
            _emailManager = emailManager;
            _logger = logger;
            _emailMessageTemplate = 
                emailMessageOptions.Value.GetEmailMessageTemplate(EmailMessageTemplateType.ForgotPassword);
        }

        public async Task Handle(ForgotPasswordCommand input)
        {
            try
            {
                var user = await _userManager.FindByEmailAsync(input.Email);

                if (user == null) return;

                var token = await _userManager.GeneratePasswordResetTokenAsync(user);

                await _emailManager.Send(new EmailMessage
                {
                    To = user.Email,
                    IsHtmlBody = true,
                    Subject = _emailMessageTemplate.Subject,
                    Body = string.Format(_emailMessageTemplate.Body, token.Encode())
                });
            }
            catch (Exception e)
            {
                _logger.Error(e.Message);
            }
        }
    }
}