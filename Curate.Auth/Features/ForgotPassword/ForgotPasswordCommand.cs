﻿using System.Threading.Tasks;
using Force.Cqrs;

namespace Curate.Auth.Features.ForgotPassword
{
    public class ForgotPasswordCommand : ICommand<Task>
    {
        public string Email { get; set; }
    }
}