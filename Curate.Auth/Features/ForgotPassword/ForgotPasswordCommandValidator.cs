﻿using System.Net;
using Curate.Domain.Entities;
using Curate.Exceptions;
using FluentValidation;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;

namespace Curate.Auth.Features.ForgotPassword
{
    public class ForgotPasswordCommandValidator : AbstractValidator<ForgotPasswordCommand>
    {
        public ForgotPasswordCommandValidator(UserManager<User> userManager)
        {
            RuleFor(x => x.Email)
                .NotEmpty();

            RuleFor(x => x)
                .MustAsync(async (e, ct) =>
                {
                    var user = await userManager.FindByEmailAsync(e.Email);

                    if (user == null)
                    {
                        throw new HttpException(HttpStatusCode.NotFound);
                    }

                    return user.EmailConfirmed;
                })
                .WithErrorCode(HttpStatusCode.BadRequest.ToString())
                .WithMessage("You didn't confirmed your email");
        }
    }
}