﻿namespace Curate.Auth.Features
{
    public class AccountInfoDto
    {
        public string Token { get; set; }

        public string Email { get; set; }

        public string Role { get; set; }

        public int Id { get; set; }
    }
}