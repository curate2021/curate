﻿using System.Threading.Tasks;
using Force.Cqrs;

namespace Curate.Auth.Features.ResetPassword
{
    public class ResetPasswordCommand : ICommand<Task>
    {
        public string Email { get; set; }
        
        public string Password { get; set; }
        
        public string Token { get; set; }
    }
}