﻿using Curate.Domain.Entities;
using FluentValidation;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;

namespace Curate.Auth.Features.ResetPassword
{
    public class ResetPasswordCommandValidator : AbstractValidator<ResetPasswordCommand>
    {
        public ResetPasswordCommandValidator(UserManager<User> userManager)
        {
            RuleFor(x => x.Email)
                .NotEmpty();
            RuleFor(x => x.Token)
                .NotEmpty();
            RuleFor(x => x.Password)
                .NotEmpty();

            RuleFor(x => x)
                .MustAsync(async (e, ct) =>
                {
                    var user = await userManager.FindByEmailAsync(e.Email);
                    return user != null && user.EmailConfirmed;
                })
                .WithErrorCode(StatusCodes.Status400BadRequest.ToString());
        }
    }
}