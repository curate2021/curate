﻿using System;
using System.Threading.Tasks;
using Curate.Domain.Entities;
using Curate.Exceptions;
using Force.Cqrs;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;

namespace Curate.Auth.Features.ResetPassword
{
    public class ResetPasswordCommandHandler : ICommandHandler<ResetPasswordCommand, Task>
    {
        private readonly UserManager<User> _userManager;

        public ResetPasswordCommandHandler(UserManager<User> userManager)
        {
            _userManager = userManager;
        }

        public async Task Handle(ResetPasswordCommand input)
        {
            var user = await _userManager.FindByEmailAsync(input.Email);

            var resetPasswordResult = await _userManager.ResetPasswordAsync(user, input.Token.Decode(), input.Password);

            if (!resetPasswordResult.Succeeded)
            {
                throw new HttpException(StatusCodes.Status422UnprocessableEntity,
                    string.Join(Environment.NewLine, resetPasswordResult.Errors));
            }
        }
    }
}