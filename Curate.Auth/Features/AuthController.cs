﻿using System.Threading.Tasks;
using Curate.Auth.Features.ConfirmEmail;
using Curate.Auth.Features.ForgotPassword;
using Curate.Auth.Features.ResetPassword;
using Curate.Auth.Features.SendEmailConfirmation;
using Curate.Auth.Features.SignIn;
using Curate.Auth.Features.SignUp;
using Curate.Mvc;
using Curate.Mvc.ControllerHelpers;
using Force.Cqrs;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Components;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace Curate.Auth.Features
{
    public class AuthController : ApiControllerBase
    {
        [HttpPost]
        [ProducesResponseType(typeof(SignUpResponse), StatusCodes.Status200OK)]
        [ProducesResponseType(typeof(ErrorResponse), StatusCodes.Status400BadRequest)]
        public async Task<IActionResult> SignUp(SignUpCommand command, [FromServices] SignUpCommandHandler handler)
        {
            return Ok(await handler.Handle(command));
        }

        [HttpPost]
        [ProducesResponseType(typeof(SignInResponse), StatusCodes.Status200OK)]
        [ProducesResponseType(typeof(ErrorResponse), StatusCodes.Status400BadRequest)]
        public async Task<IActionResult> SignIn(SignInCommand command, [FromServices] SignInCommandHandler handler)
        {
            return Ok(await handler.Handle(command));
        }

        /// <summary>
        /// обработка ссылки для подтверждения почты
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<IActionResult> Confirm([FromQuery] ConfirmEmailQuery query,
            [FromServices] ConfirmEmailQueryHandler handler)
        {
            await handler.Handle(query);
            return Ok("Email successfully confirmed");
        }

        /// <summary>
        /// пользователь в приложении авторизованным нажимает на кнопку "подтвердить почту"
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        [ProducesResponseType(typeof(ErrorResponse), StatusCodes.Status400BadRequest)]
        public async Task<IActionResult> Confirm([FromQuery] SendEmailConfirmationCommand command,
            [FromServices] ICommandHandler<SendEmailConfirmationCommand, Task<bool>> handler)
        {
            await handler.Handle(command);
            return NoContent();
        }

        [HttpPost]
        [ProducesResponseType(typeof(ErrorResponse), StatusCodes.Status400BadRequest)]
        [ProducesResponseType(typeof(ErrorResponse), StatusCodes.Status404NotFound)]
        public async Task<IActionResult> ForgotPassword([FromBody] ForgotPasswordCommand command,
            [FromServices] ICommandHandler<ForgotPasswordCommand, Task> handler)
        {
            await handler.Handle(command);
            return NoContent();
        }

        [HttpPost]
        [ProducesResponseType(typeof(ErrorResponse), StatusCodes.Status400BadRequest)]
        [ProducesResponseType(typeof(ErrorResponse), StatusCodes.Status422UnprocessableEntity)]
        public async Task<IActionResult> ResetPassword([FromBody] ResetPasswordCommand command,
            [FromServices] ICommandHandler<ResetPasswordCommand, Task> handler)
        {
            await handler.Handle(command);
            return NoContent();
        }
    }
}