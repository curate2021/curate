﻿using System.Threading.Tasks;
using Force.Cqrs;

namespace Curate.Auth.Features.SendEmailConfirmation
{
    public class SendEmailConfirmationCommand : ICommand<Task<bool>>
    {
        public SendEmailConfirmationCommand(int userId)
        {
            UserId = userId;
        }
        
        public int UserId { get; set; }
    }
}