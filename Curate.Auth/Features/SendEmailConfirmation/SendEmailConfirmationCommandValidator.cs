﻿using Curate.Domain.Entities;
using FluentValidation;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;

namespace Curate.Auth.Features.SendEmailConfirmation
{
    public class SendEmailConfirmationCommandValidator : AbstractValidator<SendEmailConfirmationCommand>
    {
        public SendEmailConfirmationCommandValidator(UserManager<User> userManager)
        {
            RuleFor(x => x.UserId)
                .NotEmpty();

            RuleFor(x => x)
                .MustAsync(async (e, ct) =>
                {
                    var user = await userManager.FindByIdAsync(e.UserId.ToString());
                    return user != null && !user.EmailConfirmed;
                })
                .WithErrorCode(StatusCodes.Status400BadRequest.ToString());
        }
    }
}