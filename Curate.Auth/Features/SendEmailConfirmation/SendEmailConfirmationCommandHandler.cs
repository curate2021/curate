﻿using System;
using System.Text;
using System.Threading.Tasks;
using Curate.Domain.Entities;
using Curate.EmailManager.Manager;
using Curate.EmailManager.Message;
using Force.Cqrs;
using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.Options;
using Serilog;

namespace Curate.Auth.Features.SendEmailConfirmation
{
    public class SendEmailConfirmationCommandHandler : ICommandHandler<SendEmailConfirmationCommand, Task<bool>>
    {
        private readonly IEmailManager _emailManager;
        private readonly ILogger _logger;
        private readonly EmailMessageTemplate _emailMessageTemplate;
        private readonly UserManager<User> _userManager;

        public SendEmailConfirmationCommandHandler(UserManager<User> userManager, IEmailManager emailManager,
            IOptions<EmailMessageOptions> emailMessageOptions, ILogger logger)
        {
            _userManager = userManager;
            _emailManager = emailManager;
            _logger = logger;
            _emailMessageTemplate =
                emailMessageOptions.Value.GetEmailMessageTemplate(EmailMessageTemplateType.EmailConfirmation);
        }

        public async Task<bool> Handle(SendEmailConfirmationCommand input)
        {
            var isEmailSuccessfullySend = true;

            try
            {
                var user = await _userManager.FindByIdAsync(input.UserId.ToString());

                if (user == null) isEmailSuccessfullySend = false;

                var (token, userId) = (await _userManager.GenerateEmailConfirmationTokenAsync(user), user!.Id);

                await _emailManager.Send(new EmailMessage
                {
                    To = user.Email,
                    IsHtmlBody = true,
                    Subject = _emailMessageTemplate.Subject,
                    Body = string.Format(_emailMessageTemplate.Body,
                        token.Encode(), userId)
                });
            }
            catch (Exception e)
            {
                _logger.Error(e.Message);
                isEmailSuccessfullySend = false;
            }

            return isEmailSuccessfullySend;
        }
    }
}