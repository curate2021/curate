﻿using System.ComponentModel.DataAnnotations;
using System.Threading.Tasks;
using Force.Cqrs;

namespace Curate.Auth.Features.SignIn
{
    public class SignInCommand : ICommand<Task<SignInResponse>>
    {
        [EmailAddress]
        public string Email { get; set; }
        
        public string Password { get; set; }
    }
}