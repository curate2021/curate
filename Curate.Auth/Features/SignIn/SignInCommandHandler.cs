﻿using System.Linq;
using System.Threading.Tasks;
using Curate.Auth.Infrastructure.Identity.Claims;
using Curate.Auth.Infrastructure.Identity.Jwt;
using Curate.Domain.Entities;
using Force.Cqrs;
using Microsoft.AspNetCore.Identity;

namespace Curate.Auth.Features.SignIn
{
    public class SignInCommandHandler : ICommandHandler<SignInCommand, Task<SignInResponse>>
    {
        private readonly ClaimsProvider _claimsProvider;
        private readonly JwtTokenProvider _jwtTokenProvider;
        private readonly UserManager<User> _userManager;

        public SignInCommandHandler(UserManager<User> userManager, ClaimsProvider claimsProvider,
            JwtTokenProvider jwtTokenProvider)
        {
            _userManager = userManager;
            _claimsProvider = claimsProvider;
            _jwtTokenProvider = jwtTokenProvider;
        }

        public async Task<SignInResponse> Handle(SignInCommand input)
        {
            var user = await _userManager.FindByEmailAsync(input.Email);

            await _userManager.ResetAccessFailedCountAsync(user);

            var role = (await _userManager.GetRolesAsync(user))
                .First();

            return new SignInResponse
            {
                Email = user.Email,
                Role = role,
                Token = _jwtTokenProvider.GetToken(await _claimsProvider.CreateUserClaimsAsync(user)),
                Id = user.Id,
                IsEmailConfirmed = user.EmailConfirmed
            };
        }
    }
}