﻿namespace Curate.Auth.Features.SignIn
{
    public class SignInResponse : AccountInfoDto
    {
        public bool IsEmailConfirmed { get; set; }
    }
}