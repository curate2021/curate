﻿using Curate.Domain.Entities;
using FluentValidation;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;

namespace Curate.Auth.Features.SignIn
{
    public class SignInCommandValidator : AbstractValidator<SignInCommand>
    {
        public SignInCommandValidator(UserManager<User> userManager)
        {
            RuleFor(x => x.Password)
                .NotEmpty();
            
            RuleFor(x => x.Email)
                .NotEmpty();
            
            RuleFor(x => x).MustAsync(async (e, ct) =>
                {
                    var user = await userManager.FindByEmailAsync(e.Email);
                    return user != null && await userManager.CheckPasswordAsync(user, e.Password);
                })
                .WithMessage("Incorrect email or password")
                .WithErrorCode(StatusCodes.Status400BadRequest.ToString());
        }
    }
}