﻿using Curate.Domain.Entities;
using FluentValidation;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;

namespace Curate.Auth.Features.SignUp
{
    public class SignUpCommandValidator : AbstractValidator<SignUpCommand>
    {
        public SignUpCommandValidator(UserManager<User> userManager, IPasswordValidator<User> passwordValidator,
            DbContext dbContext)
        {
            RuleFor(x => x.Password)
                .NotEmpty();

            RuleFor(x => x.Email)
                .NotEmpty();

            RuleFor(x => x.Name)
                .NotEmpty();

            RuleFor(x => x.CountryId)
                .NotEmpty();

            RuleFor(x => x.Surname)
                .NotEmpty();

            RuleFor(x => x).MustAsync(async (e, ct) =>
                {
                    var user = await userManager.FindByEmailAsync(e.Email);
                    if (user != null)
                    {
                        return false;
                    }

                    var validate = await passwordValidator.ValidateAsync(userManager, null, e.Password);
                    return validate.Succeeded;
                })
                .WithMessage("Email in use or incorrect password")
                .WithErrorCode(StatusCodes.Status400BadRequest.ToString());

            RuleFor(x => x)
                .MustAsync(async (e, ct) => await dbContext.Set<Country>()
                    .AnyAsync(x => x.Id == e.CountryId, ct))
                .WithMessage("Cannot find country")
                .WithErrorCode(StatusCodes.Status400BadRequest.ToString());
        }
    }
}