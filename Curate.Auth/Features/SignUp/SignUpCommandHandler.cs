﻿using System;
using System.Threading.Tasks;
using Curate.Auth.Features.SendEmailConfirmation;
using Curate.Auth.Infrastructure.Identity.Claims;
using Curate.Auth.Infrastructure.Identity.Jwt;
using Curate.Domain;
using Curate.Domain.Entities;
using Curate.EmailManager.Manager;
using Force.Cqrs;
using Microsoft.AspNetCore.Identity;

namespace Curate.Auth.Features.SignUp
{
    public class SignUpCommandHandler : ICommandHandler<SignUpCommand, Task<SignUpResponse>>
    {
        private readonly ClaimsProvider _claimsProvider;
        private readonly IEmailManager _emailManager;
        private readonly ICommandHandler<SendEmailConfirmationCommand, Task<bool>> _sendEmailConfirmationCommandHandler;
        private readonly JwtTokenProvider _jwtTokenProvider;
        private readonly UserManager<User> _userManager;

        public SignUpCommandHandler(UserManager<User> userManager, ClaimsProvider claimsProvider,
            JwtTokenProvider jwtTokenProvider, IEmailManager emailManager,
            ICommandHandler<SendEmailConfirmationCommand, Task<bool>> sendEmailConfirmationCommandHandler)
        {
            _userManager = userManager;
            _claimsProvider = claimsProvider;
            _jwtTokenProvider = jwtTokenProvider;
            _emailManager = emailManager;
            _sendEmailConfirmationCommandHandler = sendEmailConfirmationCommandHandler;
        }

        public async Task<SignUpResponse> Handle(SignUpCommand input)
        {
            var user = new User(input.Name, input.Surname, input.Email, input.CountryId);
            var create = await _userManager.CreateAsync(user, input.Password);
            if (!create.Succeeded) throw new ArgumentException($"Cannot create user {string.Join(' ', create.Errors)}");

            var addToRole = await _userManager.AddToRoleAsync(user, RoleNames.User.ToString());

            if (!addToRole.Succeeded)
                throw new ArgumentException($"Cannot add user to role {string.Join(' ', addToRole.Errors)}");

            user.NormalizedEmail = _userManager.NormalizeEmail(input.Email);
            user.NormalizedUserName = _userManager.NormalizeName(input.Email);

            await _userManager.UpdateAsync(user);

            user = await _userManager.FindByEmailAsync(user.Email);

            await _userManager.ResetAccessFailedCountAsync(user);

            var isEmailConfirmationSend =
                await _sendEmailConfirmationCommandHandler.Handle(new SendEmailConfirmationCommand(user.Id));

            return new SignUpResponse
            {
                Email = input.Email,
                Role = RoleNames.User.ToString(),
                Token = _jwtTokenProvider.GetToken(await _claimsProvider.CreateUserClaimsAsync(user)),
                Id = user!.Id,
                IsConfirmationEmailSend = isEmailConfirmationSend
            };
        }
    }
}