﻿namespace Curate.Auth.Features.SignUp
{
    public class SignUpResponse : AccountInfoDto
    {
        public bool IsConfirmationEmailSend { get; set; }
    }
}