﻿using System;
using System.Text;

namespace Curate.Auth
{
    public static class EncodingExtensions
    {
        public static string Decode(this string token) => Encoding.UTF8.GetString(Convert.FromBase64String(token));

        public static string Encode(this string token) => Convert.ToBase64String(Encoding.UTF8.GetBytes(token));
    }
}