﻿using System.IO;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;

namespace Curate.Core
{
    public interface IFileClient
    {
        Task SaveFile(string id, IFormFile formFile);

        Task<string> SaveFile(IFormFile formFile);

        Task<Stream> GetFile(string id);

        Task DeleteFile(string id);

        Task<bool> FileExists(string id);
    }
}