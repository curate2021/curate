﻿using System.Threading;
using System.Threading.Tasks;

namespace Curate.Core.Notification
{
    public interface INotificationHandler<in TNotification> where TNotification : INotification
    {
        Task Handle(TNotification sendLikePushNotificationCommand, CancellationToken cancellationToken = default);
    }
}