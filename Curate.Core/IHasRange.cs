﻿namespace Curate.Core
{
    public interface IHasRange
    {
        public decimal From { get; set; }
        
        public decimal To { get; set; }
    }
}