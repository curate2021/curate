﻿using System.Linq;

namespace Curate.Core
{
    public interface IUserContext<T>
    {
        IQueryable<T> UserQuery { get; }
        T User { get; }
        int UserId { get; }
    }
}