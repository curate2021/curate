﻿namespace Curate.Identity
{
    public class PolicyConstants
    {
        public const string AdminPolicy = "AdminPolicy";
        
        public const string UserPolicy = "UserPolicy";
    }
}