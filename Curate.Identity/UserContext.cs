﻿using System.Linq;
using System.Security.Claims;
using Curate.Core;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;

namespace Curate.Identity
{
    public class UserContext<T> : IUserContext<T> where T : IdentityUser<int>
    {
        private readonly string _userEmail;

        public UserContext(IHttpContextAccessor httpContextAccessor, DbContext dbContext)
        {
            _userEmail = httpContextAccessor.HttpContext?.User.Claims
                .FirstOrDefault(x => x.Type == ClaimsIdentity.DefaultNameClaimType)
                ?.Value;
            UserQuery = dbContext
                .Set<T>()
                .Where(x => x.NormalizedEmail == _userEmail.ToUpper());
        }

        public IQueryable<T> UserQuery { get; }

        private T _user { get; set; }

        public T User
        {
            get
            {
                if (!string.IsNullOrEmpty(_userEmail) && _user == null)
                {
                    _user = UserQuery.FirstOrDefault();
                }

                return _user;
            }
        }

        public int UserId
        {
            get
            {
                if (User != null)
                {
                    return User.Id;
                }

                return default;
            }
        }
    }
}