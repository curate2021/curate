﻿using System.Linq;
using Curate.Core;
using Curate.Domain.Entities;
using Force.Cqrs;
using Microsoft.EntityFrameworkCore;

namespace Curate.Notification.Features.Firebase.DeleteFirebaseToken
{
    public class DeleteFirebaseTokenCommandHandler : ICommandHandler<DeleteFirebaseTokenCommand>
    {
        private readonly IUserContext<User> _userContext;
        private readonly DbContext _dbContext;

        public DeleteFirebaseTokenCommandHandler(IUserContext<User> userContext, DbContext dbContext)
        {
            _userContext = userContext;
            _dbContext = dbContext;
        }
        public void Handle(DeleteFirebaseTokenCommand input)
        {
            var firebaseToken = _dbContext.Set<Domain.Entities.Firebase>()
                .FirstOrDefault(x => x.UserId == _userContext.UserId);
            if (firebaseToken != null)
            {
                firebaseToken.Delete();
                _dbContext.SaveChanges();
            }
        }
    }
}