﻿using System.ComponentModel.DataAnnotations;
using Force.Cqrs;

namespace Curate.Notification.Features.Firebase.AddFirebaseToken
{
    public class AddFirebaseTokenCommand : ICommand
    {
        [Required]
        public string Token { get; set; }
    }
}