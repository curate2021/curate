﻿using System.Linq;
using Curate.Core;
using Curate.Domain.Entities;
using Force.Cqrs;
using Microsoft.EntityFrameworkCore;

namespace Curate.Notification.Features.Firebase.AddFirebaseToken
{
    public class AddFirebaseTokenCommandHandler : ICommandHandler<AddFirebaseTokenCommand>
    {
        private readonly IUserContext<User> _userContext;
        private readonly DbContext _dbContext;

        public AddFirebaseTokenCommandHandler(IUserContext<User> userContext, DbContext dbContext)
        {
            _userContext = userContext;
            _dbContext = dbContext;
        }

        public void Handle(AddFirebaseTokenCommand input)
        {
            var firebase = _dbContext
                .Set<Domain.Entities.Firebase>()
                .IgnoreQueryFilters()
                .FirstOrDefault(x => x.UserId == _userContext.UserId);
            if (firebase == null)
            {
                firebase = new Domain.Entities.Firebase(_userContext.User, input.Token);
                _dbContext.Add(firebase);
            }
            else
            {
                firebase.SetToken(input.Token);   
            }
            _dbContext.SaveChanges();
        }
    }
}