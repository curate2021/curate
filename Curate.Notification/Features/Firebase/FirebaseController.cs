﻿using Curate.Mvc;
using Curate.Mvc.ControllerHelpers;
using Curate.Notification.Features.Firebase.AddFirebaseToken;
using Curate.Notification.Features.Firebase.DeleteFirebaseToken;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace Curate.Notification.Features.Firebase
{
    [Route("api/[controller]")]
    public class FirebaseController : AuthControllerBase
    {
        [HttpPost]
        [Produces(MimeTypes.Application.Json)]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        public IActionResult Post([FromBody] AddFirebaseTokenCommand command,
            [FromServices] AddFirebaseTokenCommandHandler handler)
        {
            handler.Handle(command);
            return NoContent();
        }
        
        [HttpDelete]
        [Produces(MimeTypes.Application.Json)]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        public IActionResult Delete([FromBody] DeleteFirebaseTokenCommand command,
            [FromServices] DeleteFirebaseTokenCommandHandler handler)
        {
            handler.Handle(command);
            return NoContent();
        }
    }
}