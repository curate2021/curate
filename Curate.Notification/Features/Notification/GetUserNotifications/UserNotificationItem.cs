﻿using Curate.Domain.Entities;

namespace Curate.Notification.Features.Notification.GetUserNotifications
{
    public class UserNotificationItem : UserNotificationItemBase
    {
        public NotificationType NotificationType { get; set; }
    }
}