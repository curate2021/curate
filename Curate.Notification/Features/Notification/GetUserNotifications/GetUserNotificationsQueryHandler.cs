﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using AutoMapper.QueryableExtensions;
using Curate.Core;
using Curate.Domain.Entities;
using Force.Cqrs;
using Microsoft.EntityFrameworkCore;

namespace Curate.Notification.Features.Notification.GetUserNotifications
{
    public class
        GetUserNotificationsQueryHandler : IQueryHandler<GetUserNotificationsQuery, Task<IEnumerable<UserNotificationItem>>>
    {
        private readonly IUserContext<User> _userContext;
        private readonly DbContext _dbContext;
        private readonly IMapper _mapper;

        public GetUserNotificationsQueryHandler(IUserContext<User> userContext, DbContext dbContext, IMapper mapper)
        {
            _userContext = userContext;
            _dbContext = dbContext;
            _mapper = mapper;
        }

        public async Task<IEnumerable<UserNotificationItem>> Handle(GetUserNotificationsQuery input) =>
            await _dbContext.Set<UserNotification>()
                .Where(x => x.UserId == _userContext.UserId)
                .ProjectTo<UserNotificationItem>(_mapper.ConfigurationProvider)
                .ToArrayAsync();
    }
}