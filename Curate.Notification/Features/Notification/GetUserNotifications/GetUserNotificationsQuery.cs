﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Force.Cqrs;

namespace Curate.Notification.Features.Notification.GetUserNotifications
{
    public class GetUserNotificationsQuery : IQuery<Task<IEnumerable<UserNotificationItem>>>
    {
        
    }
}