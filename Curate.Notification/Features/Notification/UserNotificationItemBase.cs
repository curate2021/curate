﻿namespace Curate.Notification.Features.Notification
{
    public abstract class UserNotificationItemBase
    {
        public bool IsDisabled { get; set; }
        
        public int Id { get; set; }
    }
}