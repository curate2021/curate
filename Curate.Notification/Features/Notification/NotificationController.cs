﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Curate.Mvc;
using Curate.Mvc.ControllerHelpers;
using Curate.Notification.Features.Notification.GetUserNotifications;
using Curate.Notification.Features.Notification.SetNotifications;
using Force.Extensions;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace Curate.Notification.Features.Notification
{
    [Route("api/[controller]")]
    public class NotificationController : AuthControllerBase
    {
        [HttpPut]
        [Produces(MimeTypes.Application.Json)]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        public async Task<IActionResult> Put([FromBody] SetNotificationsCommand command,
            [FromServices] SetNotificationsCommandHandler handler)
        {
            await handler.Handle(command);
            return NoContent();
        }

        [HttpGet]
        [Produces(MimeTypes.Application.Json)]
        [ProducesResponseType(typeof(IEnumerable<UserNotificationItem>), StatusCodes.Status200OK)]
        public async Task<IActionResult> Get([FromQuery] GetUserNotificationsQuery query,
            [FromServices] GetUserNotificationsQueryHandler handler) =>
            (await handler.Handle(query)).PipeTo(Ok);
    }
}