﻿using Curate.Domain.Entities;
using Curate.Notification.Features.Notification.GetUserNotifications;
using Profile = AutoMapper.Profile;

namespace Curate.Notification.Features.Notification
{
    public class NotificationsProfile : Profile
    {
        public NotificationsProfile()
        {
            CreateMap<UserNotification, UserNotificationItem>()
                .ForMember(dest => dest.NotificationType, opt =>
                    opt.MapFrom(src => src.Notification.NotificationType));
        }
    }
}