﻿using System.Linq;
using System.Threading.Tasks;
using Curate.Core;
using Curate.Domain.Entities;
using Force.Cqrs;
using Microsoft.EntityFrameworkCore;

namespace Curate.Notification.Features.Notification.SetNotifications
{
    public class SetNotificationsCommandHandler : ICommandHandler<SetNotificationsCommand, Task>
    {
        private readonly IUserContext<User> _userContext;
        private readonly DbContext _dbContext;

        public SetNotificationsCommandHandler(IUserContext<User> userContext, DbContext dbContext)
        {
            _userContext = userContext;
            _dbContext = dbContext;
        }

        public async Task Handle(SetNotificationsCommand input)
        {
            var inputNotificationsDictionary = input.NotificationItems
                .ToDictionary(x => x.Id, x => x.IsDisabled);

            var inputNotificationIds = inputNotificationsDictionary
                .Keys
                .ToArray();

            var userNotifications = _userContext.User.UserNotifications
                .Where(x => inputNotificationIds.Contains(x.Id));

            foreach (var userNotification in userNotifications)
            {
                userNotification.Toggle(inputNotificationsDictionary[userNotification.Id]);
            }

            await _dbContext.SaveChangesAsync();
        }
    }
}