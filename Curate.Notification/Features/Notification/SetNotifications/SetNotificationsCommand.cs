﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Force.Cqrs;

namespace Curate.Notification.Features.Notification.SetNotifications
{
    public class SetNotificationsCommand : ICommand<Task>
    {
        public IEnumerable<SetNotificationItem> NotificationItems { get; set; }
    }

    public class SetNotificationItem : UserNotificationItemBase
    {
    }
}