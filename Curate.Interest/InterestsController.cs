﻿using Curate.Interest.Handlers.AddUserInterests;
using Curate.Interest.Handlers.GetInterests;
using Curate.Linq;
using Curate.Mvc;
using Curate.Mvc.ControllerHelpers;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace Curate.Interest
{
    public class InterestsController : AuthControllerBase
    {
        [HttpGet]
        [ProducesResponseType(typeof(PaginationResult<InterestItem>), StatusCodes.Status200OK)]
        public IActionResult
            Get([FromQuery] GetInterestsQuery query, [FromServices] GetInterestsQueryHandler handler) =>
            Ok(handler.Handle(query));

        [HttpPost]
        [Produces(MimeTypes.Application.Json)]
        public IActionResult Post([FromBody] AddUserInterestsCommand command,
            [FromServices] AddUserInterestsCommandHandler handler)
        {
            handler.Handle(command);

            return NoContent();
        }
    }
}