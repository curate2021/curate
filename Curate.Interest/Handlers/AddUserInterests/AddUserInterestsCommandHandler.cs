﻿using System.Linq;
using Curate.Core;
using Curate.Domain.Entities;
using Force.Cqrs;
using Microsoft.EntityFrameworkCore;

namespace Curate.Interest.Handlers.AddUserInterests
{
    public class AddUserInterestsCommandHandler : ICommandHandler<AddUserInterestsCommand>
    {
        private readonly DbContext _dbContext;
        private readonly IUserContext<User> _userContext;

        public AddUserInterestsCommandHandler(IUserContext<User> userContext, DbContext dbContext)
        {
            _userContext = userContext;
            _dbContext = dbContext;
        }

        public void Handle(AddUserInterestsCommand input)
        {
            var currentUser = _userContext.User;

            var interestIds = input.Ids
                .ToArray();
            var interests = _dbContext.Set<Domain.Entities.Interest>()
                .Where(x => interestIds.Contains(x.Id))
                .ToArray();

            currentUser.Profile.AddInterests(interests);
            _dbContext.SaveChanges();
        }
    }
}