﻿using FluentValidation;

namespace Curate.Interest.Handlers.AddUserInterests
{
    public class AddUserInterestsCommandValidator : AbstractValidator<AddUserInterestsCommand>
    {
        public AddUserInterestsCommandValidator()
        {
            RuleFor(x => x.Ids)
                .NotEmpty();
        }
    }
}