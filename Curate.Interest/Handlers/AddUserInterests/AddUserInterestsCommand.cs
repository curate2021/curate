﻿using System.Collections.Generic;
using Force.Cqrs;

namespace Curate.Interest.Handlers.AddUserInterests
{
    public class AddUserInterestsCommand : ICommand
    {
        public IEnumerable<int> Ids { get; set; }
    }
}