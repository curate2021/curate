﻿namespace Curate.Interest.Handlers.GetInterests
{
    public class InterestItem
    {
        public int Id { get; set; }
        
        public string Name { get; set; }
    }
}