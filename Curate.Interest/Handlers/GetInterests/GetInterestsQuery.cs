﻿using Curate.Linq;
using Force.Cqrs;
using Force.Linq.Pagination;

namespace Curate.Interest.Handlers.GetInterests
{
    public class GetInterestsQuery : Paging, IQuery<PaginationResult<InterestItem>>
    {
        
    }
}