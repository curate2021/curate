﻿using System.Linq;
using AutoMapper;
using AutoMapper.QueryableExtensions;
using Curate.Linq;
using Force.Cqrs;
using Microsoft.EntityFrameworkCore;

namespace Curate.Interest.Handlers.GetInterests
{
    public class GetInterestsQueryHandler : IQueryHandler<GetInterestsQuery, PaginationResult<InterestItem>>
    {
        private readonly DbContext _dbContext;
        private readonly IMapper _mapper;

        public GetInterestsQueryHandler(DbContext dbContext, IMapper mapper)
        {
            _dbContext = dbContext;
            _mapper = mapper;
        }

        public PaginationResult<InterestItem> Handle(GetInterestsQuery input)
        {
            return _dbContext.Set<Domain.Entities.Interest>()
                .ProjectTo<InterestItem>(_mapper.ConfigurationProvider)
                .OrderBy(x => x.Id)
                .ToPaginationResult(input);
        }
    }
}