﻿using Curate.Interest.Handlers.GetInterests;

namespace Curate.Interest
{
    public class InterestsProfile : AutoMapper.Profile
    {
        public InterestsProfile()
        {
            CreateMap<Domain.Entities.Interest, InterestItem>();
        }
    }
}