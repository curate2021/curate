﻿namespace Curate.Category.Dtos
{
    public class CategoryItem
    {
        public int Id { get; set; }
        
        public string Name { get; set; }
        
        public string IconId { get; set; }
    }
}