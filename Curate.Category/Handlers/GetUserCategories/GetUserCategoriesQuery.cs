﻿using Curate.Category.Dtos;
using Curate.Linq;
using Force.Cqrs;
using Force.Linq.Pagination;

namespace Curate.Category.Handlers.GetUserCategories
{
    public class GetUserCategoriesQuery : Paging, IQuery<PaginationResult<CategoryItem>>
    {
    }
}