﻿using System.Linq;
using AutoMapper;
using AutoMapper.QueryableExtensions;
using Curate.Category.Dtos;
using Curate.Core;
using Curate.Linq;
using Force.Cqrs;
using Microsoft.EntityFrameworkCore;

namespace Curate.Category.Handlers.GetUserCategories
{
    public class GetUserCategoriesQueryHandler : IQueryHandler<GetUserCategoriesQuery, PaginationResult<CategoryItem>>
    {
        private readonly IUserContext<Domain.Entities.User> _userContext;
        private readonly DbContext _dbContext;
        private readonly IMapper _mapper;

        public GetUserCategoriesQueryHandler(IUserContext<Domain.Entities.User> userContext, DbContext dbContext,
            IMapper mapper)
        {
            _userContext = userContext;
            _dbContext = dbContext;
            _mapper = mapper;
        }

        public PaginationResult<CategoryItem> Handle(GetUserCategoriesQuery input) =>
            _dbContext.Set<Domain.Entities.Category>()
                .ProjectTo<CategoryItem>(_mapper.ConfigurationProvider)
                .OrderBy(x => x.Id)
                .ToPaginationResult(input);
    }
}