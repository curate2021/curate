﻿using Curate.Category.Dtos;
using Curate.Category.Handlers.GetUserCategories;
using Curate.Linq;
using Curate.Mvc.ControllerHelpers;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace Curate.Category
{
    public class CategoriesController : AuthControllerBase
    {
        [HttpGet]
        [ProducesResponseType(typeof(PaginationResult<CategoryItem>), StatusCodes.Status200OK)]
        public IActionResult Get([FromQuery] GetUserCategoriesQuery query,
            [FromServices] GetUserCategoriesQueryHandler handler) =>
            Ok(handler.Handle(query));
    }
}