﻿using Curate.Category.Dtos;

namespace Curate.Category.Profiles
{
    public class CategoriesProfile : AutoMapper.Profile 
    {
        public CategoriesProfile()
        {
            CreateMap<Domain.Entities.Category, CategoryItem>();
        }
    }
}