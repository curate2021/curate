﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;
using Npgsql.EntityFrameworkCore.PostgreSQL.Metadata;

namespace Curate.DataAccess.Migrations
{
    public partial class AddListingWithCategories : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_InterestProfile_Profiles_UsersId",
                table: "InterestProfile");

            migrationBuilder.RenameColumn(
                name: "UsersId",
                table: "InterestProfile",
                newName: "ProfilesId");

            migrationBuilder.RenameIndex(
                name: "IX_InterestProfile_UsersId",
                table: "InterestProfile",
                newName: "IX_InterestProfile_ProfilesId");

            migrationBuilder.AlterColumn<string>(
                name: "Name",
                table: "Interests",
                type: "text",
                nullable: false,
                oldClrType: typeof(string),
                oldType: "character varying(255)",
                oldMaxLength: 255);

            migrationBuilder.CreateTable(
                name: "Category",
                columns: table => new
                {
                    Id = table.Column<int>(type: "integer", nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    Name = table.Column<string>(type: "text", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Category", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Listing",
                columns: table => new
                {
                    Id = table.Column<int>(type: "integer", nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    Title = table.Column<string>(type: "text", nullable: true),
                    Description = table.Column<string>(type: "text", nullable: true),
                    Quantity = table.Column<int>(type: "integer", nullable: false),
                    Price = table.Column<decimal>(type: "numeric", nullable: false),
                    CreateDate = table.Column<DateTime>(type: "timestamp without time zone", nullable: false),
                    ProfileId = table.Column<int>(type: "integer", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Listing", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Listing_Profiles_ProfileId",
                        column: x => x.ProfileId,
                        principalTable: "Profiles",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "CategoryInterest",
                columns: table => new
                {
                    CategoriesId = table.Column<int>(type: "integer", nullable: false),
                    InterestsId = table.Column<int>(type: "integer", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_CategoryInterest", x => new { x.CategoriesId, x.InterestsId });
                    table.ForeignKey(
                        name: "FK_CategoryInterest_Category_CategoriesId",
                        column: x => x.CategoriesId,
                        principalTable: "Category",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_CategoryInterest_Interests_InterestsId",
                        column: x => x.InterestsId,
                        principalTable: "Interests",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_CategoryInterest_InterestsId",
                table: "CategoryInterest",
                column: "InterestsId");

            migrationBuilder.CreateIndex(
                name: "IX_Listing_ProfileId",
                table: "Listing",
                column: "ProfileId");

            migrationBuilder.AddForeignKey(
                name: "FK_InterestProfile_Profiles_ProfilesId",
                table: "InterestProfile",
                column: "ProfilesId",
                principalTable: "Profiles",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_InterestProfile_Profiles_ProfilesId",
                table: "InterestProfile");

            migrationBuilder.DropTable(
                name: "CategoryInterest");

            migrationBuilder.DropTable(
                name: "Listing");

            migrationBuilder.DropTable(
                name: "Category");

            migrationBuilder.RenameColumn(
                name: "ProfilesId",
                table: "InterestProfile",
                newName: "UsersId");

            migrationBuilder.RenameIndex(
                name: "IX_InterestProfile_ProfilesId",
                table: "InterestProfile",
                newName: "IX_InterestProfile_UsersId");

            migrationBuilder.AlterColumn<string>(
                name: "Name",
                table: "Interests",
                type: "character varying(255)",
                maxLength: 255,
                nullable: false,
                oldClrType: typeof(string),
                oldType: "text");

            migrationBuilder.AddForeignKey(
                name: "FK_InterestProfile_Profiles_UsersId",
                table: "InterestProfile",
                column: "UsersId",
                principalTable: "Profiles",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
