﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Curate.DataAccess.Migrations
{
    public partial class AddCountryToProfile : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "CountryId",
                table: "Profiles",
                type: "integer",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_Profiles_CountryId",
                table: "Profiles",
                column: "CountryId");

            migrationBuilder.AddForeignKey(
                name: "FK_Profiles_Country_CountryId",
                table: "Profiles",
                column: "CountryId",
                principalTable: "Country",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Profiles_Country_CountryId",
                table: "Profiles");

            migrationBuilder.DropIndex(
                name: "IX_Profiles_CountryId",
                table: "Profiles");

            migrationBuilder.DropColumn(
                name: "CountryId",
                table: "Profiles");
        }
    }
}
