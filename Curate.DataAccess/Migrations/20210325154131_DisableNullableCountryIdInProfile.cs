﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Curate.DataAccess.Migrations
{
    public partial class DisableNullableCountryIdInProfile : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql(@"update public.""Profiles"" set ""CountryId"" = 1 where ""CountryId"" is null");
            migrationBuilder.DropForeignKey(
                name: "FK_Profiles_Country_CountryId",
                table: "Profiles");

            migrationBuilder.AlterColumn<int>(
                name: "CountryId",
                table: "Profiles",
                type: "integer",
                nullable: false,
                defaultValue: 0,
                oldClrType: typeof(int),
                oldType: "integer",
                oldNullable: true);

            migrationBuilder.AddForeignKey(
                name: "FK_Profiles_Country_CountryId",
                table: "Profiles",
                column: "CountryId",
                principalTable: "Country",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Profiles_Country_CountryId",
                table: "Profiles");

            migrationBuilder.AlterColumn<int>(
                name: "CountryId",
                table: "Profiles",
                type: "integer",
                nullable: true,
                oldClrType: typeof(int),
                oldType: "integer");

            migrationBuilder.AddForeignKey(
                name: "FK_Profiles_Country_CountryId",
                table: "Profiles",
                column: "CountryId",
                principalTable: "Country",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
