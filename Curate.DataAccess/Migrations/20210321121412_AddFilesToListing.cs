﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Curate.DataAccess.Migrations
{
    public partial class AddFilesToListing : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "ListingId",
                table: "File",
                type: "integer",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_File_ListingId",
                table: "File",
                column: "ListingId");

            migrationBuilder.AddForeignKey(
                name: "FK_File_Listing_ListingId",
                table: "File",
                column: "ListingId",
                principalTable: "Listing",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_File_Listing_ListingId",
                table: "File");

            migrationBuilder.DropIndex(
                name: "IX_File_ListingId",
                table: "File");

            migrationBuilder.DropColumn(
                name: "ListingId",
                table: "File");
        }
    }
}
