﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;
using Npgsql.EntityFrameworkCore.PostgreSQL.Metadata;

namespace Curate.DataAccess.Migrations
{
    public partial class Kyc : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "KycId",
                table: "Profiles",
                type: "integer",
                nullable: true);

            migrationBuilder.CreateTable(
                name: "Kyc",
                columns: table => new
                {
                    Id = table.Column<int>(type: "integer", nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    ProfileId = table.Column<int>(type: "integer", nullable: true),
                    PhotoId = table.Column<string>(type: "text", nullable: true),
                    DocumentType = table.Column<int>(type: "integer", nullable: false),
                    CreateDate = table.Column<DateTime>(type: "timestamp without time zone", nullable: false),
                    IsConfirmed = table.Column<bool>(type: "boolean", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Kyc", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Kyc_File_PhotoId",
                        column: x => x.PhotoId,
                        principalTable: "File",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Kyc_Profiles_ProfileId",
                        column: x => x.ProfileId,
                        principalTable: "Profiles",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Profiles_KycId",
                table: "Profiles",
                column: "KycId");

            migrationBuilder.CreateIndex(
                name: "IX_Kyc_PhotoId",
                table: "Kyc",
                column: "PhotoId");

            migrationBuilder.CreateIndex(
                name: "IX_Kyc_ProfileId",
                table: "Kyc",
                column: "ProfileId");

            migrationBuilder.AddForeignKey(
                name: "FK_Profiles_Kyc_KycId",
                table: "Profiles",
                column: "KycId",
                principalTable: "Kyc",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Profiles_Kyc_KycId",
                table: "Profiles");

            migrationBuilder.DropTable(
                name: "Kyc");

            migrationBuilder.DropIndex(
                name: "IX_Profiles_KycId",
                table: "Profiles");

            migrationBuilder.DropColumn(
                name: "KycId",
                table: "Profiles");
        }
    }
}
