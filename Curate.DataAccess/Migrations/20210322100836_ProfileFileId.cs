﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Curate.DataAccess.Migrations
{
    public partial class ProfileFileId : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_DeliveryCountry_Country_CountryId",
                table: "DeliveryCountry");

            migrationBuilder.DropForeignKey(
                name: "FK_DeliveryCountry_Listing_ListingId",
                table: "DeliveryCountry");

            migrationBuilder.AlterColumn<int>(
                name: "ListingId",
                table: "DeliveryCountry",
                type: "integer",
                nullable: false,
                defaultValue: 0,
                oldClrType: typeof(int),
                oldType: "integer",
                oldNullable: true);

            migrationBuilder.AlterColumn<int>(
                name: "CountryId",
                table: "DeliveryCountry",
                type: "integer",
                nullable: false,
                defaultValue: 0,
                oldClrType: typeof(int),
                oldType: "integer",
                oldNullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "CreateDate",
                table: "Comment",
                type: "timestamp without time zone",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));

            migrationBuilder.CreateTable(
                name: "CategoryListing",
                columns: table => new
                {
                    CategoriesId = table.Column<int>(type: "integer", nullable: false),
                    ListingsId = table.Column<int>(type: "integer", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_CategoryListing", x => new { x.CategoriesId, x.ListingsId });
                    table.ForeignKey(
                        name: "FK_CategoryListing_Category_CategoriesId",
                        column: x => x.CategoriesId,
                        principalTable: "Category",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_CategoryListing_Listing_ListingsId",
                        column: x => x.ListingsId,
                        principalTable: "Listing",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_CategoryListing_ListingsId",
                table: "CategoryListing",
                column: "ListingsId");

            migrationBuilder.AddForeignKey(
                name: "FK_DeliveryCountry_Country_CountryId",
                table: "DeliveryCountry",
                column: "CountryId",
                principalTable: "Country",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_DeliveryCountry_Listing_ListingId",
                table: "DeliveryCountry",
                column: "ListingId",
                principalTable: "Listing",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_DeliveryCountry_Country_CountryId",
                table: "DeliveryCountry");

            migrationBuilder.DropForeignKey(
                name: "FK_DeliveryCountry_Listing_ListingId",
                table: "DeliveryCountry");

            migrationBuilder.DropTable(
                name: "CategoryListing");

            migrationBuilder.DropColumn(
                name: "CreateDate",
                table: "Comment");

            migrationBuilder.AlterColumn<int>(
                name: "ListingId",
                table: "DeliveryCountry",
                type: "integer",
                nullable: true,
                oldClrType: typeof(int),
                oldType: "integer");

            migrationBuilder.AlterColumn<int>(
                name: "CountryId",
                table: "DeliveryCountry",
                type: "integer",
                nullable: true,
                oldClrType: typeof(int),
                oldType: "integer");

            migrationBuilder.AddForeignKey(
                name: "FK_DeliveryCountry_Country_CountryId",
                table: "DeliveryCountry",
                column: "CountryId",
                principalTable: "Country",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_DeliveryCountry_Listing_ListingId",
                table: "DeliveryCountry",
                column: "ListingId",
                principalTable: "Listing",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
