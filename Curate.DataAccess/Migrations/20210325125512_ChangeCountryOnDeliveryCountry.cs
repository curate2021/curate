﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Curate.DataAccess.Migrations
{
    public partial class ChangeCountryOnDeliveryCountry : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Orders_Country_DeliveryCountryId",
                table: "Orders");

            migrationBuilder.AddForeignKey(
                name: "FK_Orders_DeliveryCountry_DeliveryCountryId",
                table: "Orders",
                column: "DeliveryCountryId",
                principalTable: "DeliveryCountry",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Orders_DeliveryCountry_DeliveryCountryId",
                table: "Orders");

            migrationBuilder.AddForeignKey(
                name: "FK_Orders_Country_DeliveryCountryId",
                table: "Orders",
                column: "DeliveryCountryId",
                principalTable: "Country",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
