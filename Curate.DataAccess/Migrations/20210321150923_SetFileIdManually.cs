﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Curate.DataAccess.Migrations
{
    public partial class SetFileIdManually : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "IconId",
                table: "Category",
                type: "text",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_Category_IconId",
                table: "Category",
                column: "IconId");

            migrationBuilder.AddForeignKey(
                name: "FK_Category_File_IconId",
                table: "Category",
                column: "IconId",
                principalTable: "File",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Category_File_IconId",
                table: "Category");

            migrationBuilder.DropIndex(
                name: "IX_Category_IconId",
                table: "Category");

            migrationBuilder.DropColumn(
                name: "IconId",
                table: "Category");
        }
    }
}
