﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Curate.Domain.Entities;
using Curate.Domain.Payment;
using Extensions.Hosting.AsyncInitialization;
using Microsoft.EntityFrameworkCore;

namespace Curate.DataAccess.Initializers
{
    public class StoreInitializer : IAsyncInitializer
    {
        private readonly DbContext _dbContext;

        public StoreInitializer(DbContext dbContext)
        {
            _dbContext = dbContext;
        }

        public async Task InitializeAsync()
        {
            await InitializePaymentMethods();
        }

        private async Task InitializePaymentMethods()
        {
            if (!_dbContext.Set<PaymentMethod>().Any())
            {
                var paymentMethods = new List<PaymentMethod>(new[]
                {
                    new PaymentMethod(PaymentMethodType.CreditOrDebitCard, "Credit/debit card",
                        new File("card_logo.png", FileType.Icon)),
                    new PaymentMethod(PaymentMethodType.PayPal, "PayPal",new File("paypal_logo.png", FileType.Icon), (decimal) 0.025),
                    new PaymentMethod(PaymentMethodType.Btc, "BTC/ETH or BCH", new File("btc_logo.png", FileType.Icon)),
                    new PaymentMethod(PaymentMethodType.Xcur, "XCUR", new File("xcur_logo.png", FileType.Icon))
                });

                await _dbContext.AddRangeAsync(paymentMethods);
                await _dbContext.SaveChangesAsync();
            }
        }
    }
}