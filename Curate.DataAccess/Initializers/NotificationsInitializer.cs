﻿using System.Linq;
using System.Threading.Tasks;
using Curate.Domain.Entities;
using Extensions.Hosting.AsyncInitialization;
using Microsoft.EntityFrameworkCore;

namespace Curate.DataAccess.Initializers
{
    public class NotificationsInitializer : IAsyncInitializer
    {
        private readonly DbContext _dbContext;

        public NotificationsInitializer(DbContext dbContext)
        {
            _dbContext = dbContext;
        }

        public async Task InitializeAsync()
        {
            if (!_dbContext.Set<Notification>()
                .Any())
            {
                await _dbContext.Set<Notification>()
                    .AddRangeAsync(Notification.GetDefaultNotifications);
                await _dbContext.SaveChangesAsync();
            }

            var userWithoutNotifications = _dbContext.Set<User>()
                .Where(x => x.UserNotifications == null || x.UserNotifications.Count == 0)
                .ToArray();

            foreach (var user in userWithoutNotifications)
            {
                user.CreateDefaultNotifications();
            }
        }
    }
}