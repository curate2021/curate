﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Curate.Domain;
using Curate.Domain.Entities;
using Curate.Domain.Listing;
using Extensions.Hosting.AsyncInitialization;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;

namespace Curate.DataAccess.Initializers
{
    public class UserDataInitializer : IAsyncInitializer
    {
        private static readonly List<User> Users = new List<User>
        {
            new User("Benico", "Del Toro", "testuser@mail.ru", 1),
            new User("Ayrton", "Mcdonald", "secondTestUser@mail.ru", 2)
        };

        private static readonly List<Interest> Interests = new List<Interest>
        {
            new Interest("Fashion inspiration", "Fashion inspiration description"),
            new Interest("Clothes shopping", "Clothes shopping description")
        };

        private static readonly List<Category> Categories = new List<Category>
        {
            new Category("Clothes", new File("Clothes.png", FileType.Icon)),
            new Category("Accessories", new File("Accessories.png", FileType.Icon))
        };

        private readonly DbContext _dbContext;
        private readonly UserManager<User> _userManager;

        public UserDataInitializer(DbContext dbContext, UserManager<User> userManager)
        {
            _dbContext = dbContext;
            _userManager = userManager;
        }

        public async Task InitializeAsync()
        {
            await SeedUsers();
            await SeedInterests();
            await SeedCategories();
            await SeedCountries();
            await SeedListings();
        }

        private async Task SeedCountries()
        {
            if (!_dbContext.Set<Country>().Any())
            {
                var countries = new List<Country>(new[]
                {
                    new Country("USA"),
                    new Country("Canada")
                });

                await _dbContext.AddRangeAsync(countries);
                await _dbContext.SaveChangesAsync();
            }
        }

        private async Task SeedListings()
        {
            if (!_dbContext.Set<Listing>().Any())
            {
                var categories = _dbContext.Set<Category>()
                    .ToArray();
                var countries = _dbContext.Set<Country>()
                    .ToArray();
                var profiles = _dbContext.Set<User>()
                    .Select(x => x.Profile)
                    .ToArray();
                var titles = new[]
                {
                    "Beautiful Nike Airmax 95",
                    "Adidas Superstar"
                };
                var descriptions = new[]
                {
                    @"The Nike Air Max 95 By You is your chance
                to customize the running shoe that took the
                world by storm in the mid-90s. Complement
                the iconic '95 wavy lines and iconic airbags
                with new colors and unique touches.",
                    @"adidas Originals Superstar trainers in white"
                };

                var listings = Enumerable.Range(0, 2)
                    .Select(index =>
                    {
                        var listing =
                            new Listing(titles[index], descriptions[index], 75, 190, profiles[index], new[]
                            {
                                new File($"TestImage_{index}.png", FileType.Image),
                                new File($"TestImage1_{index}.png", FileType.Image)
                            }, new[]
                            {
                                categories[index]
                            });
                        listing.AddDeliveryCountries(new[]
                        {
                            new DeliveryCountry(countries[index], listing, 120)
                        });
                        return listing;
                    });
                await _dbContext.AddRangeAsync(listings);
                await _dbContext.SaveChangesAsync();
            }
        }

        private async Task SeedInterests()
        {
            if (!_dbContext.Set<Interest>().Any())
            {
                var users = _dbContext.Set<User>()
                    .Include(x => x.Profile)
                    .Include(x => x.Profile.Interests)
                    .ToArray();

                users[0].Profile.AddInterests(Interests);
                users[1].Profile.AddInterests(new[] {Interests[1]});
                await _dbContext.AddRangeAsync(Interests);
                await _dbContext.SaveChangesAsync();
            }
        }

        private async Task SeedUsers()
        {
            if (!_dbContext.Set<User>().Any())
            {
                foreach (var user in Users)
                {
                    await _userManager.CreateAsync(user, "Qwe1234!");
                    await _userManager.AddToRoleAsync(user, RoleNames.User.ToString());
                    user.Profile.AddPhoto(new File($"{user.Profile.Name}_ProfilePicture.png", FileType.Image));
                }

                await _dbContext.SaveChangesAsync();
            }
        }

        private async Task SeedCategories()
        {
            if (!_dbContext.Set<Category>().Any())
            {
                await _dbContext.AddRangeAsync(Categories);
                await _dbContext.SaveChangesAsync();
            }
        }
    }
}