﻿using System;
using System.Linq;
using System.Threading.Tasks;
using Curate.Domain;
using Curate.Domain.Entities;
using Extensions.Hosting.AsyncInitialization;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;

namespace Curate.DataAccess.Initializers
{
    public class IdentityInitializer : IAsyncInitializer
    {
        private readonly RoleManager<Role> _roleManager;
        private readonly DbContext _dbContext;

        public IdentityInitializer(RoleManager<Role> roleManager, DbContext dbContext)
        {
            _roleManager = roleManager;
            _dbContext = dbContext;
        }

        public async Task InitializeAsync()
        {
            await _dbContext.Database.MigrateAsync();
            await SeedRoles();
        }

        private async Task SeedRoles()
        {
            var roleNames = Enum.GetNames(typeof(RoleNames));
            foreach (var roleName in roleNames)
            {
                var myRoles = _roleManager.Roles.ToArray();
                if (!await _roleManager.RoleExistsAsync(roleName))
                    await _roleManager.CreateAsync(new Role(roleName));
            }
        }
    }
}