﻿using System.Threading.Tasks;
using Curate.Domain.Entities.Reward;
using Extensions.Hosting.AsyncInitialization;
using Microsoft.EntityFrameworkCore;

namespace Curate.DataAccess.Initializers
{
    public class RewardsInitializer : IAsyncInitializer
    {
        private readonly DbContext _dbContext;

        public RewardsInitializer(DbContext dbContext)
        {
            _dbContext = dbContext;
        }

        public async Task InitializeAsync()
        {
            var orderRewardRate = await _dbContext.Set<OrderRewardRate>()
                .FirstOrDefaultAsync();

            if (orderRewardRate == null)
            {
                var baseRate = 0.5m;
                orderRewardRate = new OrderRewardRate(baseRate, baseRate);
                await _dbContext.AddAsync(orderRewardRate);
                await _dbContext.SaveChangesAsync();
            }
        }
    }
}