﻿using System;
using Curate.Domain.Entities;
using Curate.Domain.Entities.Reward;
using Curate.Domain.Order;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;

namespace Curate.DataAccess
{
    public class CurateContext : IdentityDbContext<User, Role, int>
    {
        public CurateContext(DbContextOptions options) : base(options)
        {
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            modelBuilder.ApplyConfigurationsFromAssembly(typeof(CurateContext).Assembly);
        }

        public DbSet<Profile> Profiles { get; set; }

        public DbSet<Interest> Interests { get; set; }

        public DbSet<Order> Orders { get; set; }
        
        public DbSet<Reward> Rewards { get; set; }
        
        public DbSet<OrderReward> OrderRewards { get; set; }
        
        public DbSet<OrderRewardRate> OrderRewardRates { get; set; }
    }
}