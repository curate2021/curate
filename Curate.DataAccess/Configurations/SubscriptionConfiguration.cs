﻿using Curate.Domain.Entities;
using Curate.Domain.Order;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Curate.DataAccess.Configurations
{
    public class SubscriptionConfiguration : IEntityTypeConfiguration<Subscription>
    {
        public void Configure(EntityTypeBuilder<Subscription> builder)
        {
            builder
                .HasOne(c => c.To)
                .WithMany(c => c.Subscribers);

            builder
                .HasOne(c => c.From)
                .WithMany(c => c.Subscriptions);

            builder.HasKey(k => new {k.ToId, k.FromId});
        }
    }
}