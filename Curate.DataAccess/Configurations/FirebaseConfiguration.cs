﻿using Curate.Domain.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Curate.DataAccess.Configurations
{
    public class FirebaseConfiguration : IEntityTypeConfiguration<Firebase>
    {
        public void Configure(EntityTypeBuilder<Firebase> builder)
        {
            builder.HasOne(x => x.User)
                .WithOne(x => x.Firebase)
                .IsRequired(false);

            builder.HasQueryFilter(x => !x.IsDeleted);
        }
    }
}