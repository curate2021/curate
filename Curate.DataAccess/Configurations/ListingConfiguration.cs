using Curate.Domain.Entities;
using Curate.Domain.Listing;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Curate.DataAccess.Configurations
{
    public class ListingConfiguration : IEntityTypeConfiguration<Listing>
    {
        public void Configure(EntityTypeBuilder<Listing> builder)
        {
            builder.HasQueryFilter(x=> !x.IsDeleted);
            builder.HasQueryFilter(x=> !x.IsArchived);
        }
    }
}