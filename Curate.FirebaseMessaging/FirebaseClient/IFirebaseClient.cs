﻿using System.Collections.Generic;
using System.Threading.Tasks;
using FirebaseAdmin.Messaging;

namespace Curate.FirebaseMessaging.FirebaseClient
{
    public interface IFirebaseClient
    {
        public Task SendNotifications(IEnumerable<Message> messages);
    }
}