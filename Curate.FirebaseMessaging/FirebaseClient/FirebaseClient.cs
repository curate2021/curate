﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using FirebaseAdmin;
using FirebaseAdmin.Messaging;
using Google.Apis.Auth.OAuth2;
using Microsoft.Extensions.Options;
using Serilog;

namespace Curate.FirebaseMessaging.FirebaseClient
{
    public class FirebaseClient : IFirebaseClient
    {
        private readonly FirebaseAdmin.Messaging.FirebaseMessaging _firebaseMessaging;
        private readonly ILogger _logger;

        public FirebaseClient(IOptions<FirebaseMessagingOptions> firebaseMessagingOptions, ILogger logger)
        {
            _logger = logger;
            var (credentialFilePath, firebaseMessagingUri) = (firebaseMessagingOptions.Value.CredentialFilePath,
                firebaseMessagingOptions.Value.FirebaseMessagingUri);
            _firebaseMessaging = FirebaseAdmin.Messaging.FirebaseMessaging.GetMessaging(FirebaseApp.Create(
                new AppOptions
                {
                    Credential = GoogleCredential.FromFile(credentialFilePath)
                        .CreateScoped(firebaseMessagingUri)
                }));
        }

        public async Task SendNotifications(IEnumerable<Message> messages)
        {
            try
            {
                var result = await _firebaseMessaging.SendAllAsync(messages);
                var unSend = string.Join(Environment.NewLine, 
                    result.Responses.Where(x => !x.IsSuccess)
                        .Select(x => $"{x.Exception.Message} {x.Exception.ErrorCode.ToString()}"));
                _logger.Information(unSend);
            }
            catch (Exception e)
            {
                _logger.Error(e.Message);
            }
        }
    }
}