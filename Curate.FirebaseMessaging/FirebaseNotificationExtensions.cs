﻿using System.Linq;

namespace Curate.FirebaseMessaging
{
    public static class FirebaseNotificationExtensions
    {
        public static NotificationData GetDataByNotificationType(this FirebaseNotificationOptions options,
            byte notificationType) =>
            options.NotificationsData.First(x => x.NotificationType == notificationType);
    }
}