﻿using System.Collections.Generic;

namespace Curate.FirebaseMessaging
{
    public class FirebaseNotificationOptions
    {
        public IEnumerable<NotificationData> NotificationsData { get; set; }

        public string ImagesUrl { get; set; }
    }

    public class NotificationData
    {
        public string Title { get; set; }
        
        public string Body { get; set; }
        
        public byte NotificationType { get; set; }
    }
}