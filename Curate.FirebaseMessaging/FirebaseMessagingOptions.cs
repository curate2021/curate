﻿namespace Curate.FirebaseMessaging
{
    public class FirebaseMessagingOptions
    {
        public string CredentialFilePath { get; set; }
        
        public string FirebaseMessagingUri { get; set; }
    }
}