﻿namespace Curate.Mvc
{
    public static class MimeTypes
    {
        public static class Image
        {
            public const string Png = "image/png";
            public const string Jpg = "image/jpg";
        }

        public static class Application
        {
            public const string Json = "application/json";
        }
    }
}