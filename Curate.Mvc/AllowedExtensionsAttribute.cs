﻿using System.Collections;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.IO;
using System.Linq;
using Microsoft.AspNetCore.Http;

namespace Curate.Mvc
{
    public class AllowedExtensionsAttribute : ValidationAttribute
    {
        private readonly IList _extensions;

        public AllowedExtensionsAttribute(params string[] extensions)
        {
            _extensions = extensions.ToList();
        }

        protected override ValidationResult IsValid(
            object value, ValidationContext validationContext)
        {
            if (!(value is IFormFile file)) 
                return ValidationResult.Success;

            var extension = Path.GetExtension(file.FileName);

            return !_extensions.Contains(extension?.ToLower())
                ? new ValidationResult(GetErrorMessage())
                : ValidationResult.Success;
        }

        private static string GetErrorMessage()
        {
            return "This extension is not allowed!";
        }
    }

    public class AllowedManyFilesExtensionsAttribute : ValidationAttribute
    {
        private readonly IList _extensions;

        public AllowedManyFilesExtensionsAttribute(params string[] extensions)
        {
            _extensions = extensions.ToList();
        }

        protected override ValidationResult IsValid(
            object value, ValidationContext validationContext)
        {
            if (!(value is IEnumerable<IFormFile> files))
                return ValidationResult.Success;

            return files.Select(file => Path.GetExtension(file.FileName))
                .Any(extension => !_extensions.Contains(extension?.ToLower()))
                ? new ValidationResult(ValidationMessages.AllowedExtensionErrorMessage)
                : ValidationResult.Success;
        }
    }

}