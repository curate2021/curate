﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using Microsoft.AspNetCore.Mvc.ModelBinding;

namespace Curate.Mvc.ControllerHelpers
{
    public class ControllerValidatorFilter : ValidationAttribute, IAsyncActionFilter
    {
        public async Task OnActionExecutionAsync(ActionExecutingContext context, ActionExecutionDelegate next)
        {
            var dtos = context
                .ActionDescriptor
                .Parameters
                .Where(x => x.BindingInfo?.BindingSource != BindingSource.Services)
                .ToList();

            if (!dtos.Any())
            {
                await next();
                return;
            }

            if (!context.ModelState.IsValid)
            {
                var attributesErrors = context.ModelState
                    .Where(x => x.Value.Errors.Any())
                    .Select(x => new ValidationResult(
                        x.Value.Errors.Count == 1
                            ? x.Value.Errors.Single().ErrorMessage
                            : x.Value.Errors.Aggregate("", (a, c) => $"{c.ErrorMessage}.{a}"),
                        new List<string> {x.Key}))
                    .ToList();
                
                context.Result = new JsonResult(attributesErrors)
                {
                    StatusCode = 422,
                };
                return;
            }
            
            await next();
        }
    }
}