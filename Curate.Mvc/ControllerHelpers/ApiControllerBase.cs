﻿using Microsoft.AspNetCore.Mvc;

namespace Curate.Mvc.ControllerHelpers
{
    [ApiController]
    [ControllerValidatorFilter]
    [Route("/api/[controller]/[action]")]
    public class ApiControllerBase : ControllerBase
    {
    }
}