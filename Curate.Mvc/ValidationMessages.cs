﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Curate.Mvc
{
    public class ValidationMessages
    {
        public const string MaxFileErrorMessage = "Maximum allowed file size is {0} bytes.";

        public const string AllowedExtensionErrorMessage = "This extension is not allowed!";
    }

}
